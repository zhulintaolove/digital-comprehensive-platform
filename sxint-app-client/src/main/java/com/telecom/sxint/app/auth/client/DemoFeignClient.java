package com.telecom.sxint.app.auth.client;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.auth.dto.TestDemoDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Title:
 * @version:
 * @Description:
 * @Copyright 2024 浙江信产.基础能力中心
 * @Author: liyawen
 * @Date: 2024/7/11
 */
@FeignClient(name = "sxint-app", path = "/sxint-app/rpc/demo")
public interface DemoFeignClient {

    @GetMapping(value = "/list")
    ResponseData<TestDemoDto> list(@RequestParam String name);


}
