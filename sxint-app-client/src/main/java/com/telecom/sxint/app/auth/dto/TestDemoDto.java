package com.telecom.sxint.app.auth.dto;

/**
 * @Title:
 * @version:
 * @Description:
 * @Copyright 2024 浙江信产.基础能力中心
 * @Author: liyawen
 * @Date: 2024/7/11
 */
public class TestDemoDto {

    /**
     * 名称
     */
    private String name;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 创建人id
     */
    private String createdBy;

    /**
     * 创建组织id
     */
    private String createdOrg;
}
