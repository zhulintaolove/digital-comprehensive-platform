package com.telecom.sxint.app.core.service.impl.Systemimpl.App1countyIndicatorsServiceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1CountyIndicators;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1QuarterPublicSheet;
import com.telecom.sxint.app.core.mapper.Systemmapper.App1countyIndicatorsmapper.App1CountyIndicatorsMapper;
import com.telecom.sxint.app.core.service.Systemservice.App1countyIndicatorsservice.IApp1CountyIndicatorsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 县分指标 服务实现类
 * </p>
 *
 * @author 
 * @since 2024-09-02
 */
@Service
public class App1CountyIndicatorsServiceImpl extends ServiceImpl<App1CountyIndicatorsMapper, App1CountyIndicators> implements IApp1CountyIndicatorsService {

    @Override
    public List<App1CountyIndicators> selectAllData() {
        QueryWrapper<App1CountyIndicators> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("is_delete",0);
        return baseMapper.selectList(queryWrapper);
    }

    /**
     * 逻辑删除
     */
    @Override
    public void IsDelete(App1CountyIndicators app1CountyIndicators) {
        app1CountyIndicators.setIsDelete(1);
        baseMapper.updateById(app1CountyIndicators);
    }
}
