package com.telecom.sxint.app.core.domain.entity.Query;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "分页查询实体")
public class PageBase implements Serializable {
    private Long currentPage = 1L;

    private Long pageSize = 10L;

    private Boolean doSearchTotal = true;

}