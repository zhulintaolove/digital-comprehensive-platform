package com.telecom.sxint.app.core.domain.entity.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "单条专票记录导入")
public class SingleApp1VatinvoinceprintlogDTO {

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "组织")
    private String area;

    @ApiModelProperty(value = "合同号")
    private String account;

    @ApiModelProperty(value = "打印周期")
    private String printmonth;

    @ApiModelProperty(value = "专票流水号")
    private String invoice;

    @ApiModelProperty(value = "金额")
    private String amount;

    @ApiModelProperty(value = "是否已打印")
    private String printflag;

    @ApiModelProperty(value = "录入人员")
    private String operator;

    @ApiModelProperty(value = "创建日期")
    private String created;

    @ApiModelProperty(value = "专票周期")
    private String billmonth;



}