package com.telecom.sxint.app.core.mapper;

import com.telecom.sxint.app.core.domain.entity.TestDemo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 测试信息表 Mapper 接口
 * </p>
 *
 * @author sx_tele
 * @since 2024-07-11
 */
public interface TestDemoMapper extends BaseMapper<TestDemo> {

}
