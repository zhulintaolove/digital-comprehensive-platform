package com.telecom.sxint.app.core.service.Historyservice;

import com.telecom.sxint.app.core.domain.entity.Historypojo.App1YunwangHistoryPublicSheet;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-09-24
 */
public interface IApp1YunwangHistoryPublicSheetService extends IService<App1YunwangHistoryPublicSheet> {
    /**
     * 查询所有标题字段
     * @return
     */
    List<App1YunwangHistoryPublicSheet> selectAllField();
    /**
     * 根据表名查询数据（包括标题）
     * @return
     */
    List<App1YunwangHistoryPublicSheet> selectBySheetName(String sheetName);
    /**
     * 根据sheet名查询标题字段
     * @return
     */
    List<App1YunwangHistoryPublicSheet> selectFieldBySheetName(String sheetName);

    /**
     * 查询所有数据
     * @return
     */
    List<App1YunwangHistoryPublicSheet> selectAllData();

    /**
     * 根据表名查询数据
     * @return
     */
    List<App1YunwangHistoryPublicSheet> selectDataBySheetName(String sheetName,String month);

    /**
     * 查询所有表名
     * @return
     */
    List<String> selectAllSheetNames();
}
