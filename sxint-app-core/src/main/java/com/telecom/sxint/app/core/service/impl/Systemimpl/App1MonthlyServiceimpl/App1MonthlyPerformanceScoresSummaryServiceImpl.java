package com.telecom.sxint.app.core.service.impl.Systemimpl.App1MonthlyServiceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo.App1MonthlyPerformanceScoresSummary;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangPublicSheet;
import com.telecom.sxint.app.core.mapper.Systemmapper.App1Monthlymapper.App1MonthlyPerformanceScoresSummaryMapper;
import com.telecom.sxint.app.core.service.Systemservice.App1Monthlyservice.IApp1MonthlyPerformanceScoresSummaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
@Service
public class App1MonthlyPerformanceScoresSummaryServiceImpl extends ServiceImpl<App1MonthlyPerformanceScoresSummaryMapper, App1MonthlyPerformanceScoresSummary> implements IApp1MonthlyPerformanceScoresSummaryService {
    @Override
    public List<App1MonthlyPerformanceScoresSummary> getExist() {
        QueryWrapper<App1MonthlyPerformanceScoresSummary> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0);
        return baseMapper.selectList(queryWrapper);
    }

    /**
     * 查找所有部门名称
     */
    @Override
    public List<String> getAllDepartName() {
        // 创建查询包装器
        QueryWrapper<App1MonthlyPerformanceScoresSummary> queryWrapper = new QueryWrapper<>();
        // 选择 distinct sheet_name 和 id
        queryWrapper.select("department_name", "sequence_number").eq("is_delete", 0);
        // 按照 id 排序
        queryWrapper.orderByAsc("sequence_number");
        // 执行查询
        List<App1MonthlyPerformanceScoresSummary> resultList = baseMapper.selectList(queryWrapper);
        // 提取 sheet_name 列到列表中
        List<String> sheetNameList = resultList.stream()
                .map(App1MonthlyPerformanceScoresSummary::getDepartmentName)
                .distinct()
                .collect(Collectors.toList());
        return sheetNameList;
    }

    /**
     * 逻辑删除
     */
    @Override
    public void deleteById(App1MonthlyPerformanceScoresSummary app1MonthlyPerformanceScoresSummary) {
        //is_delete设为1
        app1MonthlyPerformanceScoresSummary.setIsDelete(1);
        baseMapper.updateById(app1MonthlyPerformanceScoresSummary);
    }






}
