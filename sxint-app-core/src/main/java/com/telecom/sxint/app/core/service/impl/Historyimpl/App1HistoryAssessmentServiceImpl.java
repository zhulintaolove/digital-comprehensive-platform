package com.telecom.sxint.app.core.service.impl.Historyimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Historypojo.App1HistoryAssessment;
import com.telecom.sxint.app.core.mapper.Historymapper.App1HistoryAssessmentMapper;
import com.telecom.sxint.app.core.service.Historyservice.IApp1HistoryAssessmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-09-27
 */
@Service
public class  App1HistoryAssessmentServiceImpl extends ServiceImpl<App1HistoryAssessmentMapper, App1HistoryAssessment> implements IApp1HistoryAssessmentService {
    /**
     * 根据部门名称来查找
     * @param department
     * @return
     */
    @Override
    public List<App1HistoryAssessment> selectWithDepartment(String department, String month) {
        QueryWrapper<App1HistoryAssessment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("assessed_department", department)
                .eq("year_month", month)
                .eq("is_delete", 0);
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<String> selectAllDepartment() {
        // 创建查询包装器
        QueryWrapper<App1HistoryAssessment> queryWrapper = new QueryWrapper<>();
        // 选择 distinct assessed_department 和 id
        queryWrapper.select("assessed_department", "id").eq("is_delete", 0);
        // 按照 id 排序
        queryWrapper.orderByAsc("id");
        // 执行查询
        List<App1HistoryAssessment> resultList = baseMapper.selectList(queryWrapper);
        // 提取 assessed_department 列到列表中
        List<String> departmentList = resultList.stream()
                .map(App1HistoryAssessment::getAssessedDepartment)
                .distinct()
                .collect(Collectors.toList());
        return departmentList;
    }
}
