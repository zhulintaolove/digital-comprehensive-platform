package com.telecom.sxint.app.core.service.Messageservice;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.Messagepojo.App1UccpMessageParams;
import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.domain.entity.dto.UccpMessageParamsDTO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p >
 *
 * @author
 * @since 2024-10-25
 */
public interface IApp1UccpMessageParamsService extends IService<App1UccpMessageParams> {

    /**
     * 查询全部
     * @param
     * @return
     */
    ResponseData<List<App1UccpMessageParams>> queryAllUccp();

    /**
     * 根据id查询
     * @param id
     * @return
     */
    ResponseData<App1UccpMessageParams> queryUccpById(Integer id);

    /**
     * 新增
     */
    ResponseData<App1UccpMessageParams> insertUccp(App1UccpMessageParams app1UccpMessageParams);

    /**
     * 修改
     */
    ResponseData<App1UccpMessageParams> updateUccp(App1UccpMessageParams app1UccpMessageParams);

    /**
     * 删除
     */
    ResponseData<App1UccpMessageParams> deleteUccp(Integer id);

    /**
     * 分页查询
     * @param
     * @return
     */
    PageData queryUccpPage(UccpMessageParamsDTO uccpMessageParamsDTO);
    /**
     * 分页查询升序
     * @param
     * @return
     */
    PageData queryUccpPageAsc(UccpMessageParamsDTO uccpMessageParamsDTO);

    /**
     * 分页查询降序
     * @param
     * @return
     */
    PageData queryUccpPageDesc(UccpMessageParamsDTO uccpMessageParamsDTO);

    /*
     *根据条件查询
     */
    PageData queryAllMessagesPageByCondition(String condition,UccpMessageParamsDTO uccpMessageParamsDTO);
}