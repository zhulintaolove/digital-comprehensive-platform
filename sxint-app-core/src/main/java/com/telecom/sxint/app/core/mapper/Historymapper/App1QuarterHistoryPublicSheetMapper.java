package com.telecom.sxint.app.core.mapper.Historymapper;

import com.telecom.sxint.app.core.domain.entity.Historypojo.App1QuarterHistoryPublicSheet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-09-24
 */
public interface App1QuarterHistoryPublicSheetMapper extends BaseMapper<App1QuarterHistoryPublicSheet> {

}
