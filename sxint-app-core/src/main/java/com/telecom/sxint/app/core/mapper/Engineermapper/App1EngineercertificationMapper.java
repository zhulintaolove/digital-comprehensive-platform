package com.telecom.sxint.app.core.mapper.Engineermapper;

import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Engineercertification;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-08-19
 */
public interface App1EngineercertificationMapper extends BaseMapper<App1Engineercertification> {
    @Update("UPDATE app1_engineercertification\n" +
            "SET\n" +
            "  rd_profession_level = CASE WHEN TRIM(rd_profession_level) = '' THEN NULL ELSE rd_profession_level END,\n" +
            "  rd_common_level = CASE WHEN TRIM(rd_common_level) = '' THEN NULL ELSE rd_common_level END,\n" +
            "  rd_experience_level = CASE WHEN TRIM(rd_experience_level) = '' THEN NULL ELSE rd_experience_level END;")
    void updatenull1();
    @Update("UPDATE app1_engineercertification\n" +
            "SET\n" +
            "  rd_profession_level = COALESCE(rd_profession_level, ''),\n" +
            "  rd_common_level = COALESCE(rd_common_level, ''),\n" +
            "  rd_experience_level = COALESCE(rd_experience_level, '');")
    void updatenull2();
}
