package com.telecom.sxint.app.core.service.Systemservice.App1countyIndicatorsservice;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1CountyIndicators;
import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1CountyIndicators;

import java.util.List;

/**
 * <p>
 * 县分指标 服务类
 * </p>
 *
 * @author
 * @since 2024-09-02
 */
public interface IApp1CountyIndicatorsService extends IService<App1CountyIndicators> {

    /**
     * 查询所有数据未删除
     * @return
     */
    List<App1CountyIndicators> selectAllData();

    /**
     * 逻辑删除
     */
    void IsDelete(App1CountyIndicators app1CountyIndicators);

}