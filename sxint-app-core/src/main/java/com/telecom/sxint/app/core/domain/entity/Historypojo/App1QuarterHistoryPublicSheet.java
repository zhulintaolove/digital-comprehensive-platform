package com.telecom.sxint.app.core.domain.entity.Historypojo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p >
 *
 * @author
 * @since 2024-09-24
 */
@Data
@ExcelIgnoreUnannotated
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("app1_quarter__history_public_sheet")
@ApiModel(value="App1QuarterHistoryPublicSheet对象", description="")
public class App1QuarterHistoryPublicSheet implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "是否为表的标题属字段")
    private String isField;

    @ApiModelProperty(value = "sheet名称")
    private String sheetName;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 0)
    @ApiModelProperty(value = "字段A")
    private String generalFieldA;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 1)
    @ApiModelProperty(value = "字段B")
    private String generalFieldB;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 2)
    @ApiModelProperty(value = "字段C")
    private String generalFieldC;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 3)
    @ApiModelProperty(value = "字段D")
    private String generalFieldD;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 4)
    @ApiModelProperty(value = "字段E")
    private String generalFieldE;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 5)
    @ApiModelProperty(value = "字段F")
    private String generalFieldF;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 6)
    @ApiModelProperty(value = "字段G")
    private String generalFieldG;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 7)
    @ApiModelProperty(value = "字段H")
    private String generalFieldH;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 8)
    @ApiModelProperty(value = "字段I")
    private String generalFieldI;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 9)
    @ApiModelProperty(value = "字段J")
    private String generalFieldJ;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 10)
    @ApiModelProperty(value = "字段K")
    private String generalFieldK;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 11)
    @ApiModelProperty(value = "字段L")
    private String generalFieldL;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 12)
    @ApiModelProperty(value = "字段M")
    private String generalFieldM;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 13)
    @ApiModelProperty(value = "字段N")
    private String generalFieldN;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 14)
    @ApiModelProperty(value = "字段O")
    private String generalFieldO;

    private String yearMonth;


}