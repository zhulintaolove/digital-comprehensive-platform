package com.telecom.sxint.app.core.service.Pictureservice;


import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.sxint.app.core.domain.entity.Picturepojo.App1Pictureuser;
import com.telecom.sxint.app.core.domain.entity.dto.App1PictureuserDTO;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

public interface IApp1PictureuserService extends IService<App1Pictureuser> {
    /**
     * 用户点击下载(未下载过新增，下载过增加下载次数)
     * @param app1PictureuserDTO
     * @return
     */
    ResponseData insertUserPicture(Integer pictureid, App1PictureuserDTO app1PictureuserDTO, HttpServletResponse response) throws UnsupportedEncodingException;

    /**
     * 返回该用户的总下载次数
     * @param tokenName
     * @return
     */
    List<Map<String,Object>> userAllDownload(String tokenName);
}