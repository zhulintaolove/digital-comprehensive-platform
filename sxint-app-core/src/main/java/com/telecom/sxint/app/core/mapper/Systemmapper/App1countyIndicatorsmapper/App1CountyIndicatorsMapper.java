package com.telecom.sxint.app.core.mapper.Systemmapper.App1countyIndicatorsmapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1CountyIndicators;

/**
 * <p>
 * 县分指标 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-09-02
 */
public interface App1CountyIndicatorsMapper extends BaseMapper<App1CountyIndicators> {

}
