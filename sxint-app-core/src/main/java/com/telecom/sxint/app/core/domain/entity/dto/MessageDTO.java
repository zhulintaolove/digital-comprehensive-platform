package com.telecom.sxint.app.core.domain.entity.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(description = "批量发送信息实体")
public class MessageDTO {

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "接收消息推送的手机号码或者电子邮箱地址")
    private String accNbr;

    @ApiModelProperty(value = "消息内容，可空")
    private String orderContent;

    @ApiModelProperty(value = "请求的时间，格式：yyyy-mm-dd hh24:mi:ss")
    private String requestTime;

    @ApiModelProperty(value = "发送者")
    private String Sender;
}
