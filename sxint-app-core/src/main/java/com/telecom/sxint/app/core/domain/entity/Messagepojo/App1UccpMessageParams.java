package com.telecom.sxint.app.core.domain.entity.Messagepojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2024-10-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("app1_uccp_message_params")
@ApiModel(value="App1UccpMessageParams对象", description="")
public class App1UccpMessageParams implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "请求流水号，格式应为6位的SystemCode+YYYYMMDDHH24MISS+10位的循环序列")
    private String transactionId;

    @ApiModelProperty(value = "UCCP分配的系统编码")
    private String systemCode;

    @ApiModelProperty(value = "UCCP分配的认证密码")
    private String password;

    @ApiModelProperty(value = "UCCP分配的帐号")
    private String userAcct;

    @ApiModelProperty(value = "请求的时间，格式：yyyy-mm-dd hh24:mi:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime requestTime;

    @ApiModelProperty(value = "接收消息推送的手机号码或者电子邮箱地址")
    private String accNbr;

    @ApiModelProperty(value = "本地网/辖区（AccNbr为电子邮件时，LanId可以为空；当AccNbr为手机号码时，LanId不能为空）")
    private String lanId;

    @ApiModelProperty(value = "定时发送时间（格式：yyyy-mm-dd hh24:mi:ss） 默认为空")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime  sendDate;

    @ApiModelProperty(value = "场景标识，统一由UCCP分配")
    private String sceneId;

    @ApiModelProperty(value = "消息内容，可空")
    private String orderContent;

    @ApiModelProperty(value = "发送消息参数，当消息内容为空的时候，内容由UCCP完成，外系统只需要传关键信息即可，比如客户名称等，参数之间用“,”分割。（如果参数内容中含有“，”则需要用“&comma;”代替逗号）")
    private String contentParam;

    @ApiModelProperty(value = "外系统消息流水(查询发送结构)")
    private String extorderId;

    @ApiModelProperty(value = "发送成功/失败")
    private String state;

    @ApiModelProperty(value = "是否删除(1为删除，0为未删)")
    @TableField("is_delete")
    private Integer isDelete;

    @ApiModelProperty(value = "发送者")
    private String Sender;
}
