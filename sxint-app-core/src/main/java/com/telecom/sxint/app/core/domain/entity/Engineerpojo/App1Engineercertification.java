package com.telecom.sxint.app.core.domain.entity.Engineerpojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2024-08-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("app1_engineercertification")
@ApiModel(value="App1Engineercertification对象", description="")
public class App1Engineercertification implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "单位")
    private String company;

    @ApiModelProperty(value = "人员编码")
    private String personnelCode;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "是否研发人员（人才云）")
    private Integer isRdCloud;

    @ApiModelProperty(value = "是否研发人员（基准岗位）")
    private Integer isRdBase;

    @ApiModelProperty(value = "是否通过研发通用认证")
    private Integer isRdCommon;

    @ApiModelProperty(value = "研发通用认证级别")
    private String rdCommonLevel;

    @ApiModelProperty(value = "是否通过研发专业认证")
    private Integer isRdProfession;

    @ApiModelProperty(value = "研发专业认证级别")
    private String rdProfessionLevel;

    @ApiModelProperty(value = "研发经验级别")
    private String rdExperienceLevel;

    @ApiModelProperty(value = "研发工程师验证级别")
    private String engineerLevel;

    @ApiModelProperty(value = "是否删除(1为删除，0为未删)")
    private Integer isDelete;

    @ApiModelProperty(value = "工时")
    private Integer hours;
}
