package com.telecom.sxint.app.core.mapper.Historymapper;

import com.telecom.sxint.app.core.domain.entity.Historypojo.App1MonthlyHistoryPerformanceScoresSummary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-09-24
 */
public interface App1MonthlyHistoryPerformanceScoresSummaryMapper extends BaseMapper<App1MonthlyHistoryPerformanceScoresSummary> {

}
