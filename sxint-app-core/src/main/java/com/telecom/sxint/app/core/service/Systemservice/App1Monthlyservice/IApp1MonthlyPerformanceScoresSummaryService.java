package com.telecom.sxint.app.core.service.Systemservice.App1Monthlyservice;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo.App1MonthlyPerformanceScoresSummary;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
public interface IApp1MonthlyPerformanceScoresSummaryService extends IService<App1MonthlyPerformanceScoresSummary> {
    /**
     * 查找未被删除所以记录
     * @param
     * @return
     */
    List<App1MonthlyPerformanceScoresSummary> getExist();

    /**
     * 查找所有部门名称
     */
    List<String> getAllDepartName();

    /**
     * 逻辑删除
     */
    void deleteById(App1MonthlyPerformanceScoresSummary app1MonthlyPerformanceScoresSummary);
}
