package com.telecom.sxint.app.core.domain.entity.Vatinvoicepojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2024-08-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("app1_vatinvoinceprintlog")
@ApiModel(value="App1Vatinvoinceprintlog对象", description="")
public class App1Vatinvoinceprintlog implements Serializable {

    private static final long serialVersionUID = 1L;



    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "组织")
    private String area;

    @ApiModelProperty(value = "合同号")
    private String account;

    @ApiModelProperty(value = "打印周期")
    private String printmonth;

    @ApiModelProperty(value = "专票流水号")
    private String invoice;

    @ApiModelProperty(value = "金额")
    private String amount;

    @ApiModelProperty(value = "是否已打印")
    private String printflag;

    @ApiModelProperty(value = "录入人员")
    private String operator;

    @ApiModelProperty(value = "创建日期")
    private String created;

    @ApiModelProperty(value = "专票周期")
    private String billmonth;

    @ApiModelProperty(value = "是否删除(1为删除，0为未删)")
    @TableField("is_delete")
    private Integer isDelete;
}
