package com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkEvaluationIndicators;
import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.sxint.app.core.domain.entity.dto.AnalyseDateDTO;
import com.telecom.sxint.app.core.domain.entity.dto.EvaluationIndicatorsDTO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
public interface IApp1YunwangWorkEvaluationIndicatorsService extends IService<App1YunwangWorkEvaluationIndicators> {
    /**
     * 连表查找全部数据
     * @return
     */
    List<EvaluationIndicatorsDTO> selectWithData();
    /**
     * 根据考核项目查询
     * @param evaluationItem
     * @return
     */
    List<App1YunwangWorkEvaluationIndicators> selectByevaluationItem(String evaluationItem);
    /**
     * 连表查询数据
     */
    List<AnalyseDateDTO> selectAnalyseDateWithData();
}
