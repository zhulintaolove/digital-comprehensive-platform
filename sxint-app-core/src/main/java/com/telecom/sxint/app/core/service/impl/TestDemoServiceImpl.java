package com.telecom.sxint.app.core.service.impl;

import com.telecom.sxint.app.core.domain.entity.TestDemo;
import com.telecom.sxint.app.core.mapper.TestDemoMapper;
import com.telecom.sxint.app.core.service.TestDemoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 测试信息表 服务实现类
 * </p>
 *
 * @author sx_tele
 * @since 2024-07-11
 */
@Service
public class TestDemoServiceImpl extends ServiceImpl<TestDemoMapper, TestDemo> implements TestDemoService {

}
