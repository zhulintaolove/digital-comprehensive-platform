package com.telecom.sxint.app.core.domain.entity.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("evaluation_indicators")
@ApiModel(value="AnalyseDateDTO", description="AnalyseDateDTO 对象")
public class AnalyseDateDTO {

//    @ExcelProperty({"2024年区县云网工作评价指标体系","考核项目"})
    @ApiModelProperty(value = "考核项目")
    private String evaluationItem;

//    @ExcelProperty({"2024年区县云网工作评价指标体系","大指标分值"})
    @ApiModelProperty(value = "大指标分值")
    private String mainIndicatorScore;

    //    @ExcelProperty({"2024年7月数据","越城"})
    @ApiModelProperty(value = "越城")
    private String yuecheng;

    //    @ExcelProperty({"2024年7月数据","柯桥"})
    @ApiModelProperty(value = "柯桥")
    private String keqiao;

    //    @ExcelProperty({"2024年7月数据","上虞"})
    @ApiModelProperty(value = "上虞")
    private String shangyu;

    //    @ExcelProperty({"2024年7月数据","嵊州"})
    @ApiModelProperty(value = "嵊州")
    private String shengzhou;

    //    @ExcelProperty({"2024年7月数据","新昌"})
    @ApiModelProperty(value = "新昌")
    private String xinchang;

    //    @ExcelProperty({"2024年7月数据","诸暨"})
    @ApiModelProperty(value = "诸暨")
    private String zhuji;



    public void mergeWith(AnalyseDateDTO other) {
        if (this.evaluationItem.equals(other.getEvaluationItem())) {
            double yuecheng = parseDouble(this.getYuecheng());
            double otheryuecheng = parseDouble(other.getYuecheng());
            this.yuecheng = String.valueOf(yuecheng + otheryuecheng);
            double keqiao = parseDouble(this.getKeqiao());
            double otherkeqiao = parseDouble(other.getKeqiao());
            this.keqiao = String.valueOf(keqiao + otherkeqiao);
            double shangyu = parseDouble(this.getShangyu());
            double othershangyu = parseDouble(other.getShangyu());
            this.shangyu = String.valueOf(shangyu + othershangyu);
            double shengzhou = parseDouble(this.getShengzhou());
            double othershengzhou = parseDouble(other.getShengzhou());
            this.shengzhou = String.valueOf(shengzhou + othershengzhou);
            double xinchang = parseDouble(this.getXinchang());
            double otherxinchang = parseDouble(other.getXinchang());
            this.xinchang = String.valueOf(xinchang + otherxinchang);
            double zhuji = parseDouble(this.getZhuji());
            double otherzhuji = parseDouble(other.getZhuji());
            this.zhuji = String.valueOf(zhuji + otherzhuji);
            double mainIndicatorScore = parseDouble(this.getMainIndicatorScore());
            double othermainIndicatorScore = parseDouble(other.getMainIndicatorScore());
            this.mainIndicatorScore = String.valueOf(mainIndicatorScore + othermainIndicatorScore);
        }
    }
    private double parseDouble(String valueStr) {
        if (valueStr == null || valueStr.trim().isEmpty()) {
            return 0;
        }
        return Double.parseDouble(valueStr);
    }
}
