package com.telecom.sxint.app.core.service.Messageservice;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.Messagepojo.App1UccpMessage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.sxint.app.core.domain.entity.Messagepojo.App1UccpMessageParams;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.domain.entity.dto.MessageQueryDTO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-12-09
 */
public interface IApp1UccpMessageService extends IService<App1UccpMessage> {
    ResponseData<List<App1UccpMessage>> listAll();

    ResponseData<List<App1UccpMessage>> listByPhone(String phoneNumber);

    ResponseData<App1UccpMessage> getById(Integer id);

    ResponseData<App1UccpMessage> create(App1UccpMessage message);

    ResponseData<App1UccpMessage> update(App1UccpMessage message);

    ResponseData delete(Integer id);

    public ResponseData deleteBatch(List<Integer> ids);

    PageData<App1UccpMessage> queryAllMessagesPage(MessageQueryDTO messageQueryDTO);

    PageData<App1UccpMessage> queryAllMessagesPageAsc(MessageQueryDTO messageQueryDTO);

    PageData<App1UccpMessage> queryAllMessagesPageDesc(MessageQueryDTO messageQueryDTO);

}
