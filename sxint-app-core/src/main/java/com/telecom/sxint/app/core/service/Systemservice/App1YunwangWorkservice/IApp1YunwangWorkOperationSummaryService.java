package com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkOperationSummary;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-09-03
 */
public interface IApp1YunwangWorkOperationSummaryService extends IService<App1YunwangWorkOperationSummary> {
    /**
     * 根据考核项目更新数据
     * @param app1YunwangWorkOperationSummary
     */
    void updateByAssessmentItem(App1YunwangWorkOperationSummary app1YunwangWorkOperationSummary);

    /**
     *
     * @return
     */
    List<String> getAssessmentItem();
}
