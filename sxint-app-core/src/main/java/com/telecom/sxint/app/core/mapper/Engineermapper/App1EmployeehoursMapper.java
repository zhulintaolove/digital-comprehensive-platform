package com.telecom.sxint.app.core.mapper.Engineermapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Employeehours;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.domain.entity.dto.HoursQueryDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-08-19
 */
@Mapper
public interface App1EmployeehoursMapper extends BaseMapper<App1Employeehours> {
    @Select("SELECT eh.* " +
            "FROM app1_employeehours eh " +
            "JOIN app1_engineercertification ec ON eh.employee_name = ec.name " +
            "WHERE ec.company = #{company}"+
            "AND eh.is_delete = 0")
    Page<App1Employeehours> selectByCompanyPage(Page<?> page, @Param("company") String company);
}
