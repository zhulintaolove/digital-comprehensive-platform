package com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangPublicSheet;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author
 * @since 2024-09-03
 */
public interface IApp1YunwangPublicSheetService extends IService<App1YunwangPublicSheet> {
    /**
     * 查询所有标题字段
     * @return
     */
    List<App1YunwangPublicSheet> selectAllField();
    /**
     * 根据表名查询数据（包括标题）
     * @return
     */
    List<App1YunwangPublicSheet> selectBySheetName(String sheetName);
    /**
     * 根据sheet名查询标题字段
     * @return
     */
    List<App1YunwangPublicSheet> selectFieldBySheetName(String sheetName);

    /**
     * 查询所有数据
     * @return
     */
    List<App1YunwangPublicSheet> selectAllData();

    /**
     * 根据表名查询数据
     * @return
     */
    List<App1YunwangPublicSheet> selectDataBySheetName(String sheetName);

    /**
     * 查询所有表名
     * @return
     */
    List<String> selectAllSheetNames();
}