package com.telecom.sxint.app.core.service.impl.Systemimpl.App1countyIndicatorsServiceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangPublicSheet;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1QuarterPublicSheet;
import com.telecom.sxint.app.core.mapper.Systemmapper.App1countyIndicatorsmapper.App1QuarterPublicSheetMapper;
import com.telecom.sxint.app.core.service.Systemservice.App1countyIndicatorsservice.IApp1QuarterPublicSheetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author
 * @since 2024-09-03
 */
@Service
public class App1QuarterPublicSheetServiceImpl extends ServiceImpl<App1QuarterPublicSheetMapper, App1QuarterPublicSheet> implements IApp1QuarterPublicSheetService {
    /**
     * 查询所有标题字段
     * @return
     */
    @Override
    public List<App1QuarterPublicSheet> selectAllField() {
        QueryWrapper<App1QuarterPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_field", "true");
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }

    /**
     * 根据表名查询标题字段
     * @param sheetName
     * @return
     */
    @Override
    public List<App1QuarterPublicSheet> selectFieldBySheetName(String sheetName) {
        QueryWrapper<App1QuarterPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sheet_name", sheetName);
        queryWrapper.eq("is_field", "true");
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<App1QuarterPublicSheet> selectAllData() {
        QueryWrapper<App1QuarterPublicSheet> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("is_delete",0);
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<App1QuarterPublicSheet> selectDataBySheetName(String sheetName) {
        QueryWrapper<App1QuarterPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sheet_name", sheetName);
        queryWrapper.eq("is_field", "false").eq("is_delete", 0);
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<String> selectAllSheetNames() {
        // 创建查询包装器
        QueryWrapper<App1QuarterPublicSheet> queryWrapper = new QueryWrapper<>();
        // 选择 distinct sheet_name 和 id
        queryWrapper.select("sheet_name", "id").eq("is_delete", 0);
        // 按照 id 排序
        queryWrapper.orderByAsc("id");
        // 执行查询
        List<App1QuarterPublicSheet> resultList = baseMapper.selectList(queryWrapper);
        // 提取 sheet_name 列到列表中
        List<String> sheetNameList = resultList.stream()
                .map(App1QuarterPublicSheet::getSheetName)
                .distinct()
                .collect(Collectors.toList());
        return sheetNameList;
    }

    /**
     * 逻辑删除
     */
    @Override
    public void IsDelete(App1QuarterPublicSheet app1QuarterPublicSheet) {
        app1QuarterPublicSheet.setIsDelete(1);
        baseMapper.updateById(app1QuarterPublicSheet);
    }
}

