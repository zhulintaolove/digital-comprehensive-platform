package com.telecom.sxint.app.core.domain.entity.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@ExcelIgnoreUnannotated
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@HeadFontStyle(fontHeightInPoints = 18, fontName = "方正小标宋简体")
@HeadRowHeight(30)
@TableName("evaluation_indicators")
@ApiModel(value="EvaluationIndicatorsDTO", description="EvaluationIndicatorsDTO数据导出对象")
public class EvaluationIndicatorsDTO {

    @ColumnWidth(21)
//    @ExcelProperty({"2024年区县云网工作评价指标体系","考核项目"})
    @ExcelProperty(index = 0)
    @ApiModelProperty(value = "考核项目")
    private String evaluationItem;

    @ColumnWidth(14)
//    @ExcelProperty({"2024年区县云网工作评价指标体系","大指标"})
    @ExcelProperty(index = 1)
    @ApiModelProperty(value = "大指标")
    private String mainIndicator;

    @ColumnWidth(18)
//    @ExcelProperty({"2024年区县云网工作评价指标体系","大指标分值"})
    @ExcelProperty(index = 2)
    @ApiModelProperty(value = "大指标分值")
    private String mainIndicatorScore;

    @ColumnWidth(22)
//    @ExcelProperty({"2024年区县云网工作评价指标体系","考核数据的提供部门和取数的系统"})
    @ExcelProperty(index = 3)
    @ApiModelProperty(value = "考核数据的提供部门和取数的系统")
    private String dataProvisionDepartment;

    @ColumnWidth(13)
//    @ExcelProperty({"2024年区县云网工作评价指标体系","考核周期"})
    @ExcelProperty(index = 4)
    @ApiModelProperty(value = "考核周期")
    private String evaluationPeriod;

    @ColumnWidth(20)
//    @ExcelProperty({"2024年区县云网工作评价指标体系","目标值"})
    @ExcelProperty(index = 5)
    @ApiModelProperty(value = "目标值")
    private String targetValue;

    @ColumnWidth(33)
//    @ExcelProperty({"2024年区县云网工作评价指标体系","指标定义（解释）"})
    @ExcelProperty(index = 6)
    @ApiModelProperty(value = "指标定义（解释）")
    private String indicatorDefinition;

    @ColumnWidth(28)
//    @ExcelProperty({"2024年区县云网工作评价指标体系","扣加分方法"})
    @ExcelProperty(index = 7)
    @ApiModelProperty(value = "扣加分方法")
    private String deductionAdditionMethod;

    //    @ExcelProperty({"2024年7月数据","越城"})
    @ExcelProperty(index = 8)
    @ApiModelProperty(value = "越城")
    private String yuecheng;

    //    @ExcelProperty({"2024年7月数据","柯桥"})
    @ExcelProperty(index = 9)
    @ApiModelProperty(value = "柯桥")
    private String keqiao;

    //    @ExcelProperty({"2024年7月数据","上虞"})
    @ExcelProperty(index = 10)
    @ApiModelProperty(value = "上虞")
    private String shangyu;

    //    @ExcelProperty({"2024年7月数据","嵊州"})
    @ExcelProperty(index = 11)
    @ApiModelProperty(value = "嵊州")
    private String shengzhou;

    //    @ExcelProperty({"2024年7月数据","新昌"})
    @ExcelProperty(index = 12)
    @ApiModelProperty(value = "新昌")
    private String xinchang;

    //    @ExcelProperty({"2024年7月数据","诸暨"})
    @ExcelProperty(index = 13)
    @ApiModelProperty(value = "诸暨")
    private String zhuji;
}