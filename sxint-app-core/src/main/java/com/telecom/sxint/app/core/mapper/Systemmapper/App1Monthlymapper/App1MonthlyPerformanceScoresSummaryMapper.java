package com.telecom.sxint.app.core.mapper.Systemmapper.App1Monthlymapper;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo.App1MonthlyPerformanceScoresSummary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
public interface App1MonthlyPerformanceScoresSummaryMapper extends BaseMapper<App1MonthlyPerformanceScoresSummary> {

}
