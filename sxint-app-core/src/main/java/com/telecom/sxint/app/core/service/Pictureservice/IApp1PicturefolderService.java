package com.telecom.sxint.app.core.service.Pictureservice;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.Picturepojo.App1Picturefolder;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p >
 *
 * @author
 * @since 2024-12-03
 */
public interface IApp1PicturefolderService extends IService<App1Picturefolder> {
    ResponseData<List<App1Picturefolder>> listAll();



    ResponseData<App1Picturefolder> create(App1Picturefolder folder);

    ResponseData<App1Picturefolder> update(App1Picturefolder folder);

    ResponseData delete(Integer id);
}