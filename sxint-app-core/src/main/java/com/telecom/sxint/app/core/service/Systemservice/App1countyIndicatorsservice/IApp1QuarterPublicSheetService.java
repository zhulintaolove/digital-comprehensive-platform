package com.telecom.sxint.app.core.service.Systemservice.App1countyIndicatorsservice;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1QuarterPublicSheet;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author
 * @since 2024-09-03
 */
public interface IApp1QuarterPublicSheetService extends IService<App1QuarterPublicSheet> {
    /**
     * 查询所有标题字段
     * @return
     */
    List<App1QuarterPublicSheet> selectAllField();

    /**
     * 根据sheet名查询标题字段
     * @return
     */
    List<App1QuarterPublicSheet> selectFieldBySheetName(String sheetName);

    /**
     * 查询所有数据
     * @return
     */
    List<App1QuarterPublicSheet> selectAllData();

    /**
     * 根据表名查询数据
     * @return
     */
    List<App1QuarterPublicSheet> selectDataBySheetName(String sheetName);

    /**
     * 查询所有表名
     * @return
     */
    List<String> selectAllSheetNames();

    /**
     * 逻辑删除
     * @return
     */
    void IsDelete(App1QuarterPublicSheet app1QuarterPublicSheet);

}