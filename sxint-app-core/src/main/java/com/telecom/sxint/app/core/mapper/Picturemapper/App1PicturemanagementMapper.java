package com.telecom.sxint.app.core.mapper.Picturemapper;

import com.telecom.sxint.app.core.domain.entity.Picturepojo.App1Picturemanagement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 事件管理信息表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
public interface App1PicturemanagementMapper extends BaseMapper<App1Picturemanagement> {
    @Insert({
            "<script>",
            "INSERT INTO app1_picturemanagement (department,main_personnel,event_name,event_time,event_location,tags，event_type,download_count,link,event) ",
            "VALUES ",
            "<foreach collection='list' item='item' index='index' separator=','>",
            "(#{item.department}, #{item.mainPersonnel}, #{item.eventName}, #{item.eventTime}, #{item.eventLocation}, #{item.tags}, #{item.eventType}, #{item.downloadCount}, #{item.link}, #{item.event})", // 对象属性
            "</foreach>",
            "</script>"
    })
    void insertBatch(@Param("list") List<App1Picturemanagement> app1Picturemanagements);
}
