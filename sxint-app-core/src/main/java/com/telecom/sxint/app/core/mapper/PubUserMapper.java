package com.telecom.sxint.app.core.mapper;

import com.telecom.sxint.app.core.domain.entity.PubUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-08-14
 */
public interface PubUserMapper extends BaseMapper<PubUser> {

}
