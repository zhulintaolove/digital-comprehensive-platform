package com.telecom.sxint.app.core.domain.entity.Engineerpojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2024-08-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("app1_examschedule")
@ApiModel(value="App1Examschedule对象", description="")
public class App1Examschedule implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "考试ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "考试等级")
    private String examLevel;

    @ApiModelProperty(value = "考试日期")
    private String examDate;

    @ApiModelProperty(value = "报名开始日期")
    private String  registrationStartDate;

    @ApiModelProperty(value = "报名截止日期")
    private String   registrationDeadline;

    @ApiModelProperty(value = "考试信息描述")
    private String description;

    @ApiModelProperty(value = "考试场次")
    private String examEvent;
}
