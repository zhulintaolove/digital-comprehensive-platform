package com.telecom.sxint.app.core.mapper.Historymapper;

import com.telecom.sxint.app.core.domain.entity.Historypojo.App1HistoryAssessment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-09-27
 */
public interface App1HistoryAssessmentMapper extends BaseMapper<App1HistoryAssessment> {

}
