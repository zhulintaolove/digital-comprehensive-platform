package com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author
 * @since 2024-08-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("app1_yunwang_work_evaluation_indicators_data")
@ApiModel(value="App1YunwangWorkEvaluationIndicatorsData对象", description="")
@ColumnWidth(21)
@ExcelIgnoreUnannotated
@HeadFontStyle(fontHeightInPoints = 18, fontName = "方正小标宋简体")
@HeadRowHeight(30)
public class App1YunwangWorkEvaluationIndicatorsData implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ExcelProperty({"2024年区县云网工作评价指标体系","考核项目"})
    @ApiModelProperty(value = "考核项目")
    private String evaluationItem;

    @ExcelProperty({"2024年区县云网工作评价指标体系","大指标"})
    @ApiModelProperty(value = "大指标")
    private String mainIndicator;

    @ExcelProperty({"2024年区县云网工作评价指标体系","考核数据的提供部门和取数的系统"})
    @ApiModelProperty(value = "考核数据的提供部门和取数的系统")
    private String dataProvisionDepartment;

    @ExcelProperty({"2024年7月数据","越城"})
    @ApiModelProperty(value = "越城")
    private String yuecheng;

    @NumberFormat("##.##")
    @ExcelProperty({"2024年7月数据","柯桥"})
    @ApiModelProperty(value = "柯桥")
    private String keqiao;

    @NumberFormat("##.##")
    @ExcelProperty({"2024年7月数据","上虞"})
    @ApiModelProperty(value = "上虞")
    private String shangyu;

    @NumberFormat("##.##")
    @ExcelProperty({"2024年7月数据","嵊州"})
    @ApiModelProperty(value = "嵊州")
    private String shengzhou;

    @NumberFormat("##.##")
    @ExcelProperty({"2024年7月数据","新昌"})
    @ApiModelProperty(value = "新昌")
    private String xinchang;

    @NumberFormat("##.##")
    @ExcelProperty({"2024年7月数据","诸暨"})
    @ApiModelProperty(value = "诸暨")
    private String zhuji;


}