package com.telecom.sxint.app.core.service.impl.Engineerserviceimpl;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Examschedule;
import com.telecom.sxint.app.core.mapper.Engineermapper.App1ExamscheduleMapper;
import com.telecom.sxint.app.core.service.Engineerservice.IApp1ExamscheduleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-08-20
 */
@Service
public class App1ExamscheduleServiceImpl extends ServiceImpl<App1ExamscheduleMapper, App1Examschedule> implements IApp1ExamscheduleService {
    @Autowired
    private App1ExamscheduleMapper app1ExamscheduleMapper;
    /**
     * 清空
     */
    @Override
    public void deleteAll() {

        app1ExamscheduleMapper.delete(null);
    }
    /**
     * 条件查询
     * @param queryWrapper
     * @return
     */
    @Override
    public List<App1Examschedule> selectByMultipleConditions(QueryWrapper<App1Examschedule> queryWrapper) {
        return app1ExamscheduleMapper.selectList(queryWrapper);
    }

    @Override
    public App1Examschedule updateByEmployeeId(App1Examschedule app1Examschedule) {
        UpdateWrapper<App1Examschedule> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", app1Examschedule.getId());
        app1ExamscheduleMapper.update(app1Examschedule,updateWrapper);
        QueryWrapper<App1Examschedule> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("id",app1Examschedule.getId());
        App1Examschedule log1=app1ExamscheduleMapper.selectOne(queryWrapper);
        return log1;
    }

    @Override
    public ResponseData clearData() {
        app1ExamscheduleMapper.delete(null);
        return ResponseData.success("清除成功");
    }

    @Override
    public void DeleteById(Integer id) {
        app1ExamscheduleMapper.deleteById(id);
    }
}
