package com.telecom.sxint.app.core.service.Historyservice;

import com.telecom.sxint.app.core.domain.entity.Historypojo.App1MonthlyHistoryPerformanceScoresSummary;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-09-24
 */
public interface IApp1MonthlyHistoryPerformanceScoresSummaryService extends IService<App1MonthlyHistoryPerformanceScoresSummary> {
    /**
     * 修改
     * @param app1MonthlyHistoryPerformanceScoresSummary
     * @return
     */
    boolean updateByIdAndDate(App1MonthlyHistoryPerformanceScoresSummary app1MonthlyHistoryPerformanceScoresSummary);
    /**
     * 查找月度历史记录（根据月份）
     */
    List<App1MonthlyHistoryPerformanceScoresSummary> getByMonth(String month);
}
