package com.telecom.sxint.app.core.mapper.Systemmapper.App1YunwangWorkmapper;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkEvaluationIndicators;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.telecom.sxint.app.core.domain.entity.dto.AnalyseDateDTO;
import com.telecom.sxint.app.core.domain.entity.dto.EvaluationIndicatorsDTO;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
public interface App1YunwangWorkEvaluationIndicatorsMapper extends BaseMapper<App1YunwangWorkEvaluationIndicators> {
    @Select("SELECT ai.*, ad.yuecheng, ad.keqiao, ad.shangyu, ad.shengzhou, ad.xinchang, ad.zhuji " +
            "FROM app1_yunwang_work_evaluation_indicators ai " +
            "INNER JOIN app1_yunwang_work_evaluation_indicators_data ad " +
            "ON ai.evaluation_item = ad.evaluation_item " +
            "AND ai.main_indicator = ad.main_indicator " +
            "AND ai.data_provision_department = ad.data_provision_department " +
            "order by ai.id asc")
    List<EvaluationIndicatorsDTO> selectWithData();
    @Select("SELECT ai.main_indicator_score, ad.evaluation_item, ad.yuecheng, ad.keqiao, ad.shangyu, ad.shengzhou, ad.xinchang, ad.zhuji " +
            "FROM app1_yunwang_work_evaluation_indicators ai " +
            "INNER JOIN app1_yunwang_work_evaluation_indicators_data ad " +
            "ON ai.evaluation_item = ad.evaluation_item " +
            "AND ai.main_indicator = ad.main_indicator " +
            "AND ai.data_provision_department = ad.data_provision_department")
    List<AnalyseDateDTO> selectWithAnalyseDate();
}
