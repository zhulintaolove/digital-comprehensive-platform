package com.telecom.sxint.app.core.mapper.Picturemapper;

import com.telecom.sxint.app.core.domain.entity.Picturepojo.App1Picturefolder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-12-03
 */
public interface App1PicturefolderMapper extends BaseMapper<App1Picturefolder> {

}
