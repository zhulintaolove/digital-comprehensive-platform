package com.telecom.sxint.app.core.domain.entity.dto;

import javax.validation.constraints.NotBlank;

public class QueryParamDTO {
    @NotBlank(message = "会话id不可为空")
    private String conversationId;

    @NotBlank(message = "问题不可为空")
    private String text;

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
