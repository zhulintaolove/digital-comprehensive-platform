package com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2024-08-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ExcelIgnoreUnannotated
@TableName("app1_assessment")
@ApiModel(value="App1Assessment对象", description="")
public class App1Assessment implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "被考核部门")
    private String assessedDepartment;

    @ColumnWidth(10)
    @ExcelProperty(index = 0)
    @ApiModelProperty(value = "分类")
    private String category;

    @ColumnWidth(15)
    @ExcelProperty(index = 1)
    @ApiModelProperty(value = "考核项目")
    private String assessmentItem;

    @ColumnWidth(10)
    @ExcelProperty(index = 2)
    @ApiModelProperty(value = "权重")
    private String weight;

    @ColumnWidth(10)
    @ExcelProperty(index = 3)
    @ApiModelProperty(value = "目标值")
    private String target;

    @ColumnWidth(30)
    @ExcelProperty(index = 4)
    @ApiModelProperty(value = "计分办法")
    private String selfScoringMethod;

    @ColumnWidth(10)
    @ExcelProperty(index = 5)
    @ApiModelProperty(value = "备注")
    private String notes;

    @ApiModelProperty(value = "目标值")
    @ColumnWidth(10)
    @ExcelProperty(index = 6)
    private String targetValue;

    @ColumnWidth(10)
    @ExcelProperty(index = 7)
    @ApiModelProperty(value = "完成值")
    private String completionValue;

    @ColumnWidth(10)
    @ExcelProperty(index = 8)
    @ApiModelProperty(value = "完成率考核排名")
    private String completionRateRank;

    @ColumnWidth(10)
    @ExcelProperty(index = 9)
    @ApiModelProperty(value = "得分")
    private String score;

    @ApiModelProperty(value = "是否删除(1为删除，0为未删)")
    @TableField("is_delete")
    private Integer isDelete;

}
