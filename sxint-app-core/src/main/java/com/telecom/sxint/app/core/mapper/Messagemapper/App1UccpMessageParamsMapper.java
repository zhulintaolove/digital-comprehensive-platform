package com.telecom.sxint.app.core.mapper.Messagemapper;

import com.telecom.sxint.app.core.domain.entity.Messagepojo.App1UccpMessageParams;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-10-25
 */
public interface App1UccpMessageParamsMapper extends BaseMapper<App1UccpMessageParams> {

}
