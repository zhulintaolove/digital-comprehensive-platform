package com.telecom.sxint.app.core.service;

import com.telecom.sxint.app.core.domain.entity.TestDemo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 测试信息表 服务类
 * </p>
 *
 * @author sx_tele
 * @since 2024-07-11
 */
public interface TestDemoService extends IService<TestDemo> {

}
