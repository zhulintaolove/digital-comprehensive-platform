package com.telecom.sxint.app.core.domain.entity.Engineerpojo;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.data.redis.core.RedisHash;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2024-08-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@HeadFontStyle(fontHeightInPoints = 10, fontName = "方正小标宋简体")
@HeadRowHeight(25)
@ExcelIgnoreUnannotated
@TableName("app1_employeehours")
@ApiModel(value="App1Employeehours对象", description="")
public class App1Employeehours implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ExcelProperty("工时月份")
    @ApiModelProperty(value = "工时月份")
    @TableField("work_month")
    private String workMonth;

    @ExcelProperty("成员姓名")
    @ApiModelProperty(value = "成员姓名")
    @TableField("employee_name")
    private String employeeName;

    @ExcelProperty("成员ID")
    @ApiModelProperty(value = "成员id")
    @TableField("employee_id")
    private String employeeId;


    @ExcelProperty("复核工时（天）")
    @ApiModelProperty(value = "复核工时（天）")
    @TableField("reviewed_hours")
    private Double reviewedHours;

    @ApiModelProperty(value = "是否删除(1为删除，0为未删)")
    @TableField("is_delete")
    private Integer isDelete;

}
