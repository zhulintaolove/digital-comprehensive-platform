package com.telecom.sxint.app.core.domain.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "InvoiceFilter", description = "专票查询过滤条件")
public class InvoiceFilterDTO {

    @ApiModelProperty(value = "是否已打印")
    private String printflag;

    @ApiModelProperty(value = "组织")
    private String area;

    @ApiModelProperty(value = "合同号")
    private String account;

    // 其他查询参数...
    @ApiModelProperty(value = "打印周期")
    private String printmonth;

    @ApiModelProperty(value = "专票流水号")
    private String invoice;

    @ApiModelProperty(value = "录入人员")
    private String operator;

    @ApiModelProperty(value = "创建日期")
    private String created;

    @ApiModelProperty(value = "专票周期")
    private String billmonth;


}