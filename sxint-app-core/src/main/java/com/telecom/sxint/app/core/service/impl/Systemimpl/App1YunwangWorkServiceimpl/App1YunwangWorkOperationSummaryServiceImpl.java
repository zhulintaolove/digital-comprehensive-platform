package com.telecom.sxint.app.core.service.impl.Systemimpl.App1YunwangWorkServiceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkOperationSummary;
import com.telecom.sxint.app.core.mapper.Systemmapper.App1YunwangWorkmapper.App1YunwangWorkOperationSummaryMapper;
import com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice.IApp1YunwangWorkOperationSummaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-09-03
 */
@Service
public class App1YunwangWorkOperationSummaryServiceImpl extends ServiceImpl<App1YunwangWorkOperationSummaryMapper, App1YunwangWorkOperationSummary> implements IApp1YunwangWorkOperationSummaryService {
    @Autowired
    private App1YunwangWorkOperationSummaryMapper app1YunwangWorkOperationSummaryMapper;
    /**
     * 根据考核项目更新数据
     * @param app1YunwangWorkOperationSummary
     */
    @Override
    public void updateByAssessmentItem(App1YunwangWorkOperationSummary app1YunwangWorkOperationSummary) {
        UpdateWrapper<App1YunwangWorkOperationSummary> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("assessment_item", app1YunwangWorkOperationSummary.getAssessmentItem());
        app1YunwangWorkOperationSummaryMapper.update(app1YunwangWorkOperationSummary, updateWrapper);
    }
    /**
     * 获取所有考核项目名称
     * @return List<String>
     */
    @Override
    public List<String> getAssessmentItem() {
        QueryWrapper<App1YunwangWorkOperationSummary> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("assessment_item", "汇总");
        queryWrapper.select("assessment_item");
        List<App1YunwangWorkOperationSummary> list = app1YunwangWorkOperationSummaryMapper.selectList(queryWrapper);
        return list.stream()
                .map(App1YunwangWorkOperationSummary::getAssessmentItem)
                .collect(Collectors.toList());
    }
}

