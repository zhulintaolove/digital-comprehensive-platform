package com.telecom.sxint.app.core.service.impl.Messageimpl;

import cn.hutool.core.collection.CollUtil;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Employeehours;
import com.telecom.sxint.app.core.domain.entity.Messagepojo.App1UccpMessage;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.domain.entity.dto.MessageQueryDTO;
import com.telecom.sxint.app.core.mapper.Messagemapper.App1UccpMessageMapper;
import com.telecom.sxint.app.core.service.Messageservice.IApp1UccpMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-12-09
 */
@Service
public class App1UccpMessageServiceImpl extends ServiceImpl<App1UccpMessageMapper, App1UccpMessage> implements IApp1UccpMessageService {
    @Autowired
    private App1UccpMessageMapper uccpMessageMapper;

    @Override
    public ResponseData<List<App1UccpMessage>> listAll() {
        QueryWrapper<App1UccpMessage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0);
        List<App1UccpMessage> messages = uccpMessageMapper.selectList(queryWrapper);
        if (messages == null || messages.isEmpty()) {
            return ResponseData.failed("没有找到短信记录","");
        }
        return ResponseData.success(messages);
    }

    @Override
    public ResponseData<List<App1UccpMessage>> listByPhone(String phoneNumber) {
        List<App1UccpMessage> messages = uccpMessageMapper.selectList(new com.baomidou.mybatisplus.core.conditions.query.QueryWrapper<App1UccpMessage>()
                .eq("phone_number", phoneNumber)
                .eq("is_delete", 0));
        if (messages == null || messages.isEmpty()) {
            return ResponseData.failed("未找到该手机号的短信记录","");
        }
        return ResponseData.success(messages);
    }

    @Override
    public ResponseData<App1UccpMessage> getById(Integer id) {
        QueryWrapper<App1UccpMessage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", id);
        App1UccpMessage message = uccpMessageMapper.selectOne(queryWrapper);
        if (message == null) {
            return ResponseData.failed("未找到该短信记录","");
        }
        return ResponseData.success(message);
    }

    @Override
    public ResponseData<App1UccpMessage> create(App1UccpMessage message) {
        boolean isSaved = this.save(message);
        if (!isSaved) {
            return ResponseData.failed("短信记录创建失败","");
        }
        return ResponseData.success(message);
    }

    @Override
    public ResponseData<App1UccpMessage> update(App1UccpMessage message) {
        boolean isUpdated = this.updateById(message);
        if (!isUpdated) {
            return ResponseData.failed("短信记录更新失败","");
        }
        return ResponseData.success(message);
    }

    @Override
    public ResponseData delete(Integer id) {
        App1UccpMessage message = uccpMessageMapper.selectById(id);
        if (message == null) {
            return ResponseData.failed("未找到该短信记录","");
        }
        message.setIsDelete(1);
        boolean isDeleted = this.updateById(message);
        if (!isDeleted) {
            return ResponseData.failed("短信记录删除失败","");
        }
        return ResponseData.success("删除成功");
    }


    /**
     * 批量删除研究人员信息
     * @param ids
     * @return
     */
    @Override
    public ResponseData deleteBatch(List<Integer> ids) {
        if(CollUtil.isEmpty(ids)){
            return ResponseData.failed("未选择删除记录","410003");
        }
        UpdateWrapper<App1UccpMessage> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in("id", ids) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1
        uccpMessageMapper.update(null, updateWrapper);
        return ResponseData.success("删除成功");
    }


    @Override
    public PageData<App1UccpMessage> queryAllMessagesPage(MessageQueryDTO messageQueryDTO) {
        // 使用 MyBatis-Plus 的 Page 来处理分页
        Page<App1UccpMessage> page = new Page<>(messageQueryDTO.getCurrentPage(), messageQueryDTO.getPageSize()); // 使用 `getCurrentPage()` 和 `getPageSize()`

        // 构建查询条件
        QueryWrapper<App1UccpMessage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0);  // 确保只查询未删除的记录

        // 根据手机号过滤（如果提供了手机号）
        if (messageQueryDTO.getPhoneNumber() != null && !messageQueryDTO.getPhoneNumber().isEmpty()) {
            queryWrapper.like("phone_number", messageQueryDTO.getPhoneNumber());  // 按手机号模糊查询
        }

        // 排序，按发送时间倒序排列
//        queryWrapper.orderByDesc("storage_time");

        // 执行分页查询
        Page<App1UccpMessage> resultPage = uccpMessageMapper.selectPage(page, queryWrapper);

        // 将查询结果封装到 PageData 对象中
        PageData<App1UccpMessage> pageData = new PageData<>();
        pageData.setTotal(resultPage.getTotal());  // 设置总记录数
        pageData.setRecords(resultPage.getRecords());  // 设置当前页记录
        pageData.setCurrentPage(messageQueryDTO.getCurrentPage());  // 当前页
        pageData.setPageSize(messageQueryDTO.getPageSize());  // 每页数据量

        return pageData;
    }

    @Override
    public PageData<App1UccpMessage> queryAllMessagesPageAsc(MessageQueryDTO messageQueryDTO) {
        // 使用 MyBatis-Plus 的 Page 来处理分页
        Page<App1UccpMessage> page = new Page<>(messageQueryDTO.getCurrentPage(), messageQueryDTO.getPageSize()); // 使用 `getCurrentPage()` 和 `getPageSize()`

        // 构建查询条件
        QueryWrapper<App1UccpMessage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0);  // 确保只查询未删除的记录

        // 根据手机号过滤（如果提供了手机号）
        if (messageQueryDTO.getPhoneNumber() != null && !messageQueryDTO.getPhoneNumber().isEmpty()) {
            queryWrapper.like("phone_number", messageQueryDTO.getPhoneNumber());  // 按手机号模糊查询
        }

        // 排序，按发送时间倒序排列
        queryWrapper.orderByAsc("storage_time");

        // 执行分页查询
        Page<App1UccpMessage> resultPage = uccpMessageMapper.selectPage(page, queryWrapper);

        // 将查询结果封装到 PageData 对象中
        PageData<App1UccpMessage> pageData = new PageData<>();
        pageData.setTotal(resultPage.getTotal());  // 设置总记录数
        pageData.setRecords(resultPage.getRecords());  // 设置当前页记录
        pageData.setCurrentPage(messageQueryDTO.getCurrentPage());  // 当前页
        pageData.setPageSize(messageQueryDTO.getPageSize());  // 每页数据量

        return pageData;
    }
    @Override
    public PageData<App1UccpMessage> queryAllMessagesPageDesc(MessageQueryDTO messageQueryDTO) {
        // 使用 MyBatis-Plus 的 Page 来处理分页
        Page<App1UccpMessage> page = new Page<>(messageQueryDTO.getCurrentPage(), messageQueryDTO.getPageSize()); // 使用 `getCurrentPage()` 和 `getPageSize()`

        // 构建查询条件
        QueryWrapper<App1UccpMessage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0);  // 确保只查询未删除的记录

        // 根据手机号过滤（如果提供了手机号）
        if (messageQueryDTO.getPhoneNumber() != null && !messageQueryDTO.getPhoneNumber().isEmpty()) {
            queryWrapper.like("phone_number", messageQueryDTO.getPhoneNumber());  // 按手机号模糊查询
        }

        // 排序，按发送时间倒序排列
        queryWrapper.orderByDesc("storage_time");

        // 执行分页查询
        Page<App1UccpMessage> resultPage = uccpMessageMapper.selectPage(page, queryWrapper);

        // 将查询结果封装到 PageData 对象中
        PageData<App1UccpMessage> pageData = new PageData<>();
        pageData.setTotal(resultPage.getTotal());  // 设置总记录数
        pageData.setRecords(resultPage.getRecords());  // 设置当前页记录
        pageData.setCurrentPage(messageQueryDTO.getCurrentPage());  // 当前页
        pageData.setPageSize(messageQueryDTO.getPageSize());  // 每页数据量

        return pageData;
    }

}
