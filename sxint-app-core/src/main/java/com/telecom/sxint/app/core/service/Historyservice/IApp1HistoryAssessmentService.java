package com.telecom.sxint.app.core.service.Historyservice;

import com.telecom.sxint.app.core.domain.entity.Historypojo.App1HistoryAssessment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-09-27
 */
public interface IApp1HistoryAssessmentService extends IService<App1HistoryAssessment> {
    /**
     * 根据部门名称查找
     * @param department
     * @return
     */
    List<App1HistoryAssessment> selectWithDepartment(String department, String month);

    /**
     *
     * @return
     */
    List<String> selectAllDepartment();
}
