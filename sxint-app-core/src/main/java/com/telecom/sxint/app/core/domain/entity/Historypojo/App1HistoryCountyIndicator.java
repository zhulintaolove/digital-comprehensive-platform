package com.telecom.sxint.app.core.domain.entity.Historypojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2024-10-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("app1_history_county_indicator")
@ApiModel(value="App1HistoryCountyIndicator对象", description="")
public class App1HistoryCountyIndicator implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "指标名称")
    private String indicatorName;

    private String score;

    private String assessmentMet;

    private String provider;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Integer isDelete;

    private String yearMonth;


}
