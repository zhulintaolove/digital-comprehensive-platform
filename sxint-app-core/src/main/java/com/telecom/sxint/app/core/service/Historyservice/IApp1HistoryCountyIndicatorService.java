package com.telecom.sxint.app.core.service.Historyservice;

import com.telecom.sxint.app.core.domain.entity.Historypojo.App1HistoryCountyIndicator;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-10-15
 */
public interface IApp1HistoryCountyIndicatorService extends IService<App1HistoryCountyIndicator> {

}
