package com.telecom.sxint.app.core.domain.entity.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.telecom.sxint.app.core.domain.entity.Query.PageBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;


@Data
@ApiModel(description = "图片分页查询实体")
public class PictureQueryDTO extends PageBase implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "事件发生的部门")
    private String department;

    @ApiModelProperty(value = "事件的主要人物")
    private String mainPersonnel;

    @ApiModelProperty(value = "事件名称")
    private String eventName;

    @ApiModelProperty(value = "事件发生的时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime eventTime;

    @ApiModelProperty(value = "事件发生的地点")
    private String eventLocation;

    @ApiModelProperty(value = "事件的标签")
    private String tags;

    @ApiModelProperty(value = "事件类型")
    private String eventType;

    @ApiModelProperty(value = "资源的下载次数，默认为0")
    private Integer downloadCount;

    @ApiModelProperty(value = "媒体链接，可以是图片或视频的URL")
    private String link;




}