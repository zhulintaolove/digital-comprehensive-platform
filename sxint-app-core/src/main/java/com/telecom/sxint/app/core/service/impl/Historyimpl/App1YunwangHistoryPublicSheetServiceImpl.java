package com.telecom.sxint.app.core.service.impl.Historyimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Historypojo.App1YunwangHistoryPublicSheet;
import com.telecom.sxint.app.core.mapper.Historymapper.App1YunwangHistoryPublicSheetMapper;
import com.telecom.sxint.app.core.service.Historyservice.IApp1YunwangHistoryPublicSheetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-09-24
 */
@Service
public class App1YunwangHistoryPublicSheetServiceImpl extends ServiceImpl<App1YunwangHistoryPublicSheetMapper, App1YunwangHistoryPublicSheet> implements IApp1YunwangHistoryPublicSheetService {
    /**
     * 查询所有标题字段
     * @return
     */
    @Override
    public List<App1YunwangHistoryPublicSheet> selectAllField() {
        QueryWrapper<App1YunwangHistoryPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_field", "true");
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }
    /**
     * 根据表名查询数据（包括标题）
     * @return
     */
    @Override
    public List<App1YunwangHistoryPublicSheet> selectBySheetName(String sheetName) {
        QueryWrapper<App1YunwangHistoryPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sheet_name", sheetName)
                .orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }
    /**
     * 根据sheet名查询标题字段
     * @return
     */
    @Override
    public List<App1YunwangHistoryPublicSheet> selectFieldBySheetName(String sheetName) {
        QueryWrapper<App1YunwangHistoryPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sheet_name", sheetName);
        queryWrapper.eq("is_field", "true");
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }/**
     * 查询所有数据
     * @return
     */
    @Override
    public List<App1YunwangHistoryPublicSheet> selectAllData() {
        QueryWrapper<App1YunwangHistoryPublicSheet> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("is_field","false");
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }
    /**
     * 根据表名查询数据
     * @return
     */
    @Override
    public List<App1YunwangHistoryPublicSheet> selectDataBySheetName(String sheetName,String month) {
        QueryWrapper<App1YunwangHistoryPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sheet_name", sheetName);
        queryWrapper.eq("is_field", "false");
        queryWrapper.eq("year_month",month);
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }
    /**
     * 查询所有表名
     * @return
     */
    @Override
    public List<String> selectAllSheetNames() {
        // 创建查询包装器
        QueryWrapper<App1YunwangHistoryPublicSheet> queryWrapper = new QueryWrapper<>();
        // 选择 distinct sheet_name 和 id
        queryWrapper.select("sheet_name", "id");
        // 按照 id 排序
        queryWrapper.orderByAsc("id");
        // 执行查询
        List<App1YunwangHistoryPublicSheet> resultList = baseMapper.selectList(queryWrapper);
        // 提取 sheet_name 列到列表中
        List<String> sheetNameList = resultList.stream()
                .map(App1YunwangHistoryPublicSheet::getSheetName)
                .distinct()
                .collect(Collectors.toList());
        return sheetNameList;
    }
}
