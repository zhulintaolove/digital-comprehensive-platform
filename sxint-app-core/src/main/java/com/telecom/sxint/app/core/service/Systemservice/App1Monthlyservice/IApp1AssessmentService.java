package com.telecom.sxint.app.core.service.Systemservice.App1Monthlyservice;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo.App1Assessment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-08-27
 */
public interface IApp1AssessmentService extends IService<App1Assessment> {
    /**
     * 根据部门名称查找
     * @param department
     * @return
     */
    List<App1Assessment> selectWithDepartment(String department);

    /**
     *
     * @return
     */
    List<String> selectAllDepartment();
    /**
     * 删除
     * @param app1Assessment
     */
    void deleteById(App1Assessment app1Assessment);

}

