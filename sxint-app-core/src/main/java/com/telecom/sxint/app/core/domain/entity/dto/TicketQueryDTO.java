package com.telecom.sxint.app.core.domain.entity.dto;

import com.telecom.sxint.app.core.domain.entity.Query.PageBase;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;


@Data
@ApiModel(description = "专票分页查询实体")
public class TicketQueryDTO extends PageBase implements Serializable {






}