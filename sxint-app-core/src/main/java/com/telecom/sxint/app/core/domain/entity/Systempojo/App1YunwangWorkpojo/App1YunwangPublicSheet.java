package com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author
 * @since 2024-09-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ExcelIgnoreUnannotated
@TableName("app1_yunwang_public_sheet")
@ApiModel(value="App1YunwangPublicSheet对象", description="")
public class App1YunwangPublicSheet implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "是否为表的标题属性字段")
    private String isField;

    @ApiModelProperty(value = "sheet名称")
    private String sheetName;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 0)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldA;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 1)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldB;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 2)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldC;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 3)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldD;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 4)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldE;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 5)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldF;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 6)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldG;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 7)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldH;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 8)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldI;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 9)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldJ;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 10)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldK;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 11)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldL;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 12)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldM;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 13)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldN;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ExcelProperty(index = 14)
    @ApiModelProperty(value = "通用字段名")
    private String generalFieldO;
    @ApiModelProperty(value = "是否删除(1为删除，0为未删)")
    @TableField("is_delete")
    private Integer isDelete;

}
