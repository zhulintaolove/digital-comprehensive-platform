package com.telecom.sxint.app.core.mapper.Historymapper;

import com.telecom.sxint.app.core.domain.entity.Historypojo.App1HistoryCountyIndicator;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-10-15
 */
public interface App1HistoryCountyIndicatorMapper extends BaseMapper<App1HistoryCountyIndicator> {

}
