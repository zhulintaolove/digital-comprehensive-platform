package com.telecom.sxint.app.core.mapper.Picturemapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.telecom.sxint.app.core.domain.entity.Picturepojo.App1Pictureuser;

public interface App1PictureuserMapper extends BaseMapper<App1Pictureuser> {
}