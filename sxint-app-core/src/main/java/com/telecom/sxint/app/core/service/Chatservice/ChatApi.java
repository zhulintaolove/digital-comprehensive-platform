package com.telecom.sxint.app.core.service.Chatservice;

import com.alibaba.fastjson2.JSONObject;
import com.telecom.sxint.app.core.Constant.ChatConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

@Component
public class ChatApi {

    @Autowired(required = false)
    private RestTemplate restTemplate=new RestTemplate();
//    private RestTemplate restTemplate=new RestTemplate();
    private final HttpHeaders headers = new HttpHeaders();

    public ChatApi() {
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(ChatConstants.bearerToken);
    }

    public static void main(String[] args) {
        ChatApi chatApi = new ChatApi();
        JSONObject jsonObject = chatApi.runConversation("cb6216ac-d626-4448-addd-23a727fd15bf", "介绍一下自己");
        System.out.println(chatApi.getCleanAnswer(jsonObject));
    }

    public JSONObject startConversation() {
        String apiUrl = ChatConstants.baseUrl + "/v2/app/conversation";
        JSONObject params = new JSONObject();
        params.put("app_id", ChatConstants.appId);
        ResponseEntity<JSONObject> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.POST, new HttpEntity<>(params, headers), JSONObject.class);
        if (responseEntity.getStatusCode().is2xxSuccessful() && responseEntity.getBody() != null) {
            return responseEntity.getBody();
        } else {
            throw new RuntimeException("对话ID获取失败" + responseEntity.getBody());
        }
    }

    /**
     * 对一个不加载任何知识库的应用开始会话
     * @return
     */
    public JSONObject startCommonConversation() {
        String apiUrl = ChatConstants.baseUrl + "/v2/app/conversation";
        JSONObject params = new JSONObject();
        params.put("app_id", ChatConstants.commonAppId);
        ResponseEntity<JSONObject> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.POST, new HttpEntity<>(params, headers), JSONObject.class);
        if (responseEntity.getStatusCode().is2xxSuccessful() && responseEntity.getBody() != null) {
            return responseEntity.getBody();
        } else {
            throw new RuntimeException("对话ID获取失败" + responseEntity.getBody());
        }
    }

    public JSONObject runConversation(String conversationId, String query) {
        String apiUrl = ChatConstants.baseUrl + "/v2/app/conversation/runs";
        JSONObject params = new JSONObject();
        params.put("app_id", ChatConstants.appId);
        params.put("query", query);
        params.put("stream", false);
        params.put("conversation_id", conversationId);
        ResponseEntity<JSONObject> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.POST, new HttpEntity<>(params, headers), JSONObject.class);
        if (responseEntity.getStatusCode().is2xxSuccessful() && responseEntity.getBody() != null) {
            return responseEntity.getBody();
        } else {
            throw new RuntimeException("对话请求失败" + responseEntity.getBody());
        }
    }

    /**
     * 对一个不加载任何知识库的应用进行会话
     * @param conversationId
     * @param query
     * @return
     */
    public JSONObject runCommonConversation(String conversationId, String query) {
        String apiUrl = ChatConstants.baseUrl + "/v2/app/conversation/runs";
        JSONObject params = new JSONObject();
        params.put("app_id", ChatConstants.commonAppId);
        params.put("query", query);
        params.put("stream", false);
        params.put("conversation_id", conversationId);
        ResponseEntity<JSONObject> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.POST, new HttpEntity<>(params, headers), JSONObject.class);
        if (responseEntity.getStatusCode().is2xxSuccessful() && responseEntity.getBody() != null) {
            return responseEntity.getBody();
        } else {
            throw new RuntimeException("对话请求失败" + responseEntity.getBody());
        }
    }

    public String getCleanAnswer(JSONObject chatResponse) {
        String pattern = "\\^(\\[\\d+])+\\^?";
        return chatResponse.getString("answer").replaceAll(pattern, "");
    }
}
