package com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author
 * @since 2024-09-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("app1_yunwang_work_operation_summary")
@ApiModel(value="App1YunwangWorkOperationSummary对象", description="")
public class App1YunwangWorkOperationSummary implements Serializable {

    private static final long serialVersionUID = 1L;

    private String assessmentItem;

    private BigDecimal yuecheng;

    private BigDecimal keqiao;

    private BigDecimal shangyu;

    private BigDecimal shengzhou;

    private BigDecimal xinchang;

    private BigDecimal zhuji;


}