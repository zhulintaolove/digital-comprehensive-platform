package com.telecom.sxint.app.core.service.Historyservice;

import com.telecom.sxint.app.core.domain.entity.Historypojo.App1QuarterHistoryPublicSheet;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-09-24
 */
public interface IApp1QuarterHistoryPublicSheetService extends IService<App1QuarterHistoryPublicSheet> {
    /**
     * 查询所有标题字段
     * @return
     */
    List<App1QuarterHistoryPublicSheet> selectAllField();

    /**
     * 根据sheet名查询标题字段
     * @return
     */
    List<App1QuarterHistoryPublicSheet> selectFieldBySheetName(String sheetName);

    /**
     * 查询所有数据
     * @return
     */
    List<App1QuarterHistoryPublicSheet> selectAllData();

    /**
     * 根据表名查询数据
     * @return
     */
    List<App1QuarterHistoryPublicSheet> selectDataBySheetName(String sheetName,String month);

    /**
     * 查询所有表名
     * @return
     */
    List<String> selectAllSheetNames();
}
