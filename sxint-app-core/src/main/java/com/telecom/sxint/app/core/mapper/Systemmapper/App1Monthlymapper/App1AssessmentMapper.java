package com.telecom.sxint.app.core.mapper.Systemmapper.App1Monthlymapper;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo.App1Assessment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-08-27
 */
public interface App1AssessmentMapper extends BaseMapper<App1Assessment> {

}
