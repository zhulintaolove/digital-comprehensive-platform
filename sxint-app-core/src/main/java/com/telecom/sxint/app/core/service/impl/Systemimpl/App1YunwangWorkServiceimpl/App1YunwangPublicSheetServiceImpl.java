package com.telecom.sxint.app.core.service.impl.Systemimpl.App1YunwangWorkServiceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangPublicSheet;
import com.telecom.sxint.app.core.mapper.Systemmapper.App1YunwangWorkmapper.App1YunwangPublicSheetMapper;
import com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice.IApp1YunwangPublicSheetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-09-03
 */
@Service
public class App1YunwangPublicSheetServiceImpl extends ServiceImpl<App1YunwangPublicSheetMapper, App1YunwangPublicSheet> implements IApp1YunwangPublicSheetService {
    /**
     * 查询所有标题字段
     * @return
     */
    @Override
    public List<App1YunwangPublicSheet> selectAllField() {
        QueryWrapper<App1YunwangPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_field", "true");
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }
    /**
     * 根据表名查询数据（包括标题）
     * @return
     */
    @Override
    public List<App1YunwangPublicSheet> selectBySheetName(String sheetName) {
        QueryWrapper<App1YunwangPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sheet_name", sheetName)
                .eq("is_delete",0)
                .orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }
    /**
     * 根据sheet名查询标题字段
     * @return
     */
    @Override
    public List<App1YunwangPublicSheet> selectFieldBySheetName(String sheetName) {
        QueryWrapper<App1YunwangPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sheet_name", sheetName);
        queryWrapper.eq("is_field", "true");
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }
    /**
     * 查询所有数据
     * @return
     */
    @Override
    public List<App1YunwangPublicSheet> selectAllData() {
        QueryWrapper<App1YunwangPublicSheet> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("is_field","false");
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }
    /**
     * 根据表名查询数据
     * @return
     */
    @Override
    public List<App1YunwangPublicSheet> selectDataBySheetName(String sheetName) {
        QueryWrapper<App1YunwangPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sheet_name", sheetName);
        queryWrapper.eq("is_field", "false").eq("is_delete", 0);
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }
    /**
     * 查询所有表名
     * @return
     */
    @Override
    public List<String> selectAllSheetNames() {
        // 创建查询包装器
        QueryWrapper<App1YunwangPublicSheet> queryWrapper = new QueryWrapper<>();
        // 选择 distinct sheet_name 和 id
        queryWrapper.select("sheet_name", "id").eq("is_delete", 0);
        // 按照 id 排序
        queryWrapper.orderByAsc("id");
        // 执行查询
        List<App1YunwangPublicSheet> resultList = baseMapper.selectList(queryWrapper);
        // 提取 sheet_name 列到列表中
        List<String> sheetNameList = resultList.stream()
                .map(App1YunwangPublicSheet::getSheetName)
                .distinct()
                .collect(Collectors.toList());
        return sheetNameList;
    }
}
