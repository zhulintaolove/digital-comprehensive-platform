package com.telecom.sxint.app.core.service.impl.Engineerserviceimpl;

import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1ExamRegistrationUser;
import com.telecom.sxint.app.core.mapper.Engineermapper.App1ExamRegistrationUserMapper;
import com.telecom.sxint.app.core.service.Engineerservice.IApp1ExamRegistrationUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-10-22
 */
@Service
public class App1ExamRegistrationUserServiceImpl extends ServiceImpl<App1ExamRegistrationUserMapper, App1ExamRegistrationUser> implements IApp1ExamRegistrationUserService {

}
