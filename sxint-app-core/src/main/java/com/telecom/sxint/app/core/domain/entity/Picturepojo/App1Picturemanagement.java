package com.telecom.sxint.app.core.domain.entity.Picturepojo;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 事件管理信息表
 * </p>
 *
 * @author
 * @since 2024-08-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("app1_picturemanagement")
@ApiModel(value="App1Picturemanagement对象", description="事件管理信息表")
public class App1Picturemanagement implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "事件发生的部门")
    private String department;

    @ApiModelProperty(value = "事件的主要人物")
    private String mainPersonnel;

    @ApiModelProperty(value = "事件名称")
    private String eventName;

    @ApiModelProperty(value = "事件发生的时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime eventTime;

    @ApiModelProperty(value = "事件发生的地点")
    private String eventLocation;

    @ApiModelProperty(value = "事件的标签")
    private String tags;

    @ApiModelProperty(value = "事件类型")
    private String eventType;

    @ApiModelProperty(value = "资源的下载次数，默认为0")
    private Integer downloadCount;

    @ApiModelProperty(value = "媒体链接，可以是图片或视频的URL")
    private String link;

    @ApiModelProperty(value = "事件类型")
    private String event;

    @ApiModelProperty(value = "是否删除(1为删除，0为未删)")
    @TableField("is_delete")
    private Integer isDelete;

}