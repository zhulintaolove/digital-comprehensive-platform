package com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 县分指标
 * </p>
 *
 * @author 
 * @since 2024-09-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ExcelIgnoreUnannotated
@TableName("app1_county_indicators")
@ApiModel(value="App1CountyIndicators对象", description="县分指标")
public class App1CountyIndicators implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelProperty({"附件",""})  // 虚拟列，不填充实际数据
    private static final String dummyColumn="";

    @ExcelProperty({"区县网运线季度考核指标","指标名称"})
    @ApiModelProperty(value = "指标名称")
    private String indicatorName;

    @ExcelProperty({"区县网运线季度考核指标","分值"})
    @ApiModelProperty(value = "分值")
    private String score;

    @ExcelProperty({"区县网运线季度考核指标","具体的考核办法"})
    @ApiModelProperty(value = "具体的考核办法")
    private String assessmentMethod;

    @ExcelProperty({"","提供人"})
    @ApiModelProperty(value = "提供人")
    private String provider;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "是否删除(1为删除，0为未删)")
    @TableField("is_delete")
    private Integer isDelete;


}
