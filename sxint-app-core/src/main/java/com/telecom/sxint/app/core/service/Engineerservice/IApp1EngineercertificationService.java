package com.telecom.sxint.app.core.service.Engineerservice;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Engineercertification;
import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.domain.entity.dto.CertificationQueryDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-08-19
 */
public interface IApp1EngineercertificationService extends IService<App1Engineercertification> {
    /**
     * 修改明细
     * @param app1Engineercertification
     * @return
     */
    App1Engineercertification updateByIdrdate(App1Engineercertification app1Engineercertification);

    /**
     * 根据条件查询工程师数据
     * @param filter
     * @return
     */
    List<App1Engineercertification> queryByConditions(App1Engineercertification filter);
    /**
     * 明细表分页查询
     * @param certificationQueryDTO
     * @return
     */
    PageData queryCertificationPage(CertificationQueryDTO certificationQueryDTO);
    /**
     * 根据组织分页查询
     * @param certificationQueryDTO
     * @return
     */
    PageData queryByCompanyPage(CertificationQueryDTO certificationQueryDTO,String company);
    /**
     * 根据ID查询工程师数据
     * @param id
     * @return
     */
    App1Engineercertification queryById(Integer id);
    /**
     *获取研发人员各等级区县分布
     * @param level
     * @return
     */
    List<Map<String, Object>> getCompanyCountByLevel(Integer level);

    /**
     *获取研发人员认证级别缺失情况
     * @return
     */
    List<Map<String, Object>> getLostSituation();

    /*
    *查询所有工程师数据
    */
    List<App1Engineercertification> queryAllCertification();
    /**
     * 批量删除研究人员信息
     * @param ids
     * @return
     */
    void deletByIds(List<Integer> ids);

    /**
     * 新增明细
     * @param app1Engineercertification
     * @return
     */
    ResponseData addCertification(App1Engineercertification app1Engineercertification);

}
