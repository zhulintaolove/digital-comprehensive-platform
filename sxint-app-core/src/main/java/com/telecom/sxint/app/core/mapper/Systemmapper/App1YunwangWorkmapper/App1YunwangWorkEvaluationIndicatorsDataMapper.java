package com.telecom.sxint.app.core.mapper.Systemmapper.App1YunwangWorkmapper;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkEvaluationIndicatorsData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
@Mapper
public interface App1YunwangWorkEvaluationIndicatorsDataMapper extends BaseMapper<App1YunwangWorkEvaluationIndicatorsData> {

}
