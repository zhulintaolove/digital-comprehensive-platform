package com.telecom.sxint.app.core.mapper.Messagemapper;
import com.telecom.sxint.app.core.domain.entity.Messagepojo.App1UccpMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-12-09
 */
public interface App1UccpMessageMapper extends BaseMapper<App1UccpMessage> {

}
