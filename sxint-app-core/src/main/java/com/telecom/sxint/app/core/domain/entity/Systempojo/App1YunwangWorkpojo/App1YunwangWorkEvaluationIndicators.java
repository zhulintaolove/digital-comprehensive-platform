package com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@ExcelIgnoreUnannotated
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@HeadFontStyle(fontHeightInPoints = 18, fontName = "方正小标宋简体")
@HeadRowHeight(30)
@TableName("app1_yunwang_work_evaluation_indicators")
@ApiModel(value="App1YunwangWorkEvaluationIndicators对象", description="")
public class App1YunwangWorkEvaluationIndicators implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    @ColumnWidth(21)
    @ExcelProperty({"2024年区县云网工作评价指标体系","考核项目"})
    @ApiModelProperty(value = "考核项目")
    private String evaluationItem;

    @ColumnWidth(14)
    @ExcelProperty({"2024年区县云网工作评价指标体系","大指标"})
    @ApiModelProperty(value = "大指标")
    private String mainIndicator;

    @ColumnWidth(18)
    @ExcelProperty({"2024年区县云网工作评价指标体系","大指标分值"})
    @ApiModelProperty(value = "大指标分值")
    private String mainIndicatorScore;

    @ColumnWidth(22)
    @ExcelProperty({"2024年区县云网工作评价指标体系","考核数据的提供部门和取数的系统"})
    @ApiModelProperty(value = "考核数据的提供部门和取数的系统")
    private String dataProvisionDepartment;

    @ColumnWidth(13)
    @ExcelProperty({"2024年区县云网工作评价指标体系","考核周期"})
    @ApiModelProperty(value = "考核周期")
    private String evaluationPeriod;

    @ColumnWidth(20)
    @ExcelProperty({"2024年区县云网工作评价指标体系","目标值"})
    @ApiModelProperty(value = "目标值")
    private String targetValue;

    @ColumnWidth(33)
    @ExcelProperty({"2024年区县云网工作评价指标体系","指标定义（解释）"})
    @ApiModelProperty(value = "指标定义（解释）")
    private String indicatorDefinition;

    @ColumnWidth(28)
    @ExcelProperty({"2024年区县云网工作评价指标体系","扣加分方法"})
    @ApiModelProperty(value = "扣加分方法")
    private String deductionAdditionMethod;

    @ApiModelProperty(value = "是否删除(1为删除，0为未删)")
    @TableField("is_delete")
    private Integer isDelete;


}