package com.telecom.sxint.app.core.service.impl.Pictureimpl;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Picturepojo.App1Picturefolder;
import com.telecom.sxint.app.core.mapper.Picturemapper.App1PicturefolderMapper;
import com.telecom.sxint.app.core.service.Pictureservice.IApp1PicturefolderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p >
 *
 * @author
 * @since 2024-12-03
 */
@Service
public class App1PicturefolderServiceImpl extends ServiceImpl<App1PicturefolderMapper, App1Picturefolder> implements IApp1PicturefolderService {
    @Autowired
    private App1PicturefolderMapper pictureFolderMapper;

    @Override
    public ResponseData<List<App1Picturefolder>> listAll() {
        List<App1Picturefolder> folders = pictureFolderMapper.selectList(new QueryWrapper<>());
        return ResponseData.success(folders);
    }
    @Override
    public ResponseData<App1Picturefolder> create(App1Picturefolder folder) {
        pictureFolderMapper.insert(folder);
        return ResponseData.success(folder);
    }

    @Override
    public ResponseData<App1Picturefolder> update(App1Picturefolder folder) {
        pictureFolderMapper.updateById(folder);
        return ResponseData.success(folder);
    }

    @Override
    public ResponseData delete(Integer id) {
        pictureFolderMapper.deleteById(id);
        return ResponseData.success("删除成功");
    }
}