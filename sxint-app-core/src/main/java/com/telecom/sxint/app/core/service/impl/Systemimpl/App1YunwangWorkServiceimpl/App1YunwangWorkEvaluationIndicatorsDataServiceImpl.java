package com.telecom.sxint.app.core.service.impl.Systemimpl.App1YunwangWorkServiceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkEvaluationIndicatorsData;
import com.telecom.sxint.app.core.mapper.Systemmapper.App1YunwangWorkmapper.App1YunwangWorkEvaluationIndicatorsDataMapper;
import com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice.IApp1YunwangWorkEvaluationIndicatorsDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author
 * @since 2024-08-23
 */
@Service
public class App1YunwangWorkEvaluationIndicatorsDataServiceImpl extends ServiceImpl<App1YunwangWorkEvaluationIndicatorsDataMapper, App1YunwangWorkEvaluationIndicatorsData> implements IApp1YunwangWorkEvaluationIndicatorsDataService {
    @Autowired
    private App1YunwangWorkEvaluationIndicatorsDataMapper app1YunwangWorkEvaluationIndicatorsDataMapper;

    /**
     * 查询所有考核项目
     * @param
     * @return
     */
    @Override
    public List<String> selectAllevaluationItem() {
        QueryWrapper<App1YunwangWorkEvaluationIndicatorsData> queryWrapper=new QueryWrapper<>();
        queryWrapper.select("evaluation_item");
        List<App1YunwangWorkEvaluationIndicatorsData> list=app1YunwangWorkEvaluationIndicatorsDataMapper.selectList(queryWrapper);
        return list.stream().map(App1YunwangWorkEvaluationIndicatorsData::getEvaluationItem).distinct().collect(Collectors.toList());
    }

    /**
     * 查询所有考核指标根据id排序
     * @param
     * @return
     */
    @Override
    public List<App1YunwangWorkEvaluationIndicatorsData> getAllByAsc() {
        List<App1YunwangWorkEvaluationIndicatorsData> list=new ArrayList<>();
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.orderByAsc("id");
        return app1YunwangWorkEvaluationIndicatorsDataMapper.selectList(queryWrapper);
    }

    /**
     * 根据考核项目查询
     * @param evaluationItem
     * @return
     */
    @Override
    public List<App1YunwangWorkEvaluationIndicatorsData> selectByevaluationItem(String evaluationItem) {
        QueryWrapper<App1YunwangWorkEvaluationIndicatorsData> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("evaluation_item",evaluationItem);
        List<App1YunwangWorkEvaluationIndicatorsData> list=app1YunwangWorkEvaluationIndicatorsDataMapper.selectList(queryWrapper);
        return list;
    }
    /**
     * 单条修改
     * @param app1YunwangWorkEvaluationIndicatorsData
     * @return
     */
    @Override
    public App1YunwangWorkEvaluationIndicatorsData updateByItemAndIndicator(App1YunwangWorkEvaluationIndicatorsData app1YunwangWorkEvaluationIndicatorsData) {
        UpdateWrapper<App1YunwangWorkEvaluationIndicatorsData> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("evaluation_item", app1YunwangWorkEvaluationIndicatorsData.getEvaluationItem())
                .eq("main_indicator", app1YunwangWorkEvaluationIndicatorsData.getMainIndicator());
        app1YunwangWorkEvaluationIndicatorsDataMapper.update(app1YunwangWorkEvaluationIndicatorsData, updateWrapper);
        return app1YunwangWorkEvaluationIndicatorsData;
    }
    /**
     * 根据条件查询
     * @param filter
     * @return
     */
    @Override
    public List<App1YunwangWorkEvaluationIndicatorsData> queryByConditions(App1YunwangWorkEvaluationIndicatorsData filter) {
        QueryWrapper<App1YunwangWorkEvaluationIndicatorsData> queryWrapper = new QueryWrapper<>();

        if (filter.getEvaluationItem() != null && !filter.getEvaluationItem().isEmpty()) {
            queryWrapper.eq("evaluation_item", filter.getEvaluationItem());
        }
        if (filter.getMainIndicator() != null && !filter.getMainIndicator().isEmpty()) {
            queryWrapper.eq("main_indicator", filter.getMainIndicator());
        }
        if (filter.getDataProvisionDepartment() != null && !filter.getDataProvisionDepartment().isEmpty()) {
            queryWrapper.eq("data_provision_department", filter.getDataProvisionDepartment());
        }
        if (filter.getYuecheng() != null&& !filter.getYuecheng().isEmpty()) {
            queryWrapper.eq("yuecheng", filter.getYuecheng());
        }
        if (filter.getKeqiao() != null&& !filter.getKeqiao().isEmpty()) {
            queryWrapper.eq("keqiao", filter.getKeqiao());
        }
        if (filter.getShangyu() != null&& !filter.getShangyu().isEmpty()) {
            queryWrapper.eq("shangyu", filter.getShangyu());
        }
        if (filter.getShengzhou() != null&& !filter.getShengzhou().isEmpty()) {
            queryWrapper.eq("shengzhou", filter.getShengzhou());
        }
        if (filter.getXinchang() != null&& !filter.getXinchang().isEmpty()) {
            queryWrapper.eq("xinchang", filter.getXinchang());
        }
        if (filter.getZhuji() != null&& !filter.getZhuji().isEmpty()) {
            queryWrapper.eq("zhuji", filter.getZhuji());
        }


        return app1YunwangWorkEvaluationIndicatorsDataMapper.selectList(queryWrapper);
    }
}