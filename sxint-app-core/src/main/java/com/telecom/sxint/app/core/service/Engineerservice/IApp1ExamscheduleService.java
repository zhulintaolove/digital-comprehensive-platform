package com.telecom.sxint.app.core.service.Engineerservice;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Examschedule;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-08-20
 */
public interface IApp1ExamscheduleService extends IService<App1Examschedule> {
    /**
     * 清空
     */
    void deleteAll();
    /**
     *条件查询
     * @param queryWrapper
     * @return
     */
    List<App1Examschedule> selectByMultipleConditions(QueryWrapper<App1Examschedule> queryWrapper);

    /**
     * 根据id修改考情信息
     * @param app1Examschedule
     * @return
     */
    App1Examschedule updateByEmployeeId(App1Examschedule app1Examschedule);

    /**
     * 清空数据
     * @param
     * @return
     */
    ResponseData clearData();

    /**
     * 单条删除
     * @param id
     */
    void DeleteById(Integer id);
}
