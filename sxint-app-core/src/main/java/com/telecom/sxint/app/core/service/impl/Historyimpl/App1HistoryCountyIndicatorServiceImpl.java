package com.telecom.sxint.app.core.service.impl.Historyimpl;

import com.telecom.sxint.app.core.domain.entity.Historypojo.App1HistoryCountyIndicator;
import com.telecom.sxint.app.core.mapper.Historymapper.App1HistoryCountyIndicatorMapper;
import com.telecom.sxint.app.core.service.Historyservice.IApp1HistoryCountyIndicatorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-10-15
 */
@Service
public class App1HistoryCountyIndicatorServiceImpl extends ServiceImpl<App1HistoryCountyIndicatorMapper, App1HistoryCountyIndicator> implements IApp1HistoryCountyIndicatorService {

}
