package com.telecom.sxint.app.core.domain.entity.Query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class PageData<T> extends PageBase {

    public PageData(PageBase pageBase) {
        this.setCurrentPage(pageBase.getCurrentPage());
        this.setPageSize(pageBase.getPageSize());
        this.setDoSearchTotal(pageBase.getDoSearchTotal());
    }

    public PageData(PageBase pageBase, List<T> records, Long total) {
        this.setCurrentPage(pageBase.getCurrentPage());
        this.setPageSize(pageBase.getPageSize());
        this.setDoSearchTotal(pageBase.getDoSearchTotal());
        this.records = records;
        this.total = total;
    }

    private List<T> records;
    private Long total;

}