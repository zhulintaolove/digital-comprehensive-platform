package com.telecom.sxint.app.core.domain.entity.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.telecom.sxint.app.core.domain.entity.Query.PageBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author
 * @since 2024-10-25
 */
@Data
@ApiModel(description = "信息分页查询实体")
public class UccpMessageParamsDTO extends PageBase implements Serializable {

}
