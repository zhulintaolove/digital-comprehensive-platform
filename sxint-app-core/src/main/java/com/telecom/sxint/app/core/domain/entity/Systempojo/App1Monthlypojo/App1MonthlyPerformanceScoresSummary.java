package com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("app1_monthly_performance_scores_summary")
@ApiModel(value="App1MonthlyPerformanceScoresSummary对象", description="")
public class App1MonthlyPerformanceScoresSummary implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "序号")
    @TableId(value = "sequence_number", type = IdType.AUTO)
    private Integer sequenceNumber;

    @ApiModelProperty(value = "简称")
    private String abbreviation;

    @ApiModelProperty(value = "部门")
    private String departmentName;

    @ApiModelProperty(value = "公共KPI指标，保留两位小数需要计算")
    private BigDecimal publicKpiScore;

    @ApiModelProperty(value = "职能工作考核")
    private BigDecimal functionalWorkScore;

    @ApiModelProperty(value = "GS考核")
    private BigDecimal gsScore;

    @ApiModelProperty(value = "党支部堡垒指数考核")
    private BigDecimal partyBranchIndex;

    @ApiModelProperty(value = "倒三角支撑")
    private BigDecimal reverseSupportScore;

    @ApiModelProperty(value = "合计，需要计算")
    private BigDecimal totalScore;

    @ApiModelProperty(value = "排名，可能包含特殊字符或格式，使用VARCHAR,需要计算")
    private Integer ranking;

    @ApiModelProperty(value = "是否删除(1为删除，0为未删)")
    @TableField("is_delete")
    private Integer isDelete;

}
