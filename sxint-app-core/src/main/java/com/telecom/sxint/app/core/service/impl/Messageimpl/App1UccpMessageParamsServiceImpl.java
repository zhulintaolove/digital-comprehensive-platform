package com.telecom.sxint.app.core.service.impl.Messageimpl;

import cn.hutool.core.collection.CollUtil;
import cn.zjtele.pubinfo.boot.redis.config.RedisHelper;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Employeehours;
import com.telecom.sxint.app.core.domain.entity.Messagepojo.App1UccpMessageParams;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.domain.entity.dto.UccpMessageParamsDTO;
import com.telecom.sxint.app.core.mapper.Messagemapper.App1UccpMessageParamsMapper;
import com.telecom.sxint.app.core.service.Messageservice.IApp1UccpMessageParamsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author
 * @since 2024-10-25
 */
@Service
public class App1UccpMessageParamsServiceImpl extends ServiceImpl<App1UccpMessageParamsMapper, App1UccpMessageParams> implements IApp1UccpMessageParamsService {

    @Autowired
    private App1UccpMessageParamsMapper app1UccpMessageParamsMapper;
    @Autowired
    private RedisHelper redisHelper;
    String cacheKey = "UccpMessageParams:list";

    /**
     * 查询全部
     * @param
     * @return
     */
    @Override
    public ResponseData<List<App1UccpMessageParams>> queryAllUccp() {
        QueryWrapper<App1UccpMessageParams> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete",0);
        List<App1UccpMessageParams> list = app1UccpMessageParamsMapper.selectList(queryWrapper);
        return ResponseData.success(list);
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @Override
    public ResponseData<App1UccpMessageParams> queryUccpById(Integer id) {
        App1UccpMessageParams app1UccpMessageParams = app1UccpMessageParamsMapper.selectById(id);
        return ResponseData.success(app1UccpMessageParams);
    }

    /**
     * 新增
     */
    @Override
    public ResponseData<App1UccpMessageParams> insertUccp(App1UccpMessageParams app1UccpMessageParams) {
        app1UccpMessageParamsMapper.insert(app1UccpMessageParams);
        redisHelper.delete(cacheKey);
        return ResponseData.success(app1UccpMessageParams);
    }

    /**
     * 修改
     */
    @Override
    public ResponseData<App1UccpMessageParams> updateUccp(App1UccpMessageParams app1UccpMessageParams) {
        app1UccpMessageParamsMapper.updateById(app1UccpMessageParams);
        redisHelper.delete(cacheKey);
        return ResponseData.success(app1UccpMessageParams);
    }

    /**
     * 删除
     */
    @Override
    public ResponseData<App1UccpMessageParams> deleteUccp(Integer id) {
        //isDelete为1表示删除
        App1UccpMessageParams app1UccpMessageParams = app1UccpMessageParamsMapper.selectById(id);
        app1UccpMessageParams.setIsDelete(1);
        app1UccpMessageParamsMapper.updateById(app1UccpMessageParams);
        redisHelper.delete(cacheKey);
        return ResponseData.success();
    }

    /**
     * 分页查询
     * @param
     * @return
     */
    @Override
    public PageData queryUccpPage(UccpMessageParamsDTO uccpMessageParamsDTO) {

        // 1. 构建查询条件
        Page<App1UccpMessageParams> page = Page.of(uccpMessageParamsDTO.getCurrentPage(), uccpMessageParamsDTO.getPageSize());

        // 2. 分页查询
        // 由于不需要查询条件，直接使用selectAll()方法查询所有数据
        Page<App1UccpMessageParams> p = lambdaQuery()
                .select()
                .eq(App1UccpMessageParams::getIsDelete, 0)
                .page(page);

        // 3. 封装VO结果
        PageData<App1UccpMessageParams> dto = new PageData<>();
        // 总条数
        dto.setTotal(p.getTotal());
        // 当前页
        dto.setCurrentPage(p.getCurrent());
        dto.setPageSize(p.getSize());
        // 当前页数条数
        dto.setRecords(p.getRecords());

        // 4. 返回
        return dto;
    }

    @Override
    public PageData queryUccpPageAsc(UccpMessageParamsDTO uccpMessageParamsDTO) {
        // 1. 构建查询条件
        Page<App1UccpMessageParams> page = Page.of(uccpMessageParamsDTO.getCurrentPage(), uccpMessageParamsDTO.getPageSize());
        // 2. 分页查询
        // 由于不需要查询条件，直接使用selectAll()方法查询所有数据
        Page<App1UccpMessageParams> p = lambdaQuery()
                .select()
                .eq(App1UccpMessageParams::getIsDelete, 0)
                //根据sendDate来升序排序
                .orderByAsc(App1UccpMessageParams::getRequestTime)
                .page(page);

        // 3. 封装VO结果
        PageData<App1UccpMessageParams> dto = new PageData<>();
        // 总条数
        dto.setTotal(p.getTotal());
        // 当前页
        dto.setCurrentPage(p.getCurrent());
        dto.setPageSize(p.getSize());
        // 当前页数条数
        dto.setRecords(p.getRecords());

        // 4. 返回
        return dto;
    }

    @Override
    public PageData queryUccpPageDesc(UccpMessageParamsDTO uccpMessageParamsDTO) {
        // 1. 构建查询条件
        Page<App1UccpMessageParams> page = Page.of(uccpMessageParamsDTO.getCurrentPage(), uccpMessageParamsDTO.getPageSize());
        // 2. 分页查询
        // 由于不需要查询条件，直接使用selectAll()方法查询所有数据
        Page<App1UccpMessageParams> p = lambdaQuery()
                .select()
                .eq(App1UccpMessageParams::getIsDelete, 0)
                //根据sendDate来升序排序
                .orderByDesc(App1UccpMessageParams::getRequestTime)
                .page(page);

        // 3. 封装VO结果
        PageData<App1UccpMessageParams> dto = new PageData<>();
        // 总条数
        dto.setTotal(p.getTotal());
        // 当前页
        dto.setCurrentPage(p.getCurrent());
        dto.setPageSize(p.getSize());
        // 当前页数条数
        dto.setRecords(p.getRecords());

        // 4. 返回
        return dto;
    }
    @Override
    public PageData queryAllMessagesPageByCondition(String condition,UccpMessageParamsDTO uccpMessageParamsDTO) {
        // 1. 构建查询条件
        Page<App1UccpMessageParams> page = Page.of(uccpMessageParamsDTO.getCurrentPage(), uccpMessageParamsDTO.getPageSize());
        // 2. 分页查询
        // 由于不需要查询条件，直接使用selectAll()方法查询所有数据
        Page<App1UccpMessageParams> p = lambdaQuery()
                .select()
                .eq(App1UccpMessageParams::getIsDelete, 0)
                .like(App1UccpMessageParams::getSender, condition)
                .page(page);

        // 3. 封装VO结果
        PageData<App1UccpMessageParams> dto = new PageData<>();
        // 总条数
        dto.setTotal(p.getTotal());
        // 当前页
        dto.setCurrentPage(p.getCurrent());
        dto.setPageSize(p.getSize());
        // 当前页数条数
        dto.setRecords(p.getRecords());

        // 4. 返回
        return dto;
    }
}