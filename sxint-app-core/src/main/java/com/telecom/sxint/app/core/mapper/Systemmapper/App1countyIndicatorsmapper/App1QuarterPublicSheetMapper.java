package com.telecom.sxint.app.core.mapper.Systemmapper.App1countyIndicatorsmapper;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1QuarterPublicSheet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author
 * @since 2024-09-03
 */
public interface App1QuarterPublicSheetMapper extends BaseMapper<App1QuarterPublicSheet> {

}