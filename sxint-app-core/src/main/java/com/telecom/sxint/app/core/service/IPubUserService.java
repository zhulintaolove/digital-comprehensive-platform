package com.telecom.sxint.app.core.service;

import com.telecom.sxint.app.core.domain.entity.PubUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-08-14
 */
public interface IPubUserService extends IService<PubUser> {

}
