package com.telecom.sxint.app.core.mapper.Systemmapper.App1YunwangWorkmapper;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkOperationSummary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author
 * @since 2024-09-03
 */
public interface App1YunwangWorkOperationSummaryMapper extends BaseMapper<App1YunwangWorkOperationSummary> {

}