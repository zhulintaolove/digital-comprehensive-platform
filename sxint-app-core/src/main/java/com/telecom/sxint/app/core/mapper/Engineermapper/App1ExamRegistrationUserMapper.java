package com.telecom.sxint.app.core.mapper.Engineermapper;

import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1ExamRegistrationUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-10-22
 */
public interface App1ExamRegistrationUserMapper extends BaseMapper<App1ExamRegistrationUser> {

}
