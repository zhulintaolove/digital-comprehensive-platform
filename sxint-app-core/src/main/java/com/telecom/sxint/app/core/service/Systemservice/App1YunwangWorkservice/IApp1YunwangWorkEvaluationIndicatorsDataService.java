package com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice;

import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkEvaluationIndicatorsData;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
public interface IApp1YunwangWorkEvaluationIndicatorsDataService extends IService<App1YunwangWorkEvaluationIndicatorsData> {


    /**
     * 查询排序
     *
     * @param
     * @return
     */
    List<App1YunwangWorkEvaluationIndicatorsData> getAllByAsc();

    /**
     * 根据id单条修改
     *
     * @param app1YunwangWorkEvaluationIndicatorsData
     * @return
     */
    App1YunwangWorkEvaluationIndicatorsData updateByItemAndIndicator(App1YunwangWorkEvaluationIndicatorsData app1YunwangWorkEvaluationIndicatorsData);

    /**
     * 根据条件查询
     *
     * @param filter
     * @return
     */
    List<App1YunwangWorkEvaluationIndicatorsData> queryByConditions(App1YunwangWorkEvaluationIndicatorsData filter);

    /**
     * 查询所有考核项目
     *
     * @param
     * @return
     */
    List<String> selectAllevaluationItem();

    /**
     * 根据考核项目查询
     *
     * @param evaluationItem
     * @return
     */
    List<App1YunwangWorkEvaluationIndicatorsData> selectByevaluationItem(String evaluationItem);
}
