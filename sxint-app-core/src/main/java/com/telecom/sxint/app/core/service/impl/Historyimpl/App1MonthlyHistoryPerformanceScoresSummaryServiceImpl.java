package com.telecom.sxint.app.core.service.impl.Historyimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.core.domain.entity.Historypojo.App1MonthlyHistoryPerformanceScoresSummary;
import com.telecom.sxint.app.core.mapper.Historymapper.App1MonthlyHistoryPerformanceScoresSummaryMapper;
import com.telecom.sxint.app.core.service.Historyservice.IApp1MonthlyHistoryPerformanceScoresSummaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-09-24
 */
@Service
public class App1MonthlyHistoryPerformanceScoresSummaryServiceImpl extends ServiceImpl<App1MonthlyHistoryPerformanceScoresSummaryMapper, App1MonthlyHistoryPerformanceScoresSummary> implements IApp1MonthlyHistoryPerformanceScoresSummaryService {
    @Autowired
    private App1MonthlyHistoryPerformanceScoresSummaryMapper app1MonthlyHistoryPerformanceScoresSummaryMapper;
    @Override
    public boolean updateByIdAndDate(App1MonthlyHistoryPerformanceScoresSummary app1MonthlyHistoryPerformanceScoresSummary) {
        UpdateWrapper<App1MonthlyHistoryPerformanceScoresSummary> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("year_Month", app1MonthlyHistoryPerformanceScoresSummary.getYearMonth());
        updateWrapper.eq("sequence_number", app1MonthlyHistoryPerformanceScoresSummary.getSequenceNumber());
        return app1MonthlyHistoryPerformanceScoresSummaryMapper.update(app1MonthlyHistoryPerformanceScoresSummary, updateWrapper) > 0;
    }
    /**
     * 查找月度历史记录（根据月份）
     */
    @Override
    public List<App1MonthlyHistoryPerformanceScoresSummary> getByMonth(String month) {
        QueryWrapper<App1MonthlyHistoryPerformanceScoresSummary> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("year_month", month);
        queryWrapper.eq("is_delete", 0);
        return app1MonthlyHistoryPerformanceScoresSummaryMapper.selectList(queryWrapper);
    }
}
