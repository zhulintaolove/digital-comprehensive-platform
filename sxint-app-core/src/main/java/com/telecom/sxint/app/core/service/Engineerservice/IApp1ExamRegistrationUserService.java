package com.telecom.sxint.app.core.service.Engineerservice;

import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1ExamRegistrationUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-10-22
 */
public interface IApp1ExamRegistrationUserService extends IService<App1ExamRegistrationUser> {

}
