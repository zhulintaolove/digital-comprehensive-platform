package com.telecom.sxint.app.core.service.impl.Systemimpl.App1MonthlyServiceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo.App1Assessment;
import com.telecom.sxint.app.core.mapper.Systemmapper.App1Monthlymapper.App1AssessmentMapper;
import com.telecom.sxint.app.core.service.Systemservice.App1Monthlyservice.IApp1AssessmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-08-27
 */
@Service
public class App1AssessmentServiceImpl extends ServiceImpl<App1AssessmentMapper, App1Assessment> implements IApp1AssessmentService {
    /**
     * 根据部门名称来查找
     * @param department
     * @return
     */
    @Override
    public List<App1Assessment> selectWithDepartment(String department) {
        QueryWrapper<App1Assessment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("assessed_department", department).eq("is_delete", 0);
        queryWrapper.orderByAsc("id");
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<String> selectAllDepartment() {
        // 创建查询包装器
        QueryWrapper<App1Assessment> queryWrapper = new QueryWrapper<>();
        // 选择 distinct assessed_department 和 id
        queryWrapper.select("assessed_department", "id").eq("is_delete", 0);
        // 按照 id 排序
        queryWrapper.orderByAsc("id");
        // 执行查询
        List<App1Assessment> resultList = baseMapper.selectList(queryWrapper);
        // 提取 assessed_department 列到列表中
        List<String> departmentList = resultList.stream()
                .map(App1Assessment::getAssessedDepartment)
                .distinct()
                .collect(Collectors.toList());
        return departmentList;
    }
    @Override
    public void deleteById(App1Assessment app1Assessment) {
        //is_delete设为1
        app1Assessment.setIsDelete(1);
        baseMapper.updateById(app1Assessment);
    }
}
