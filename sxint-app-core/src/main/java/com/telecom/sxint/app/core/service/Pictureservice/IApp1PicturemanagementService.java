package com.telecom.sxint.app.core.service.Pictureservice;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.Picturepojo.App1Picturemanagement;
import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.domain.entity.dto.PictureQueryDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * <p>
 * 事件管理信息表 服务类
 * </p>
 *
 * @author
 * @since 2024-08-23
 */
public interface IApp1PicturemanagementService extends IService<App1Picturemanagement> {
    /**
     * 新增图片数据
     * @param app1Picturemanagement
     * @return
     */
    ResponseData<App1Picturemanagement> insertPicture(App1Picturemanagement app1Picturemanagement);
    /**
     * 搜索图片、关键词搜索
     * @param key
     * @return
     */
    ResponseData<List<App1Picturemanagement>> getPictureByKey(String key);
    /**
     * 根据id修改图片
     * @param app1Picturemanagement
     * @return
     */
    ResponseData<App1Picturemanagement> updatePicture(App1Picturemanagement app1Picturemanagement);

    /**
     * 分页条件查询
     * @param pictureQueryDTO
     * @return
     */
    PageData queryPicturePage(PictureQueryDTO pictureQueryDTO);
    /**
     * 根据事件类型分页查询图片
     * @param event
     * @return
     */
    PageData queryPicturePageByEvent(PictureQueryDTO pictureQueryDTO,String event);
    /**
     * 上传图片或视频
     * @param file
     * @return
     */
    ResponseData uploadFile(MultipartFile file);
    /**
     * 根据事件类型查询图片
     *
     */
    ResponseData<List<App1Picturemanagement>> getPictureByEvent(String event);

    /**
     * 查询全部图片分页
     * @param pictureQueryDTO
     * @return
     */
    PageData<App1Picturemanagement> queryAllPicturesPage(PictureQueryDTO pictureQueryDTO);


    ResponseData uploadImage(MultipartFile imageFile) throws IOException;

    ResponseData<List<App1Picturemanagement>> insertPictures(List<App1Picturemanagement> app1Picturemanagements);
}