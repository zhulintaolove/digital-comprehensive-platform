package com.telecom.sxint.app.core.service.Vatinvoiceservice;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.Vatinvoicepojo.App1Vatinvoinceprintlog;

import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.domain.entity.dto.InvoiceFilterDTO;
import com.telecom.sxint.app.core.domain.entity.dto.TicketQueryDTO;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2024-08-07
 */

public interface IApp1VatinvoinceprintlogService extends IService<App1Vatinvoinceprintlog> {
    /**
     * 删除
     * @param id
     */
    void DeleteById(Integer id);

    /**
     * 修改
     * @param app1Vatinvoinceprintlog
     * @return
     */
    App1Vatinvoinceprintlog updateByIdrdate(App1Vatinvoinceprintlog app1Vatinvoinceprintlog);
    /**
     * 修改打印状态
     * @param ids
     * @return
     */
    ResponseData updatePrintflag(List<Integer> ids);
    /**
     *  查询
     * @param filter
     * @return
     */
    List<App1Vatinvoinceprintlog> findInvoicesByFilter(InvoiceFilterDTO filter);

    /**
     * 根据组织关键字查询
     * @param keyword
     * @param area
     * @return
     */
    List<App1Vatinvoinceprintlog> searchByKeyword2(String keyword, String area);
    /**
     * 关键字查询
     * @param keyword
     * @return
     */
    List<App1Vatinvoinceprintlog> searchByKeyword(String keyword);

    /**
     * 单记录上传
     * @param singleApp1VatinvoinceprintlogDto
     * @return
     */
    ResponseData handleSingleUpload(App1Vatinvoinceprintlog singleApp1VatinvoinceprintlogDto);

    /**
     * 导出文件id查询
     * @param ids
     * @return
     */
    List<App1Vatinvoinceprintlog> findByIds(List<Integer> ids);
    /**
     * 清空专票表
     */
    void deleteAll();
    /**
     * 导出所有记录
     */
    void exportAllExcel(HttpServletResponse response);
    /**
     * 导出部分记录
     * @param ids
     * @param response
     */
    void exportExcel(List<Integer> ids,HttpServletResponse response);
    /**
     * 分页查询
     * @param ticketQueryDTO
     * @return
     */
    PageData queryTicketPage(TicketQueryDTO ticketQueryDTO);
    /**
     * 根据组织分页查询
     * @param ticketQueryDTO
     * @return
     */
    PageData queryAreaTicketPage(TicketQueryDTO ticketQueryDTO,String area);
}
