package com.telecom.sxint.app.core.service.impl;

import com.telecom.sxint.app.core.domain.entity.PubUser;
import com.telecom.sxint.app.core.mapper.PubUserMapper;
import com.telecom.sxint.app.core.service.IPubUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-08-14
 */
@Service
public class PubUserServiceImpl extends ServiceImpl<PubUserMapper, PubUser> implements IPubUserService {

}
