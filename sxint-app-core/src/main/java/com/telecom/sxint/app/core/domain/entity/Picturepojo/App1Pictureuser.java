package com.telecom.sxint.app.core.domain.entity.Picturepojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("app1_pictureuser")
@ApiModel(value="App1Pictureuser对象", description="用户图片下载表")
public class App1Pictureuser implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @ApiModelProperty(value = "用户名")
    private String tokenName;
    @ApiModelProperty(value = "媒体链接，可以是图片或视频的URL")
    private String link;
    @ApiModelProperty(value = "下载次数，默认为0")
    private Integer downloadCount;
    @ApiModelProperty(value = "标签")
    private String tags;
    @ApiModelProperty(value = "标识用户是否为管理员")
    private Boolean isAdmin;


}