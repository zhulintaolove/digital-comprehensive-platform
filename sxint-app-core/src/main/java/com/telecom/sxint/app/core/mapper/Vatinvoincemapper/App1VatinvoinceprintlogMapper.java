package com.telecom.sxint.app.core.mapper.Vatinvoincemapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.telecom.sxint.app.core.domain.entity.Vatinvoicepojo.App1Vatinvoinceprintlog;
import org.apache.ibatis.annotations.Insert;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-08-07
 */
public interface App1VatinvoinceprintlogMapper extends BaseMapper<App1Vatinvoinceprintlog> {
    /**
     * 单条导入
     * @param
     */
    @Insert("insert into app1_vatinvoinceprintlog(area, account, printmonth, invoice,amount,printflag, operator,created,billmonth) values (#{area}, #{account}, #{printmonth}, #{invoice},#{amount},#{printflag}, #{operator},#{created},#{billmonth}) ")
    int Single(App1Vatinvoinceprintlog singleApp1VatinvoinceprintlogDto);
}
