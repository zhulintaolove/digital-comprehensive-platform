package com.telecom.sxint.app.core.domain.entity.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@ExcelIgnoreUnannotated
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@HeadFontStyle(fontHeightInPoints = 18, fontName = "方正小标宋简体")
@HeadRowHeight(30)
@ApiModel(value="AnalyseCountyDTO", description="yunwang制表数据导出对象")
public class AnalyseCountyDTO {

    @ExcelProperty("模块")
    @ApiModelProperty(value = "考核项目")
    private String evaluationItem;

    @ExcelProperty("越城")
    @ApiModelProperty(value = "越城")
    private String yuecheng;

    @ExcelProperty("柯桥")
    @ApiModelProperty(value = "柯桥")
    private String keqiao;

    @ExcelProperty("上虞")
    @ApiModelProperty(value = "上虞")
    private String shangyu;

    @ExcelProperty("嵊州")
    @ApiModelProperty(value = "嵊州")
    private String shengzhou;

    @ExcelProperty("新昌")
    @ApiModelProperty(value = "新昌")
    private String xinchang;

    @ExcelProperty("诸暨")
    @ApiModelProperty(value = "诸暨")
    private String zhuji;

    @ExcelProperty("分值")
    @ApiModelProperty(value = "分值")
    private String score;
}
