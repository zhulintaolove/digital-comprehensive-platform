package com.telecom.sxint.app.core.domain.entity.dto;

import lombok.Data;

/**
 * 短信记录查询的DTO类
 */
@Data
public class MessageQueryDTO {
    private Long currentPage;  // 当前页
    private Long pageSize;     // 每页记录数
    private String phoneNumber;   // 可选：按手机号查询
}