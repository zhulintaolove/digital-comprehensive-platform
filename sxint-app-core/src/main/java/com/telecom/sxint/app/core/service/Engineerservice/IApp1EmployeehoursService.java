package com.telecom.sxint.app.core.service.Engineerservice;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Employeehours;
import com.baomidou.mybatisplus.extension.service.IService;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.domain.entity.dto.CertificationQueryDTO;
import com.telecom.sxint.app.core.domain.entity.dto.HoursQueryDTO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author
 * @since 2024-08-19
 */
public interface IApp1EmployeehoursService extends IService<App1Employeehours> {
    /**
     * 修改工时
     * @param app1Employeehours
     * @return
     */
    App1Employeehours updateByEmployeeId(App1Employeehours app1Employeehours);
    /**
     *  根据id查询该员工每月信息
     */
    List<App1Employeehours> queryAllById(String id);
    /**
     * 查询所有工时数据
     * @return
     */
    List<App1Employeehours> listAll();
    /**
     * 根据姓名或人员编码查询研发人员明细和工时
     * @param filter
     * @return
     */
    List<App1Employeehours> queryByEmployeeNameOrId(App1Employeehours filter);
    /**
     * 工时分页查询
     * @param hoursQueryDTO
     * @return
     */
    PageData queryHoursPage(HoursQueryDTO hoursQueryDTO);

    /**
     * 单条插入
     * @param app1Employeehours
     * @return
     */
    ResponseData handleSingleUpload(App1Employeehours app1Employeehours);
    /**
     * 根据组织分页查询
     * @param hoursQueryDTO
     * @return
     */
    PageData queryByCompanyPage(HoursQueryDTO hoursQueryDTO,String company);
    /**
     * 清空数据
     * @return
     */
    ResponseData clearData();
    /**
     *  根据id删除员工工时信息
     * @param id
     */
    ResponseData DeleteById(Integer id);
    /**
     * 批量删除研究人员信息
     * @param ids
     * @return
     */
    ResponseData deleteBatch(List<Integer> ids);

    /**
     *  删除所有员工工时信息
     */
    void deleteAll();
}