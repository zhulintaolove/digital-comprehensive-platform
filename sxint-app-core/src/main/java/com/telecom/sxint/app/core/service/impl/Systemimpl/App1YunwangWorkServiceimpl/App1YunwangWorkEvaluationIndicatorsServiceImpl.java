package com.telecom.sxint.app.core.service.impl.Systemimpl.App1YunwangWorkServiceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkEvaluationIndicators;
import com.telecom.sxint.app.core.domain.entity.dto.AnalyseDateDTO;
import com.telecom.sxint.app.core.domain.entity.dto.EvaluationIndicatorsDTO;
import com.telecom.sxint.app.core.mapper.Systemmapper.App1YunwangWorkmapper.App1YunwangWorkEvaluationIndicatorsDataMapper;
import com.telecom.sxint.app.core.mapper.Systemmapper.App1YunwangWorkmapper.App1YunwangWorkEvaluationIndicatorsMapper;
import com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice.IApp1YunwangWorkEvaluationIndicatorsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
@Service
public class App1YunwangWorkEvaluationIndicatorsServiceImpl extends ServiceImpl<App1YunwangWorkEvaluationIndicatorsMapper, App1YunwangWorkEvaluationIndicators> implements IApp1YunwangWorkEvaluationIndicatorsService {
    /**
     * 连表查找全部数据
     * @return
     */
    @Override
    public List<EvaluationIndicatorsDTO> selectWithData() {
        return baseMapper.selectWithData();
    }
    /**
     * 根据考核项目查询
     * @param evaluationItem
     * @return
     */
    @Override
    public List<App1YunwangWorkEvaluationIndicators> selectByevaluationItem(String evaluationItem) {
        QueryWrapper<App1YunwangWorkEvaluationIndicators> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("evaluation_item",evaluationItem).eq("is_delete", 0);
        return baseMapper.selectList(queryWrapper);
    }
    @Override
    public List<AnalyseDateDTO> selectAnalyseDateWithData() {
        return baseMapper.selectWithAnalyseDate();
    }
}
