package com.telecom.sxint.app.core.domain.entity;

import cn.zjtele.pubinfo.common.dict.annotation.DictTranslateField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 测试信息表
 * </p>
 *
 * @author sx_tele
 * @since 2024-07-11
 */
@Getter
@Setter
@TableName("test_demo")
@Schema(name = "TestDemo", description = "测试信息表")
public class TestDemo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @Schema(description = "名称")
    @TableField("name")
    private String name;

    @DictTranslateField(dictType = "kfType")
    @Schema(description = "字典类型")
    @TableField("dict_type")
    private String dictType;

    @Schema(description = "创建人id")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private String createdBy;

    @Schema(description = "创建组织id")
    @TableField("created_org")
    private String createdOrg;
}
