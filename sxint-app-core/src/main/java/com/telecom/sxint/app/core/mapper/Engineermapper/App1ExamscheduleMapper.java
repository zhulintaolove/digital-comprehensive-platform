package com.telecom.sxint.app.core.mapper.Engineermapper;

import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Examschedule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2024-08-20
 */
public interface App1ExamscheduleMapper extends BaseMapper<App1Examschedule> {

}
