@echo off
rem ======================================================================
rem windows startup script
rem ======================================================================

set DIR_HOME=%~dp0
rem added double quotation marks to avoid the issue caused by the folder names containing spaces.
rem removed the last 5 chars(which means \bin\) to get the base DIR.
set DIR_HOME=%DIR_HOME:~0,-5%

cd %DIR_HOME%

call %DIR_HOME%/bin/env.bat

title=%RUN_NAME% [%DIR_HOME%]

if not exist "%JAVA_HOME%\bin\jps.exe" echo Please set the JAVA_HOME variable in your environment, We need java(x64)! jdk8 is better! & EXIT /B 1

setlocal

set "PATH=%JAVA_HOME%\bin;%PATH%"

echo killing %RUN_NAME%

for /f "tokens=1" %%i in ('jps -m ^| find "%RUN_NAME%"') do ( taskkill /F /PID %%i )

echo Done!
