@echo off
rem ======================================================================
rem windows startup script
rem ======================================================================

set DIR_HOME=%~dp0
rem added double quotation marks to avoid the issue caused by the folder names containing spaces.
rem removed the last 5 chars(which means \bin\) to get the base DIR.
set DIR_HOME=%DIR_HOME:~0,-5%

cd %DIR_HOME%

call %DIR_HOME%/bin/env.bat

title=%RUN_NAME% [%DIR_HOME%]

if not exist "%JAVA_HOME%\bin\java.exe" echo Please set the JAVA_HOME variable in your environment, We need java(x64)! jdk8 is better! & EXIT /B 1
set JAVA=%JAVA_HOME%\bin\java.exe



echo #####################################################################
echo ######         DIR_HOME:  %DIR_HOME%
echo ######             JAVA:  %JAVA_HOME%
echo ######         JVM_OPTS:  %JVM_OPTS%
echo ######       JAR_D_OPTS:  %JAR_D_OPTS%
echo ######         JAR_OPTS:  %JAR_OPTS%
echo ######      SPRING_OPTS:  %SPRING_OPTS%
echo ######    SW_AGENT_OPTS:  %SW_AGENT_OPTS%
echo ######         RUN_NAME:  %RUN_NAME%
echo #####################################################################

if not exist "%JAVA_HOME%\bin\java.exe" set JAVA=%JAVA_HOME%\bin\java.exe
if not exist %DIR_LOGS% md %DIR_LOGS%
if not exist %DIR_GC% md %DIR_GC%
if not exist %DIR_SW_LOG% md %DIR_SW_LOG%

call %JAVA% %SW_AGENT_OPTS% %JVM_OPTS% %JAR_D_OPTS% %JAR_OPTS% %SPRING_OPTS% %RUN_NAME% %*