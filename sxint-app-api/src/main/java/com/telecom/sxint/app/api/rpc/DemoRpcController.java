package com.telecom.sxint.app.api.rpc;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.TestDemo;
import com.telecom.sxint.app.core.service.TestDemoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Title:
 * @version:
 * @Description:
 * @Copyright 2024 浙江信产.基础能力中心
 * @Author: liyawen
 * @Date: 2024/7/11
 */
@RestController
@RequestMapping("/rpc/demo")
@Tag(name = "测试RPC接口", description = "测试RPC接口")
public class DemoRpcController {

    @Resource
    private TestDemoService testDemoService;

    @GetMapping("/list")
    @Operation(summary = "获取所有TestDemo", description = "返回所有测试信息表列表")
    public ResponseData<List<TestDemo>> list() {
        return ResponseData.success(testDemoService.list());
    }


}
