package com.telecom.sxint.app.api.controller.SystemController.App1CountyIndicatorsController;


import cn.zjtele.pubinfo.boot.redis.config.RedisHelper;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Examschedule;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1CountyIndicators;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1CountyIndicators;
import com.telecom.sxint.app.core.service.Systemservice.App1countyIndicatorsservice.IApp1CountyIndicatorsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * <p>
 * 县分指标 前端控制器
 * </p>
 *
 * @author 
 * @since 2024-09-02
 */
@RestController
@Tag(name = "县分季度指标控制器", description = "县分季度指标操作管理")
@RequestMapping("/app1-system")
public class App1CountyIndicatorsController {
    @Autowired
    private IApp1CountyIndicatorsService countyIndicatorsService;
    @Autowired
    private RedisHelper redisHelper;
    String cacheKey = "CountyIndicators:countylist";

    /**
     * 新增县分指标记录
     * @param indicator
     * @return
     */
    @Operation(summary = "新增县分指标记录")
    @PostMapping("/countycreate")
    public ResponseData create(@RequestBody App1CountyIndicators indicator) {
        boolean result = countyIndicatorsService.save(indicator);
        redisHelper.delete(cacheKey);
        return result ? ResponseData.success("新增成功") : ResponseData.failed("新增失败","");
    }

    /**
     * 更新县分指标记录
     * @param indicator
     * @return
     */
    @Operation(summary = "更新县分指标记录")
    @PutMapping("/countyupdate")
    public ResponseData update(@RequestBody App1CountyIndicators indicator) {
        boolean result = countyIndicatorsService.updateById(indicator);
        redisHelper.delete(cacheKey);
        return result ? ResponseData.success("更新成功") : ResponseData.failed("更新失败","");
    }

    /**
     * 根据id删除县分指标记录
     * @param id
     * @return
     */
    @Operation(summary = "根据id删除县分指标记录")
    @DeleteMapping("/county/{id}")
    public ResponseData delete(@PathVariable Integer id) {
//        boolean result = countyIndicatorsService.removeById(id);
//        return result ? ResponseData.success("删除成功") : ResponseData.failed("删除失败","");
        UpdateWrapper<App1CountyIndicators> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1
        countyIndicatorsService.update(null, updateWrapper);
        redisHelper.delete(cacheKey);
        return ResponseData.success("删除成功");
    }

    /**
     * 根据id获取县分指标记录
     * @param id
     * @return
     */
    @Operation(summary = "根据id获取县分指标记录")
    @PostMapping("/county/{id}")
    public ResponseData getById(@PathVariable Integer id) {
        App1CountyIndicators indicator = countyIndicatorsService.getById(id);
        // 检查is_delete字段是否为0
        if (indicator != null && indicator.getIsDelete() == 0) {
            // 如果is_delete为0，返回成功响应
            return ResponseData.success(indicator);
        } else {
            // 如果is_delete为1或对象为空，返回空结果或错误信息
            return ResponseData.failed("未找到有效的县分数据","40000" );
        }
    }

    /**
     * 获取所有县分指标记录
     * @return
     */
    @Operation(summary = "获取所有县分指标记录")
    @PostMapping("/countylist")
    public ResponseData list() {
        List<App1CountyIndicators> cacheData = (List<App1CountyIndicators>) redisHelper.valueGet(cacheKey);
        if (cacheData != null && !cacheData.isEmpty()) {
            return ResponseData.success(cacheData);
        }
        QueryWrapper<App1CountyIndicators> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0); // 只查询 is_delete 字段为 0 的记录
        List<App1CountyIndicators> indicator = countyIndicatorsService.list(queryWrapper);
        redisHelper.valueSet(cacheKey,indicator);
        return ResponseData.success(indicator);
    }
}
