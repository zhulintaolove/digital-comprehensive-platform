package com.telecom.sxint.app.api.controller.EngineerController;


import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.api.controller.Vatinvoincecontroller.App1VatinvoinceprintlogController;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Examschedule;
import com.telecom.sxint.app.core.service.Engineerservice.IApp1ExamscheduleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import com.alibaba.cloud.commons.lang.StringUtils;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2024-08-20
 */
@RestController
@RequestMapping("/app1-engineer")
@Tag(name = "考情发布", description = "考情发布")
public class App1ExamscheduleController {
    @Autowired
    private IApp1ExamscheduleService iapp1ExamscheduleService;
    @Autowired
    private App1VatinvoinceprintlogController app1VatinvoinceprintlogController;

    /**
     * 查询所有考情信息
     * @return
     */
    @Operation(summary = "查询所有考情信息")
    @GetMapping("/examlist")
    public ResponseData listAll() {
        QueryWrapper<App1Examschedule> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0)
                .orderByAsc("exam_date");
        List<App1Examschedule> app1ExamschedulesList = iapp1ExamscheduleService.list(queryWrapper);
        return ResponseData.success(app1ExamschedulesList);
    }

    /**
     * 根据条件查询考情信息
     * @param filter
     * @return
     */
    @Operation(summary = "根据条件查询考情信息")
    @PostMapping("/examsearch")
    public ResponseData<List<App1Examschedule>> searchByConditions(@RequestBody App1Examschedule filter) {
        QueryWrapper<App1Examschedule> queryWrapper = new QueryWrapper<>();
        queryWrapper
                .eq("is_delete", 0) // 添加条件，确保只查询 is_delete 字段为 0 的记录
                .like(StringUtils.isNotBlank(filter.getExamLevel()), "exam_level", filter.getExamLevel())
                .or()
                .like(StringUtils.isNotBlank(filter.getExamDate()), "exam_date", filter.getExamDate())
                .or()
                .like(StringUtils.isNotBlank(filter.getRegistrationStartDate()), "registration_start_date", filter.getRegistrationStartDate())
                .or()
                .like(StringUtils.isNotBlank(filter.getRegistrationDeadline()), "registration_deadline", filter.getRegistrationDeadline())
                .or()
                .like(StringUtils.isNotBlank(filter.getDescription()), "description", filter.getDescription());
        
        queryWrapper.orderByAsc("exam_date");
        List<App1Examschedule> results = iapp1ExamscheduleService.list(queryWrapper);
        return ResponseData.success(results);
    }

    /**
     * 修改考情信息
     * @param app1Examschedule
     * @return
     */
    @Operation(summary = "修改考情信息", description = "根据id修改考情信息")
    @PostMapping("/examupdate")
    public ResponseData<App1Examschedule> update(@RequestBody App1Examschedule app1Examschedule) {
        App1Examschedule log1=iapp1ExamscheduleService.updateByEmployeeId(app1Examschedule);
        return ResponseData.success(log1);


    }
    /**
     * 清空考情记录
     * @param
     * @return
     */
    @Operation(summary = "清空考情信息", description = "返回成功信息")
    @PostMapping("/examclearData")
    public ResponseData clearData() {
        //return iapp1ExamscheduleService.clearData();
        UpdateWrapper<App1Examschedule> updateWrapper = new UpdateWrapper<>();

        // 设置 is_delete 字段的值为 1
        updateWrapper.set("is_delete", 1);

        // 调用服务类的 update 方法进行更新
        // 这里传入 null 作为实体对象，因为我们不需要更新除了 is_delete 之外的其他字段
        iapp1ExamscheduleService.update(null, updateWrapper);
        return ResponseData.success("清空成功");
    }
    /**
     * 删除单条考情信息
     * @param id
     * @return
     */
    @Operation(summary = "删除单条考情信息", description = "根据id删除单条考情信息")
    @DeleteMapping("/examdelete")
    public ResponseData deletById(@RequestParam Integer id) {
        UpdateWrapper<App1Examschedule> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1
        iapp1ExamscheduleService.update(null, updateWrapper);
        return ResponseData.success("删除成功");
    }
    /**
     * 批量删除考情信息
     * @param ids
     * @return
     */
    @Operation(summary = "批量删除考情信息", description = "根据ids删除考情明细")
    @DeleteMapping("/examdeleteids")
    public ResponseData deletByIds(@RequestBody List<Integer> ids) {
        if(ids==null||ids.isEmpty())return ResponseData.success("未选择删除记录");
        UpdateWrapper<App1Examschedule> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in("id", ids) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1

        // 调用服务类的 update 方法进行批量更新
        iapp1ExamscheduleService.update(null, updateWrapper);
        return ResponseData.success("删除成功");
    }

}
