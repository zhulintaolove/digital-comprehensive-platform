package com.telecom.sxint.app.api.controller.EngineerController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.ByteArrayOutputStream;

/**
 * @author zhulintao
 * @version 2.0
 * @date 2024/8/26 15:23
 */
@RestController
@RequestMapping("/app1-engineer")
@Tag(name = "导出控制器", description = "信息导出管理")
@RequiredArgsConstructor
public class FileexportController {
    /**
     * 导出工时模板
     * @return
     */
    @Operation(summary = "导出工时excel模板", description = "导出位置在D:\\App1Vatinvoinceprintlog.xlsx")
    @GetMapping("/exportWorkHoursTemplate")
    public ResponseEntity<ByteArrayResource> exportWorkHoursTemplate() {
        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            // 创建一个空白工作表
            XSSFSheet sheet = workbook.createSheet("Blank Template");

            // 创建标题行
            XSSFRow titleRow = sheet.createRow(0);

            titleRow.createCell(0).setCellValue("工时月份");
            titleRow.createCell(1).setCellValue("成员姓名");
            titleRow.createCell(2).setCellValue("成员ID");
            titleRow.createCell(3).setCellValue("复核工时（天）");
            // 创建一个示例数据行
            XSSFRow dataRow = sheet.createRow(1); // 第一行数据从索引1开始
            dataRow.createCell(0).setCellValue("2022-01");
            dataRow.createCell(1).setCellValue("张三");
            dataRow.createCell(2).setCellValue("9527");
            dataRow.createCell(3).setCellValue(21);
            // 创建其他行和单元格，留空供用户填写
            // 将工作簿转换为字节流
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            byte[] bytes = bos.toByteArray();
            // 创建Resource对象
            ByteArrayResource resource = new ByteArrayResource(bytes);
            // 设置HTTP响应头
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=WorkHoursTemplate.xlsx");

            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } catch (Exception e) {
            e.printStackTrace();
            // 处理异常
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 导出工程师信息excel模板
     * @return
     */
    @Operation(summary = "导出工程师信息excel模板", description = "导出工程师信息excel模板")
    @GetMapping("/exportEngineerInfoTemplate")
    public ResponseEntity<ByteArrayResource> exportEngineerInfoTemplate() {
        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            // 创建一个空白工作表
            XSSFSheet sheet = workbook.createSheet("Blank Template");
            // 创建标题行
            XSSFRow titleRow = sheet.createRow(0);
            titleRow.createCell(0).setCellValue("单位");
            titleRow.createCell(1).setCellValue("人员编码");
            titleRow.createCell(2).setCellValue("姓名");
            titleRow.createCell(3).setCellValue("是否研发人员（人才云）");
            titleRow.createCell(4).setCellValue("是否研发人员（基准岗位）");
            titleRow.createCell(5).setCellValue("是否通过研发通用认证");
            titleRow.createCell(6).setCellValue("研发通用认证级别");
            titleRow.createCell(7).setCellValue("是否通过研发专业认证");
            titleRow.createCell(8).setCellValue("研发专业认证级别");
            titleRow.createCell(9).setCellValue("研发经验级别");
            // 创建一个示例数据行
            XSSFRow dataRow = sheet.createRow(1); // 第一行数据从索引1开始
            dataRow.createCell(0).setCellValue("中国电信股份有限公司厦门分公司");
            dataRow.createCell(1).setCellValue("9527");
            dataRow.createCell(2).setCellValue("张三");
            dataRow.createCell(3).setCellValue("0");
            dataRow.createCell(4).setCellValue("1");
            dataRow.createCell(5).setCellValue("1");
            dataRow.createCell(6).setCellValue("研发通用L2");
            dataRow.createCell(7).setCellValue("1");
            dataRow.createCell(8).setCellValue("研发专业L2");
            dataRow.createCell(9).setCellValue("研发经验L3");
            // 创建其他行和单元格，留空供用户填写
            // 将工作簿转换为字节流
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            byte[] bytes = bos.toByteArray();
            // 创建Resource对象
            ByteArrayResource resource = new ByteArrayResource(bytes);
            // 设置HTTP响应头
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=EngineerInfoTemplate.xlsx");
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } catch (Exception e) {
            e.printStackTrace();
            // 处理异常
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
    /**
     * 导出考情信息excel模板
     * @return
     */
    @Operation(summary = "导出考情信息excel模板", description = "导出位置在D:\\App1Vatinvoinceprintlog.xlsx")
    @GetMapping("/exportExamInfoTemplate")
    public ResponseEntity<ByteArrayResource> exportExamInfoTemplate() {
        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            // 创建一个空白工作表
            XSSFSheet sheet = workbook.createSheet("Blank Template");
            // 创建标题行
            XSSFRow titleRow = sheet.createRow(0);
            titleRow.createCell(0).setCellValue("考试等级");
            titleRow.createCell(1).setCellValue("考试日期");
            titleRow.createCell(2).setCellValue("报名开始日期");
            titleRow.createCell(3).setCellValue("报名截至日期");
            titleRow.createCell(4).setCellValue("考情描述");
            // 创建一个示例数据行
            XSSFRow dataRow = sheet.createRow(1); // 第一行数据从索引1开始
            dataRow.createCell(0).setCellValue("研发工程师通用能力L1");
            dataRow.createCell(1).setCellValue("10月21日");
            dataRow.createCell(2).setCellValue("08月01日");
            dataRow.createCell(3).setCellValue("12月4日");
            dataRow.createCell(4).setCellValue("12月份");
            // 创建其他行和单元格，留空供用户填写
            // 将工作簿转换为字节流
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            byte[] bytes = bos.toByteArray();
            // 创建Resource对象
            ByteArrayResource resource = new ByteArrayResource(bytes);
            // 设置HTTP响应头
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=ExamInfoTemplate.xlsx");
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } catch (Exception e) {
            e.printStackTrace();
            // 处理异常
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

}
