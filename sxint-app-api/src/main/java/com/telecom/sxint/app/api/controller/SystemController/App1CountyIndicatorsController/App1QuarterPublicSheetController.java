package com.telecom.sxint.app.api.controller.SystemController.App1CountyIndicatorsController;


import cn.zjtele.pubinfo.boot.redis.config.RedisHelper;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1CountyIndicators;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1QuarterPublicSheet;
import com.telecom.sxint.app.core.service.Systemservice.App1countyIndicatorsservice.IApp1QuarterPublicSheetService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p >
 *
 * @author
 * @since 2024-09-03
 */
@RestController
@Tag(name = "县分季度公共指标控制器", description = "县分季度公共指标操作管理")
@RequestMapping("/app1-system")
public class App1QuarterPublicSheetController {
    @Autowired
    private IApp1QuarterPublicSheetService quarterPublicSheetService;
    @Autowired
    private RedisHelper redisHelper;

    String cacheKey = "QuarterPublicSheet:quarterPublicSheetlist";

    @Operation(summary = "新增县分季度指标")
    @PostMapping("/quarterPublicSheetcreate")
    public ResponseData create(@RequestBody App1QuarterPublicSheet publicSheet) {
        boolean result = quarterPublicSheetService.save(publicSheet);
        redisHelper.delete(cacheKey);
        return result ? ResponseData.success("新增成功") : ResponseData.failed("新增失败","");
    }

    @Operation(summary = "根据ID获取县分季度指标")
    @GetMapping("/quarterPublicSheetget/{id}")
    public ResponseData getById(@PathVariable Integer id) {
//        App1QuarterPublicSheet result = quarterPublicSheetService.getById(id);
//        return ResponseData.success(result);
        App1QuarterPublicSheet app1QuarterPublicSheet = quarterPublicSheetService.getById(id);

        // 检查is_delete字段是否为0
        if (app1QuarterPublicSheet != null && app1QuarterPublicSheet.getIsDelete() == 0) {
            // 如果is_delete为0，返回成功响应
            return ResponseData.success(app1QuarterPublicSheet);
        } else {
            // 如果is_delete为1或对象为空，返回空结果或错误信息
            return ResponseData.failed("未找到有效的县分数据","40000" );
        }
    }


    /**
     * 根据sheetname获取县分季度指标
     * @param sheetname
     * @return
     */
    @Operation(summary = "根据sheetname获取县分季度指标")
    @GetMapping("/quarterPublicSheetlistbysheetname")
    public ResponseData list(@RequestParam String sheetname) {
        QueryWrapper<App1QuarterPublicSheet> queryWrapper = new QueryWrapper<>();
        if(sheetname != null && !sheetname.isEmpty()) {
            queryWrapper.eq("sheet_name", sheetname)
                    .eq("is_delete",0)
                    .orderByAsc("id");
            List<App1QuarterPublicSheet> list = quarterPublicSheetService.list(queryWrapper);
            return ResponseData.success(list);
        }
        return ResponseData.failed("未填写sheet_name", "15151");
    }

    /**
     * 获取所有县分季度指标
     * @return
     */
    @Operation(summary = "获取所有县分季度指标")
    @GetMapping("/quarterPublicSheetlist")
    public ResponseData list() {

        List<App1QuarterPublicSheet> cacheData = (List<App1QuarterPublicSheet>) redisHelper.valueGet(cacheKey);
        if (cacheData != null && !cacheData.isEmpty()) {
            return ResponseData.success(cacheData);
        }

        // 构建查询条件
        QueryWrapper<App1QuarterPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0)
                .orderByAsc("id"); // 确保只查询is_delete字段为0的记录

        // 根据查询条件获取列表
        List<App1QuarterPublicSheet> list = quarterPublicSheetService.list(queryWrapper);
        Collections.sort(list, Comparator.comparing(App1QuarterPublicSheet::getId));
        redisHelper.valueSet(cacheKey,list);
        return ResponseData.success(list);
    }

    /**
     * 更新县分季度指标
     * @param publicSheet
     * @return
     */
    @Operation(summary = "更新县分季度指标")
    @PutMapping("/quarterPublicSheetupdate")
    public ResponseData update(@RequestBody App1QuarterPublicSheet publicSheet) {
        boolean result = quarterPublicSheetService.updateById(publicSheet);
        redisHelper.delete(cacheKey);
        return result ? ResponseData.success("更新成功") : ResponseData.failed("更新失败","");
    }

    /**
     * 删除县分季度指标
     * @param id
     * @return
     */
    @Operation(summary = "删除县分季度指标")
    @DeleteMapping("/quarterPublicSheetdelete/{id}")
    public ResponseData deleteById(@PathVariable Integer id) {
        UpdateWrapper<App1QuarterPublicSheet> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1
        quarterPublicSheetService.update(null, updateWrapper);
        redisHelper.delete(cacheKey);
        return ResponseData.success("删除成功");
    }
}