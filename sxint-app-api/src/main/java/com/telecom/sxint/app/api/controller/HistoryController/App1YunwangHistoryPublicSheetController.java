package com.telecom.sxint.app.api.controller.HistoryController;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Historypojo.App1QuarterHistoryPublicSheet;
import com.telecom.sxint.app.core.domain.entity.Historypojo.App1YunwangHistoryPublicSheet;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangPublicSheet;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1QuarterPublicSheet;
import com.telecom.sxint.app.core.service.Historyservice.IApp1YunwangHistoryPublicSheetService;
import com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice.IApp1YunwangPublicSheetService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p >
 *
 * @author
 * @since 2024-09-24
 */
@RestController
@Tag(name = "网运历史记录管理控制器", description = "网运历史记录管理")
@RequestMapping("/app1-systemhistory")
public class App1YunwangHistoryPublicSheetController {

    @Autowired
    private IApp1YunwangHistoryPublicSheetService historyService;
    @Autowired
    private IApp1YunwangPublicSheetService yunwangPublicSheetService;

    // 创建记录
    @PostMapping("/create")
    public ResponseData createHistoryRecord(@RequestParam String yearMonth) {
        List<App1YunwangPublicSheet>   app1YunwangPublicSheets = yunwangPublicSheetService.list();
        for (App1YunwangPublicSheet a : app1YunwangPublicSheets) {
            App1YunwangHistoryPublicSheet yunwangHistoryPublicSheet = new App1YunwangHistoryPublicSheet();
            yunwangHistoryPublicSheet.setId(a.getId());
            yunwangHistoryPublicSheet.setIsField(a.getIsField());
            yunwangHistoryPublicSheet.setSheetName(a.getSheetName());
            yunwangHistoryPublicSheet.setGeneralFieldA(a.getGeneralFieldA());
            yunwangHistoryPublicSheet.setGeneralFieldB(a.getGeneralFieldB());
            yunwangHistoryPublicSheet.setGeneralFieldC(a.getGeneralFieldC());
            yunwangHistoryPublicSheet.setGeneralFieldD(a.getGeneralFieldD());
            yunwangHistoryPublicSheet.setGeneralFieldE(a.getGeneralFieldE());
            yunwangHistoryPublicSheet.setGeneralFieldF(a.getGeneralFieldF());
            yunwangHistoryPublicSheet.setGeneralFieldG(a.getGeneralFieldG());
            yunwangHistoryPublicSheet.setGeneralFieldH(a.getGeneralFieldH());
            yunwangHistoryPublicSheet.setGeneralFieldI(a.getGeneralFieldI());
            yunwangHistoryPublicSheet.setGeneralFieldJ(a.getGeneralFieldJ());
            yunwangHistoryPublicSheet.setGeneralFieldK(a.getGeneralFieldK());
            yunwangHistoryPublicSheet.setGeneralFieldL(a.getGeneralFieldL());
            yunwangHistoryPublicSheet.setGeneralFieldM(a.getGeneralFieldM());
            yunwangHistoryPublicSheet.setGeneralFieldN(a.getGeneralFieldN());
            yunwangHistoryPublicSheet.setGeneralFieldO(a.getGeneralFieldO());
            yunwangHistoryPublicSheet.setYearMonth(yearMonth);
            historyService.save(yunwangHistoryPublicSheet);

        }
        return ResponseData.success("记录创建成功");
    }

    // 根据ID读取记录
    @GetMapping("/get/{id}")
    public ResponseData getHistoryRecord(@PathVariable Integer id) {
        return ResponseData.success(historyService.getById(id)) ;
    }

    // 更新记录
    @PutMapping("/update")
    public ResponseData updateHistoryRecord(@RequestBody App1YunwangHistoryPublicSheet record) {
        boolean isUpdated = historyService.updateById(record);
        return ResponseData.success(isUpdated ? "记录更新成功" : "记录更新失败") ;
    }

    // 根据ID删除记录
    @DeleteMapping("/delete/{id}")
    public ResponseData deleteHistoryRecord(@PathVariable Integer id) {
        boolean isDeleted = historyService.removeById(id);
        return ResponseData.success(isDeleted ? "记录删除成功" : "记录删除失败") ;
    }

    // 查询所有记录
    @GetMapping("/list")
    public ResponseData listAllHistoryRecords() {
        return ResponseData.success( historyService.list());
    }
    @Operation(summary = "根据年月获取")
    @GetMapping("/getByYearMonth")
    public ResponseData getByYearMonth(@RequestParam String yearMonth) {
        QueryWrapper<App1YunwangHistoryPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("year_month", yearMonth);
        queryWrapper.orderByAsc("id");
        List<App1YunwangHistoryPublicSheet> list = historyService.list(queryWrapper);
        return ResponseData.success(list);
    }
}