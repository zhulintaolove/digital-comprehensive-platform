package com.telecom.sxint.app.api.controller.SystemController.App1YunwangWorkController;


import cn.zjtele.pubinfo.boot.redis.config.RedisHelper;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangPublicSheet;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkEvaluationIndicators;
import com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice.IApp1YunwangWorkEvaluationIndicatorsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
@RestController
@Tag(name = "云网工作评估指标", description = "云网工作评估")
@RequestMapping("/app1-system")
public class App1YunwangWorkEvaluationIndicatorsController {
    @Autowired
    private IApp1YunwangWorkEvaluationIndicatorsService indicatorsService;
    @Autowired
    private RedisHelper redisHelper;
    String cacheKey = "YunwangWorkEvaluationIndicators:list";
    /**
     * 获取所有云网工作评估指标
     * @return
     */
    @Operation(summary = "获取所有云网工作评估指标")
    @GetMapping("/getindicators")
    public ResponseData<List<App1YunwangWorkEvaluationIndicators>> listAll() {
        List<App1YunwangWorkEvaluationIndicators> cacheData = (List<App1YunwangWorkEvaluationIndicators>) redisHelper.valueGet(cacheKey);
        if (cacheData != null && !cacheData.isEmpty()) {
            return ResponseData.success(cacheData);
        }
        QueryWrapper<App1YunwangWorkEvaluationIndicators> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0)// 只查询 is_delete 字段为 0 的记录
                .orderByAsc("id");  // 按照id升序排序

        List<App1YunwangWorkEvaluationIndicators> list = indicatorsService.list(queryWrapper);
        redisHelper.valueSet(cacheKey,list);
        return ResponseData.success(list);
    }

    /**
     * 根据条件查询云网工作评估指标
     * @param filter
     * @return
     */
    @Operation(summary = "根据条件查询云网工作评估指标")
    @PostMapping("/indicators/search")
    public ResponseData<List<App1YunwangWorkEvaluationIndicators>> search(@RequestBody App1YunwangWorkEvaluationIndicators filter) {
        QueryWrapper<App1YunwangWorkEvaluationIndicators> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0)// 只查询 is_delete 字段为 0 的记录
                .like(filter.getEvaluationItem() != null, "evaluation_item", filter.getEvaluationItem())
                .like(filter.getMainIndicator() != null, "main_indicator", filter.getMainIndicator())
                .like(filter.getEvaluationPeriod() != null, "evaluation_period", filter.getEvaluationPeriod())
                .orderByAsc("id");
        List<App1YunwangWorkEvaluationIndicators> list = indicatorsService.list(queryWrapper);
        return ResponseData.success(list);
    }

    /**
     * 新增云网工作评估指标
     * @param indicators
     * @return
     */
    @Operation(summary = "新增云网工作评估指标")
    @PostMapping("/addindicators")
    public ResponseData<App1YunwangWorkEvaluationIndicators> create(@RequestBody App1YunwangWorkEvaluationIndicators indicators) {
        indicatorsService.save(indicators);
        redisHelper.delete(cacheKey);
        return ResponseData.success(indicators);
    }

    /**
     * 更新云网工作评估指标
     * @param indicators
     * @return
     */
    @Operation(summary = "更新云网工作评估指标")
    @PutMapping("/updateindicators")
    public ResponseData<App1YunwangWorkEvaluationIndicators> update(@RequestBody App1YunwangWorkEvaluationIndicators indicators) {
        indicatorsService.updateById(indicators);
        redisHelper.delete(cacheKey);
        return ResponseData.success(indicators);
    }

//    @Operation(summary = "删除云网工作评估指标")
//    @DeleteMapping("/indicators/{id}")
//    public ResponseData<Void> delete(@PathVariable Integer id) {
//        indicatorsService.removeById(id);
//        return ResponseData.success();
//    }

    /**
     * 批量删除云网工作评估指标
     * @param ids
     * @return
     */
    @Operation(summary = "批量删除云网工作评估指标")
    @DeleteMapping("/deleteindicators")
    public ResponseData deleteBatch(@RequestBody List<Integer> ids) {
//        indicatorsService.removeByIds(ids);
//        return ResponseData.success();
        if(ids==null||ids.isEmpty()){
            return ResponseData.success("未选择删除记录");
        }

        UpdateWrapper<App1YunwangWorkEvaluationIndicators> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in("id", ids) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1
        // 调用服务类的 update 方法进行批量更新
        indicatorsService.update(null, updateWrapper);
        redisHelper.delete(cacheKey);
        return ResponseData.success("删除成功");
    }

}


