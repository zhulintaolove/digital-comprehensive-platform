package com.telecom.sxint.app.api.controller.EngineerController;


import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Employeehours;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Engineercertification;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.service.Engineerservice.IApp1EmployeehoursService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.telecom.sxint.app.core.domain.entity.dto.HoursQueryDTO;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author
 * @since 2024-08-19
 */
@RestController
@RequestMapping("/app1-engineer")
@Tag(name = "研发人员数据操作控制器", description = "研发人员数据操作管理")
public class App1EmployeehoursController {
    @Autowired
    private IApp1EmployeehoursService iApp1EmployeehoursService;

    /**
     * 查询所有工时数据
     * @return
     */
    @Operation(summary = "查询所有工时数据",description = "查询所有工时数据")
    @GetMapping("/listAll")
    public ResponseData listAll() {
        List<App1Employeehours> app1EmployeehoursList = iApp1EmployeehoursService.listAll();
        return ResponseData.success(app1EmployeehoursList);
    }
    /**
     * 修改工时
     * @param app1Employeehours
     * @return
     */
    @Operation(summary = "修改工时", description = "根据成员id更新工时")
    @PostMapping("/Engineerupdate")
    public ResponseData<App1Employeehours> hoursupdate(@RequestBody App1Employeehours app1Employeehours) {
        App1Employeehours log1=iApp1EmployeehoursService.updateByEmployeeId(app1Employeehours);
        return ResponseData.success(log1);
    }

    /**
     * 根据姓名或人员编码查询研发人员明细和工时
     * @param filter
     * @return
     */
    @Operation(summary = "根据姓名或人员编码查询研发人员明细和工时")
    @PostMapping ("/Engineersearch")
    public ResponseData searchByEmployeeNameOrId(
            @RequestBody App1Employeehours filter) {
        List <App1Employeehours> list= iApp1EmployeehoursService.queryByEmployeeNameOrId(filter);
        return ResponseData.success(list) ;
    }
    /**
     * 单条工时记录插入
     * @param App1Employeehours
     * @return
     */
    @Operation(summary = "单条工时记录录入数据库", description = "返回成功信息")
    @PostMapping("/EngineerSingleUpload")
    public ResponseData handleSingleUpload(@RequestBody App1Employeehours App1Employeehours){
        return iApp1EmployeehoursService.handleSingleUpload(App1Employeehours);
    }
//    /**
//     * 根据成员id查询全部工时
//     * @param employeeId
//     * @return
//     */
//    @Operation(summary = "根据id查询全部工时", description = "根据id查询全部工时")
//    @GetMapping("/getSumHour")
//    public ResponseData getSumHour(@RequestParam String employeeId) {
//        List<App1Employeehours> list = iApp1EmployeehoursService.queryAllById(employeeId);
//        Double sum = 0.0;
//        for (App1Employeehours app1Employeehours : list) {
//            sum += Double.parseDouble(String.valueOf(app1Employeehours.getReviewedHours()));
//        }
//        return ResponseData.success(sum);
//    }
    /**
     * 清空历史记录
     * @param
     * @return
     */
    @Operation(summary = "清空历史记录", description = "返回成功信息")
    @PostMapping("/EngineerclearData")
    public ResponseData clearData(){
        return iApp1EmployeehoursService.clearData();
    }
    /**
     * 工时分页查询
     * @param hoursQueryDTO
     * @return
     */
    @PostMapping("/Engineerpage")
    @Operation(summary = "分页查询", description = "工时分页查询")
    public ResponseData<PageData<App1Employeehours>> queryHoursPage(@RequestBody HoursQueryDTO hoursQueryDTO){
        PageData pageData=iApp1EmployeehoursService.queryHoursPage(hoursQueryDTO);
        return ResponseData.success(pageData);
    }
    /**
     * 删除单条工时明细
     * @param id
     * @return
     */
    @Operation(summary = "删除单条工时明细(is_delete=1)", description = "根据id删除单条工时明细(is_delete=1)")
    @DeleteMapping("/Engineerdelete")
    public ResponseData deletById(@RequestParam Integer id) {
        return ResponseData.success(iApp1EmployeehoursService.DeleteById(id));
    }

    /**
     * 清空工时表
     */
    @Operation(summary = "清空工时明细", description = "清空工时明细")
    @DeleteMapping("/Engineerdeleteall")
    public ResponseData deleteAll() {
        return ResponseData.success(iApp1EmployeehoursService.clearData());
    }

    /**
     * 批量删除研究人员信息
     * @param ids
     * @return
     */
    @Operation(summary = "批量删除工时明细", description = "根据ids删除工时明细")
    @DeleteMapping("/Engineerdeleteids")
    public ResponseData deletByIds(@RequestBody List<Integer> ids) {
        if(ids==null||ids.isEmpty())return ResponseData.success("未选择删除记录");
        return ResponseData.success(iApp1EmployeehoursService.deleteBatch(ids));
    }
    /**
     * 根据组织分页查询工程师
     *
     * @param hoursQueryDTO
     * @return
     */
    @PostMapping("/byhoursCompanyPage")
    @Operation(summary = "根据组织分页查询工时明细", description = "根据组织分页查询工时明细")

    public ResponseData<PageData<App1Employeehours>> queryByhoursCompanyPage(@RequestBody HoursQueryDTO hoursQueryDTO, String company){
        PageData pageData=iApp1EmployeehoursService.queryByCompanyPage(hoursQueryDTO, company);
        return ResponseData.success(pageData);
    }
}