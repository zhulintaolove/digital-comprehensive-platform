package com.telecom.sxint.app.api.controller.EngineerController;


import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1ExamRegistrationUser;
import com.telecom.sxint.app.core.domain.entity.Vatinvoicepojo.App1Vatinvoinceprintlog;
import com.telecom.sxint.app.core.service.Engineerservice.IApp1ExamRegistrationUserService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p >
 *
 * @author
 * @since 2024-10-22
 */
@RestController
@RequestMapping("/app1-engineer")
public class App1ExamRegistrationUserController {

    @Autowired
    private IApp1ExamRegistrationUserService examRegistrationUserService; // 注入服务类

    // 查询所有报名用户
    @Operation(summary = "考试报名用户获取所有", description = "查询并返回所有报名用户的列表")
    @GetMapping("/examuserlist")
    public ResponseData listAll() {
        List<App1ExamRegistrationUser> users = examRegistrationUserService.list();
        return ResponseData.success(users);
    }

    // 根据 ID 查询报名用户
    @Operation(summary = "考试报名用户根据ID获取", description = "根据用户ID查询并返回报名用户信息")
    @GetMapping("/examuser/{id}")
    public ResponseData getById(@PathVariable Integer id) {
        App1ExamRegistrationUser user = examRegistrationUserService.getById(id);
        if (user != null) {
            return ResponseData.success(user);
        } else {
            return ResponseData.failed("用户未找到", "");
        }
    }

    // 根据姓名模糊查询报名用户
    @Operation(summary = "考试报名用户根据姓名模糊查询", description = "通过姓名进行模糊查询并返回符合条件的用户列表")
    @GetMapping("/examusersearch")
    public ResponseData searchByName(@RequestParam String name) {
        QueryWrapper<App1ExamRegistrationUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("name", name);
        List<App1ExamRegistrationUser> users = examRegistrationUserService.list(queryWrapper);
        return ResponseData.success(users);
    }

    // 根据状态查询报名用户
    @Operation(summary = "考试报名用户根据状态获取", description = "根据用户状态查询并返回符合条件的用户列表")
    @GetMapping("/examuserstatus/{status}")
    public ResponseData getByStatus(@PathVariable String status) {
        QueryWrapper<App1ExamRegistrationUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", status);
        List<App1ExamRegistrationUser> users = examRegistrationUserService.list(queryWrapper);
        return ResponseData.success(users);
    }

    // 修改报名用户
    @Operation(summary = "考试报名用户修改明细", description = "根据考试人员编码更新明细表")
    @PutMapping("/examuserupdate")
    public ResponseData update(@RequestBody App1ExamRegistrationUser user) {
        boolean isSuccess = examRegistrationUserService.updateById(user);
        if (isSuccess) {
            return ResponseData.success("用户信息更新成功");
        } else {
            return ResponseData.failed("用户信息更新失败", "");
        }
    }

    // 删除报名用户
    @Operation(summary = "考试报名用户删除", description = "根据用户ID删除考试报名用户信息")
    @DeleteMapping("/examuser/{id}")
    public ResponseData delete(@PathVariable Integer id) {
        boolean isSuccess = examRegistrationUserService.removeById(id);
        if (isSuccess) {
            return ResponseData.success("用户删除成功");
        } else {
            return ResponseData.failed("用户删除失败", "");
        }
    }
    // 根据人员编码查询报名用户
    @Operation(summary = "考试报名用户根据人员编码查询", description = "根据人员编码获取报名用户信息")
    @GetMapping("/examsearchByPersonCode")
    public ResponseData searchByPersonCode(@RequestParam String personCode) {
        QueryWrapper<App1ExamRegistrationUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("person_code", personCode);
        List<App1ExamRegistrationUser> users = examRegistrationUserService.list(queryWrapper);

        if (users.isEmpty()) {
            return ResponseData.failed("未找到对应的报名用户", "");
        }

        return ResponseData.success(users);
    }

    // 添加报名用户
    @Operation(summary = "考试报名用户添加", description = "新增一个考试报名用户")
    @PostMapping("/examuseradd")
    public ResponseData add(@RequestBody App1ExamRegistrationUser user) {
        // 设置默认状态和报名时间
        user.setStatus("已报名");
        user.setRegistrationDate(LocalDateTime.now());

        boolean isSuccess = examRegistrationUserService.save(user);
        if (isSuccess) {
            return ResponseData.success("用户添加成功");
        } else {
            return ResponseData.failed("用户添加失败", "");
        }
    }

}