package com.telecom.sxint.app.api.controller.Vatinvoincecontroller;

import com.telecom.sxint.app.core.service.Vatinvoiceservice.IApp1VatinvoinceprintlogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.List;

@RestController
@RequestMapping("/app1-vatinvoinceprintlog")
@Tag(name = "信息导出控制器", description = "信息导出管理")
@RequiredArgsConstructor
public class FileExportController {
    @Autowired
    private final IApp1VatinvoinceprintlogService iApp1VatinvoinceprintlogService;
    /**
     * 导出部分记录
     */
    @Operation(summary = "导出指定记录为excel", description = "返回下载")
    @PostMapping ("/FileExport")
    public void exportExcel(@RequestBody List<Integer> ids,HttpServletResponse response){
        iApp1VatinvoinceprintlogService.exportExcel(ids,response);
    }

    /**
     * 导出全部记录
     */
    @Operation(summary = "导出全部记录为excel", description = "返回下载")
    @GetMapping ("/AllFileExport")
    public void exportAllExcel(HttpServletResponse response)  {
        iApp1VatinvoinceprintlogService.exportAllExcel(response);
    }

    /**
     * 导出专票信息excel模板
     * @return
     */
    @Operation(summary = "导出专票信息excel模板", description = "导出位置在D:\\App1Vatinvoinceprintlog.xlsx")
    @GetMapping("/exportTicketTemplate")
    public ResponseEntity<ByteArrayResource> exportTicketTemplate() {
        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            // 创建一个空白工作表
            XSSFSheet sheet = workbook.createSheet("Blank Template");

            // 创建标题行
            XSSFRow titleRow = sheet.createRow(0);
            titleRow.createCell(0).setCellValue("组织");
            titleRow.createCell(1).setCellValue("合同号");
            titleRow.createCell(2).setCellValue("打印周期");
            titleRow.createCell(3).setCellValue("专票流水号");
            titleRow.createCell(4).setCellValue("金额");
            titleRow.createCell(5).setCellValue("是否已打印");
            titleRow.createCell(6).setCellValue("录入人员");
            titleRow.createCell(7).setCellValue("创建日期");
            titleRow.createCell(8).setCellValue("专票周期");

            // 创建一个示例数据行
            XSSFRow dataRow = sheet.createRow(1); // 第一行数据从索引1开始
            dataRow.createCell(0).setCellValue("政企客户部");
            dataRow.createCell(1).setCellValue("27500a640243");
            dataRow.createCell(2).setCellValue("202408");
            dataRow.createCell(3).setCellValue("10000520");
            dataRow.createCell(4).setCellValue("877.35");
            dataRow.createCell(5).setCellValue("否");
            dataRow.createCell(6).setCellValue("高峰");
            dataRow.createCell(7).setCellValue("20240806");
            dataRow.createCell(8).setCellValue("202407");
            // 创建其他行和单元格，留空供用户填写

            // 将工作簿转换为字节流
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            byte[] bytes = bos.toByteArray();
            // 创建Resource对象
            ByteArrayResource resource = new ByteArrayResource(bytes);

            // 设置HTTP响应头
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=TicketTemplate.xlsx");

            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } catch (Exception e) {
            e.printStackTrace();
            // 处理异常
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

}