package com.telecom.sxint.app.api.controller.SystemController.App1MonthlyController;


import cn.zjtele.pubinfo.boot.redis.config.RedisHelper;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo.App1Assessment;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1QuarterPublicSheet;
import com.telecom.sxint.app.core.service.Systemservice.App1Monthlyservice.IApp1AssessmentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2024-08-27
 */
@RestController
@RequestMapping("/app1-system")
@Tag(name = "月度指标操作控制器", description = "月度指标操作控制器")
public class App1AssessmentController {
    @Autowired
    private IApp1AssessmentService assessmentService;
    @Autowired
    private RedisHelper redisHelper;

    String cacheKey = "Assessment:list";

    @Operation(summary = "添加新的考核项")
    @PostMapping("/add")
    public ResponseData<App1Assessment> addAssessment(@RequestBody App1Assessment assessment) {
        assessmentService.save(assessment);
        redisHelper.delete(cacheKey);
        return ResponseData.success(assessment);
    }

    @Operation(summary = "更新考核项")
    @PutMapping("/update")
    public ResponseData<App1Assessment> updateAssessment(@RequestBody App1Assessment assessment) {
        assessmentService.updateById(assessment);
        redisHelper.delete(cacheKey);
        return ResponseData.success(assessment);
    }

    @Operation(summary = "根据ID删除考核项")
    @DeleteMapping("/delete/{id}")
    public ResponseData<Void> deleteAssessment(@PathVariable("id") Integer id) {
        UpdateWrapper<App1Assessment> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1
        assessmentService.update(null, updateWrapper);
        redisHelper.delete(cacheKey);
        return ResponseData.success();
    }

    @Operation(summary = "批量删除考核项")
    @DeleteMapping("/delete")
    public ResponseData deleteAssessments(@RequestBody List<Integer> ids) {
//        assessmentService.removeByIds(ids);
//        return ResponseData.success();
        if(ids==null||ids.isEmpty()){
            return ResponseData.success("未选择删除记录");
        }
//        else if(!iApp1EmployeehoursService.removeBatchByIds(ids)){
//            return ResponseData.success("删除失败");
//        }
        UpdateWrapper<App1Assessment> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in("id", ids) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1

        // 调用服务类的 update 方法进行批量更新
        assessmentService.update(null, updateWrapper);
        redisHelper.delete(cacheKey);
        return ResponseData.success("删除成功");
    }

    @Operation(summary = "查询所有考核项")
    @GetMapping("/list")
    public ResponseData<List<App1Assessment>> listAssessments() {
        List<App1Assessment> cacheData = (List<App1Assessment>) redisHelper.valueGet(cacheKey);
        if (cacheData != null && !cacheData.isEmpty()) {
            return ResponseData.success(cacheData);
        }
        QueryWrapper<App1Assessment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0)
                .orderByAsc("id"); // 只查询 is_delete 字段为 0 的记录
        List<App1Assessment> list = assessmentService.list(queryWrapper);
        redisHelper.valueSet(cacheKey,list);
        return ResponseData.success(list);
    }

    @Operation(summary = "根据ID查询考核项")
    @GetMapping("/get/{id}")
    public ResponseData<App1Assessment> getAssessment(@PathVariable("id") Integer id) {
        App1Assessment assessment = assessmentService.getById(id);
        // 检查is_delete字段是否为0
        if (assessment != null && assessment.getIsDelete() == 0) {
            // 如果is_delete为0，返回成功响应
            return ResponseData.success(assessment);
        } else {
            // 如果is_delete为1或对象为空，返回空结果或错误信息
            return ResponseData.failed("未找到有效的工程师数据","40000" );
        }
    }
    @Operation(summary = "根据assessedDepartment查询考核项")
    @GetMapping("/bydepartment")
    public ResponseData getByAssessedDepartment(@RequestParam String assessedDepartment) {
        QueryWrapper<App1Assessment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete",0)
                .eq("assessed_department", assessedDepartment)
                .orderByAsc("id");  // 按照 id 升序排序;
        List<App1Assessment> list = assessmentService.list(queryWrapper);
        return ResponseData.success(list);
    }

}
