package com.telecom.sxint.app.api.controller.SystemController.App1SystemFileController;

import org.apache.poi.ss.usermodel.Sheet;

import com.alibaba.excel.write.handler.SheetWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteWorkbookHolder;

/**
 * 自定义sheet样式
 *
 * @author 睡竹
 * @date 2022年5月20日
 */
public class CustomSheetWriteHandler implements SheetWriteHandler {

    @Override
    public void afterSheetCreate(WriteWorkbookHolder writeWorkbookHolder, WriteSheetHolder writeSheetHolder) {
        Sheet sheet = writeSheetHolder.getSheet();
        // 去除网格线
        sheet.setDisplayGridlines(false);
    }

}