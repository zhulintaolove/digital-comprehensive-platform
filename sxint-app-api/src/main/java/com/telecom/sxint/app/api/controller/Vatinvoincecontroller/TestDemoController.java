package com.telecom.sxint.app.api.controller.Vatinvoincecontroller;

import cn.zjtele.pubinfo.common.dict.annotation.DictTranslate;
import cn.zjtele.pubinfo.common.pagination.PageBase;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.telecom.sxint.app.core.domain.entity.TestDemo;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import cn.zjtele.pubinfo.common.pagination.PageData;
import cn.zjtele.pubinfo.boot.repository.utils.PageUtil;
import com.telecom.sxint.app.core.service.TestDemoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * 测试信息表
 *
 * @author sx_tele
 * @since 2024-07-11
 */

@RestController
@RequestMapping("/testDemo")
@Tag(name = "测试信息表控制器", description = "测试信息表管理")
public class TestDemoController {

    @Resource
    private TestDemoService testDemoServiceImpl;


    @DictTranslate
    @GetMapping("/getAll")
    @Operation(summary = "获取所有TestDemo", description = "返回所有测试信息表列表")
    public ResponseData<List<TestDemo>> getAll() {
        return ResponseData.success(testDemoServiceImpl.list());
    }

    @PostMapping("/pageQuery")
    @Operation(summary = "分页查询TestDemo", description = "返回测试信息表分页")
    public ResponseData<PageData<TestDemo>> pageQuery(@RequestBody PageBase pageBase) {
        IPage<TestDemo> pageData = testDemoServiceImpl.lambdaQuery().page(PageUtil.toMyBatisPlusPage(pageBase));
        return ResponseData.success(PageUtil.toPubPageData(pageData));
    }

    @GetMapping("/getById")
    @Operation(summary = "根据ID获取测试信息表", description = "返回指定ID的测试信息表")
    public ResponseData<TestDemo> getById(@RequestParam Serializable id) {
        return ResponseData.success(testDemoServiceImpl.getById(id));
    }

    @PostMapping("/save")
    @Operation(summary = "创建测试信息表", description = "根据请求体创建一个新的测试信息表")
    public ResponseData<TestDemo> save(@RequestBody TestDemo entityNameLower) {
        testDemoServiceImpl.save(entityNameLower);
        return ResponseData.success(entityNameLower);
    }

    @PostMapping("/update")
    @Operation(summary = "更新TestDemo", description = "根据ID更新TestDemo的详细信息")
    public ResponseData<TestDemo> update(@RequestBody TestDemo entityNameLower) {
        testDemoServiceImpl.updateById(entityNameLower);
        return ResponseData.success(entityNameLower);
    }

    @DeleteMapping("/delete")
    @Operation(summary = "删除TestDemo", description = "根据ID删除TestDemo")
    public ResponseData<Void> delete(@RequestParam Serializable id) {
        testDemoServiceImpl.removeById(id);
        return ResponseData.success();
        //haha
    }

}