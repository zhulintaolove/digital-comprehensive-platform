package com.telecom.sxint.app.api.controller.Vatinvoincecontroller;
import cn.zjtele.pubinfo.boot.redis.config.RedisHelper;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkOperationSummary;
import com.telecom.sxint.app.core.domain.entity.Vatinvoicepojo.App1Vatinvoinceprintlog;
import com.telecom.sxint.app.core.domain.entity.dto.InvoiceFilterDTO;
import com.telecom.sxint.app.core.domain.entity.dto.TicketQueryDTO;
import com.telecom.sxint.app.core.service.Vatinvoiceservice.IApp1VatinvoinceprintlogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2024-08-07
 */
@RestController
@Slf4j
@Tag(name = "数据操作控制器", description = "数据操作管理")
@RequestMapping("/app1-vatinvoinceprintlog")
@RequiredArgsConstructor
public class App1VatinvoinceprintlogController {
    private final IApp1VatinvoinceprintlogService iApp1VatinvoinceprintlogService;
    @Autowired
    private RedisHelper redisHelper;
    String cacheKey = "Vatinvoinceprintlog:list";
    /**
     * 单挑专票插入
     * @param App1Vatinvoinceprintlog
     * @return
     */
    @Operation(summary = "导入单条记录录入数据库", description = "返回成功信息")
    @PostMapping("/SingleUpload")
    public ResponseData handleSingleUpload(@RequestBody App1Vatinvoinceprintlog App1Vatinvoinceprintlog){
        redisHelper.delete(cacheKey);
        return iApp1VatinvoinceprintlogService.handleSingleUpload(App1Vatinvoinceprintlog);
    }
    /**
     * 修改专票
     * @param app1Vatinvoinceprintlog
     * @return
     */
    @Operation(summary = "修改专票", description = "根据id更新专票")
    @PostMapping ("/update")
    public ResponseData<App1Vatinvoinceprintlog> ticketupdate(@RequestBody App1Vatinvoinceprintlog app1Vatinvoinceprintlog) {
        log.info("修改专票id为"+app1Vatinvoinceprintlog.getId());
        App1Vatinvoinceprintlog log1=iApp1VatinvoinceprintlogService.updateByIdrdate(app1Vatinvoinceprintlog);
        redisHelper.delete(cacheKey);
        return ResponseData.success(log1);
    }
    /**
     * 导出部分记录
     */
    @Operation(summary = "修改打印状态", description = "修改打印状态")
    @PostMapping ("/updatePrintflag")
    public ResponseData updatePrintflag(@RequestBody List<Integer> ids){
        redisHelper.delete(cacheKey);
        return iApp1VatinvoinceprintlogService.updatePrintflag(ids);
    }
    /**
     * 关键字搜索专票
     * @param keyword
     * @return
     */
    @Operation(summary = "搜索专票记录", description = "搜索专票记录")
    @GetMapping("/searchkeyword")
    public ResponseData searchkeyword(@RequestParam String keyword) {
        List<App1Vatinvoinceprintlog> results  = iApp1VatinvoinceprintlogService.searchByKeyword(keyword);
        if (results==null||results.isEmpty()) {
            return ResponseData.success("未查到相关记录");
        }
        return ResponseData.success(results);
    }

    /**
     * 根据组织关键字搜索专票
     * @param keyword
     * @param area
     * @return
     */
    @Operation(summary = "搜索组织专票记录", description = "搜索组织专票记录")
    @GetMapping("/searchkeywordbyarea")
    public ResponseData searchkeywordByArea(@RequestParam String keyword,String area) {
        List<App1Vatinvoinceprintlog> results  = iApp1VatinvoinceprintlogService.searchByKeyword2(keyword,area);
        if (results==null||results.isEmpty()) {
            return ResponseData.success("未查到相关记录");
        }
        return ResponseData.success(results);
    }
    /**
     * 根据组织专票查询
     * @param area
     * @return
     */
    @Operation(summary = "查询组织所有专票记录", description = "根据组织查询所有专票记录")
    @GetMapping("/listbyarea")
    public ResponseData getListByArea(@RequestParam String area) {
        List<App1Vatinvoinceprintlog> results = iApp1VatinvoinceprintlogService
                .lambdaQuery()
                .eq(App1Vatinvoinceprintlog::getIsDelete,0)
                .eq(App1Vatinvoinceprintlog::getArea,area)
                .orderByDesc(App1Vatinvoinceprintlog::getCreated)
                .list();
        if (results==null||results.isEmpty()) {
            return ResponseData.failed("未查到相关记录","4001");
        }
        return ResponseData.success(results);
    }

    /**
     * 查询全部
     */
    @Operation(summary = "查询所有专票记录", description = "根据查询所有专票记录")
    @GetMapping("/list")
    public ResponseData getList() {
        List<App1Vatinvoinceprintlog> cacheData = (List<App1Vatinvoinceprintlog>) redisHelper.valueGet(cacheKey);
        if (cacheData != null && !cacheData.isEmpty()) {
            return ResponseData.success(cacheData);
        }
        QueryWrapper<App1Vatinvoinceprintlog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0); // 只查询 is_delete 字段为 0 的记录
        // 查询数据库，获取符合条件的记录
        List<App1Vatinvoinceprintlog> results = iApp1VatinvoinceprintlogService.list(queryWrapper);
        if (results==null||results.isEmpty()) {
            return ResponseData.failed("未查到相关记录","4001");
        }
        redisHelper.valueSet(cacheKey,results);
        return ResponseData.success(results);
    }

    /**
     * 专票查询
     * @param filter
     * @return
     */
    @Operation(summary = "查询专票记录", description = "根据不同条件查询专票记录")
    @GetMapping("/search")
    public ResponseData getInvoices(@RequestBody InvoiceFilterDTO filter) {
        // 在这里你可以根据 filter 中的条件进行查询

        // 查询数据库，获取符合条件的记录
        List<App1Vatinvoinceprintlog> results = iApp1VatinvoinceprintlogService.findInvoicesByFilter(filter);
        if (results==null||results.isEmpty()) {
            return ResponseData.failed("未查到相关记录","4001");
        }
        return ResponseData.success(results);
    }

    /**
     * 删除查询
     * @param id
     * @return
     */
    @Operation(summary = "删除传票记录", description = "根据id删除传票记录")
    @DeleteMapping("/delete")
    public ResponseData deletById(@RequestParam Integer id) {
        UpdateWrapper<App1Vatinvoinceprintlog> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1
        iApp1VatinvoinceprintlogService.update(null, updateWrapper);
        redisHelper.delete(cacheKey);
        return ResponseData.success("删除成功");
    }
    /**
     * 批量删除查询
     * @param ids
     * @return
     */
    @Operation(summary = "批量删除传票记录", description = "根据ids删除传票记录")
    @DeleteMapping("/deleteids")
    public ResponseData deletByIds(@RequestBody List<Integer> ids) {
        if(ids==null||ids.isEmpty())return ResponseData.success("未选择删除记录");
        UpdateWrapper<App1Vatinvoinceprintlog> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in("id", ids) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1

        // 调用服务类的 update 方法进行批量更新
        iApp1VatinvoinceprintlogService.update(null, updateWrapper);
        redisHelper.delete(cacheKey);
        return ResponseData.success("删除成功");
    }
    /**
     * 清空表
     */
    @Operation(summary = "清空传票记录", description = "清空专票记录")
    @DeleteMapping("/deleteall")
    public ResponseData deleteAll() {
        UpdateWrapper<App1Vatinvoinceprintlog> updateWrapper = new UpdateWrapper<>();
        // 设置 is_delete 字段的值为 1
        updateWrapper.set("is_delete", 1);
        // 调用服务类的 update 方法进行更新
        // 这里传入 null 作为实体对象，因为我们不需要更新除了 is_delete 之外的其他字段
        iApp1VatinvoinceprintlogService.update(null, updateWrapper);
        redisHelper.delete(cacheKey);
        return ResponseData.success("清空成功");
    }
    /**
     * 组织清空表
     */
    @Operation(summary = "清空传票记录", description = "清空专票记录")
    @DeleteMapping("/deleteAllbyArea")
    public ResponseData deleteAllbyArea(@RequestParam String area) {
        List<App1Vatinvoinceprintlog> results =iApp1VatinvoinceprintlogService
                .lambdaQuery()
                .eq(App1Vatinvoinceprintlog::getIsDelete,0)
                .eq(App1Vatinvoinceprintlog::getArea,area)
                .list();
        if (results==null||results.isEmpty()) {
            return ResponseData.failed("未查到相关记录","4001");
        }
        UpdateWrapper<App1Vatinvoinceprintlog> updateWrapper = new UpdateWrapper<>();
        // 设置 is_delete 字段的值为 1
        updateWrapper.eq("area", area)
                .set("is_delete", 1);
        // 调用服务类的 update 方法进行更新
        // 这里传入 null 作为实体对象，因为我们不需要更新除了 is_delete 之外的其他字段
        iApp1VatinvoinceprintlogService.update(null, updateWrapper);
        redisHelper.delete(cacheKey);
        return ResponseData.success("清空成功");
    }
    /**
     * 分页查询
     * @param ticketQueryDTO
     * @return
     */
    @PostMapping("/page")
    @Operation(summary = "分页查询", description = "专票分页查询")

    public ResponseData<PageData<App1Vatinvoinceprintlog>> queryTicketPage(@RequestBody TicketQueryDTO ticketQueryDTO){

        System.out.println(ticketQueryDTO.getCurrentPage());

        PageData pageData=iApp1VatinvoinceprintlogService.queryTicketPage(ticketQueryDTO);


        return ResponseData.success(pageData);
    }

    /**
     * 根据组织分页查询
     * @param ticketQueryDTO
     * @return
     */
    @PostMapping("/byAreaPage")
    @Operation(summary = "根据组织分页专票查询", description = "根据组织分页专票查询")

    public ResponseData<PageData<App1Vatinvoinceprintlog>> queryAreaTicketPage(@RequestBody TicketQueryDTO ticketQueryDTO,String area){

        System.out.println(ticketQueryDTO.getCurrentPage());

        PageData pageData=iApp1VatinvoinceprintlogService.queryAreaTicketPage(ticketQueryDTO,area);


        return ResponseData.success(pageData);
    }
}
