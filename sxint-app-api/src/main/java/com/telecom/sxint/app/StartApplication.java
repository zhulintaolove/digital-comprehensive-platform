package com.telecom.sxint.app;

import cn.zjtele.pubinfo.cloud.openfeign.config.FeignGlobalConfig;
import org.springframework.cloud.openfeign.EnableFeignClients;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.core.env.Environment;

/**
 *
 * @author sx_tele
 * @since 2024-07-11
 */

@EnableCaching
@SpringBootApplication
@Slf4j
@EnableDiscoveryClient
@MapperScan("com.telecom.sxint.app.core.mapper")
@EnableFeignClients(basePackages = {"com.telecom.sxint.**.client"}, defaultConfiguration = FeignGlobalConfig.class)
public class StartApplication {

    public static void main(String[] args) {
        Environment env = SpringApplication.run(StartApplication.class, args).getEnvironment();
        String serverPort = env.getProperty("server.port");
        String contextPath = env.getProperty("server.servlet.context-path");
        String protocol = "http";
        log.info("\n----------------------------------------------------------\n" +
                        "Application '{}' is running! Access URLs:\n" +
                        "API doc: \t{}://localhost:{}{}/doc.html\n" +
                        "Profile(s): \t{}\n----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                protocol,
                serverPort,
                contextPath,
                env.getActiveProfiles());
    }
}