package com.telecom.sxint.app.api.controller.ChatController;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.alibaba.fastjson2.JSONObject;
import com.telecom.sxint.app.core.Constant.ChatConstants;
import com.telecom.sxint.app.core.domain.entity.dto.QueryParamDTO;
import com.telecom.sxint.app.core.service.Chatservice.ChatApi;
import com.telecom.sxint.app.core.service.Chatservice.ChatApi2;
import com.telecom.sxint.app.core.service.Chatservice.SpeechTextApi;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    private ChatApi chatApi;
    @Autowired
    private ChatApi2 chatApi2;
    @Operation(summary = "开启会话", description = "开启会话")
    @PostMapping("/startConversation")
    public ResponseData startConversation() {
        return ResponseData.success(chatApi.startConversation());
    }
    @Operation(summary = "获取会话地址", description = "获取会话地址")
    @PostMapping("/baseUrl")
    public ResponseData baseUrl() {
        return ResponseData.success(ChatConstants.baseUrl);
    }
    @Operation(summary = "语音文件转换为文本，然后将回答转换为语音", description = "语音文件转换为文本，然后将回答转换为语音")
    @PostMapping("/speechQuery")
    public ResponseData speechQuery(@RequestParam String conversationId, @RequestParam MultipartFile file) {
        try {
            String text = SpeechTextApi.speech2Text(file);
            JSONObject chatResponse = chatApi.runConversation(conversationId, text);
            String answer = chatApi.getCleanAnswer(chatResponse);
            JSONObject response = SpeechTextApi.createText2Speech(answer);
            response.put("textAnswer", answer);
            return ResponseData.success(response);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseData.failed("语音IO错误",null);
        }
    }
    @Operation(summary = "文本转语音", description = "文本转语音")
    @PostMapping("/textQuery")
    public ResponseData textQuery(@RequestBody @Validated QueryParamDTO queryParam) {
        try {
            JSONObject chatResponse = chatApi.runConversation(queryParam.getConversationId(), queryParam.getText());
            String answer = chatApi.getCleanAnswer(chatResponse);
            JSONObject response = SpeechTextApi.createText2Speech(answer);
            response.put("textAnswer", answer);
            return ResponseData.success(response);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseData.failed("语音IO错误", null);
        }
    }
    @Operation(summary = "文件下载请求", description = "语音下载请求")
    @GetMapping("/speechDownload")
    public ResponseData speechDownload(@RequestParam String taskId) {
        try {
            JSONObject response = SpeechTextApi.queryText2Speech(taskId);
            return ResponseData.success(response);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseData.failed("语音IO错误",null);
        }
    }
    @Operation(summary = "语音文件转文本", description = "语音文件转文字")
    @PostMapping("/speechToText")
    public ResponseData speechToText(@RequestParam MultipartFile file) {
        try {
            String text = SpeechTextApi.speech2Text(file);
            return ResponseData.success("转换后文字", text);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseData.failed("语音IO错误", null);
        }
    }
    @Operation(summary = "上下文联系", description = "连续提问相关")
    @PostMapping(value = "textQueryFlux", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> textQueryFlux(@RequestBody @Validated QueryParamDTO queryParam) {
        return chatApi2.runConversation(queryParam.getConversationId(), queryParam.getText());
    }
}
