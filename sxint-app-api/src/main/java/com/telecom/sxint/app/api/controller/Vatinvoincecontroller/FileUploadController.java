package com.telecom.sxint.app.api.controller.Vatinvoincecontroller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Employeehours;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Engineercertification;
import com.telecom.sxint.app.core.domain.entity.TestDemo;
import com.telecom.sxint.app.core.service.Engineerservice.IApp1EmployeehoursService;
import com.telecom.sxint.app.core.service.Engineerservice.IApp1EngineercertificationService;
import io.swagger.annotations.ApiOperation;
import com.telecom.sxint.app.core.domain.entity.Vatinvoicepojo.App1Vatinvoinceprintlog;
import com.telecom.sxint.app.core.service.Vatinvoiceservice.IApp1VatinvoinceprintlogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zhulintao
 * @version 2.0
 * @date 2024/8/9 10:30
 */
@RestController
@RequestMapping("/app1-vatinvoinceprintlog")
@Tag(name = "信息录入控制器", description = "信息录入管理")
@RequiredArgsConstructor
public class FileUploadController {
    private final IApp1VatinvoinceprintlogService iApp1VatinvoinceprintlogService;

    private final IApp1EngineercertificationService iApp1EngineercertificationService;

    private final IApp1EmployeehoursService iApp1EmployeehoursService;

    String getCellValue(Cell cell) {
        if (cell != null) {
            switch (cell.getCellType()) {
                case STRING:
                    return cell.getStringCellValue();
                case NUMERIC:
                    return String.valueOf(cell.getNumericCellValue());
                case BOOLEAN:
                    return String.valueOf(cell.getBooleanCellValue());
                default:
                    return "";
            }
        }
        return "";
    }
    /**
     * 导入文件
     * @param file
     * @return
     */
    @Operation(summary = "导入excel文档录入数据库", description = "返回测试信息表分页")
    @PostMapping("/Fileupload")
    public ResponseData<List<App1Vatinvoinceprintlog>>  handleFileUpload(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return ResponseData.failed("没有文件","4101");
        }
        String fileName = file.getOriginalFilename();
        if (!fileName.endsWith(".xls") && !fileName.endsWith(".xlsx")) {
            return ResponseData.failed("文件格式错误","4103");
        }
        try (InputStream inputStream = file.getInputStream();

             Workbook workbook = WorkbookFactory.create(inputStream)) {

            Sheet sheet = workbook.getSheetAt(0);
            if (sheet == null) {
                // 工作表为空，返回错误信息或空列表
                return ResponseData.failed("没有可读取的工作表","4100");
            }

            List<App1Vatinvoinceprintlog> app1= new ArrayList<>();
            for (Row row : sheet) {
                if (row.getRowNum() == 0) continue; // Skip header row if present

                App1Vatinvoinceprintlog log = new App1Vatinvoinceprintlog();
                int id = 0;
                if (row.getCell(0) != null) {
                    // 检查单元格是否包含一个数字
                    if (row.getCell(0).getCellType() == CellType.NUMERIC) {
                        // 将单元格的数值转换为整数
                        id = (int) row.getCell(0).getNumericCellValue();
                    } else {
                        // 单元格不是数值类型，根据需要处理错误或返回默认值
                    }
                }
                log.setId(id);
                log.setArea(getCellValue(row.getCell(1)));
                log.setAccount(getCellValue(row.getCell(2)));
                log.setPrintmonth(getCellValue(row.getCell(3)));
                log.setInvoice(getCellValue(row.getCell(4)));
                log.setAmount(getCellValue(row.getCell(5)));
                log.setPrintflag(getCellValue(row.getCell(6)));
                log.setOperator(getCellValue(row.getCell(7)));
                log.setCreated(getCellValue(row.getCell(8)));
                log.setBillmonth(getCellValue(row.getCell(9)));
                String account = log.getAccount();
                String invoice = log.getInvoice();
                if (account.equals("") && invoice.equals("")) {
                    // 如果 ACCOUNT 和 INVOICE 都为空，跳过当前插入操作
                    System.out.println("ACCOUNT 和 INVOICE 都为空，跳过当前插入操作");
                } else {
                    // 处理日志和保存操作
                    app1.add(log);
                }


            }
            iApp1VatinvoinceprintlogService.saveBatch(app1);
            return ResponseData.success(app1);

        } catch (IOException e) {
            return ResponseData.failed("导入失败","4102");
        }
    }
}
