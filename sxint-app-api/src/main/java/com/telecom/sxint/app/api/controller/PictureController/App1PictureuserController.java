package com.telecom.sxint.app.api.controller.PictureController;

import cn.hutool.json.JSONObject;
import cn.zjtele.pubinfo.common.http.request.RequestContextHolder;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Messagepojo.App1UccpMessage;
import com.telecom.sxint.app.core.domain.entity.Messagepojo.App1UccpMessageParams;
import com.telecom.sxint.app.core.domain.entity.Picturepojo.App1Pictureuser;
import com.telecom.sxint.app.core.domain.entity.dto.App1PictureuserDTO;
import com.telecom.sxint.app.core.service.Pictureservice.IApp1PictureuserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Tag(name = "用户图片管理控制器", description = "用户图片管理")
@RequestMapping("/app1-picturemanagement")
public class App1PictureuserController {

    @Autowired
    private IApp1PictureuserService iApp1PictureuserService;

    /**
     * 用户点击下载(未下载过新增，下载过增加下载次数)
     * @param pictureid
     * @param app1PictureuserDTO
     * @param response
     * @return
     * @throws UnsupportedEncodingException
     */
    @Operation(summary = "用户点击下载", description = "用户点击下载(未下载过新增，下载过增加下载次数)")
    @PostMapping("/insertuserpicture/{pictureid}")
    public ResponseData insertUserPicture(@PathVariable Integer pictureid, @RequestBody App1PictureuserDTO app1PictureuserDTO, HttpServletResponse response) throws UnsupportedEncodingException {
        return iApp1PictureuserService.insertUserPicture(pictureid,app1PictureuserDTO,response);
    }

    /**
     * 查询某个用户总共下载次数
     * @param tokenName
     * @return
     */
    @Operation(summary = "查询某个用户总共下载次数" ,description = "根据token_name查询某个用户总共下载次数")
    @GetMapping("/useralldownload")
    public ResponseData<List<Map<String,Object>>> userAllDownload(@RequestParam(value = "用户名",required = true) String tokenName) {
        List<Map<String,Object>> map= iApp1PictureuserService.userAllDownload(tokenName);
        return new ResponseData<>(map);
    }

    /**
     * 查询所有用户的图片记录
     * @return
     */
    @Operation(summary = "查询所有用户的图片记录", description = "查询所有用户的图片记录")
    @GetMapping("/Pictureuserlist")
    public ResponseData<List<App1Pictureuser>> listAllUsers() {
        List<App1Pictureuser> users = iApp1PictureuserService.list();
        return ResponseData.success(users);
    }

    /**
     * 根据ID获取用户图片下载信息
     * @param id
     * @return
     */
    // 根据ID查询接口
    @Operation(summary = "根据ID获取用户图片下载信息")
    @GetMapping("/Pictureuserget/{id}")
    public ResponseData getPictureUserById(@PathVariable Integer id) {
        App1Pictureuser pictureUser = iApp1PictureuserService.getById(id);
        return pictureUser != null ? ResponseData.success(pictureUser) : ResponseData.failed("未找到该记录", "");
    }

    /**
     * 更新用户图片下载信息
     * @param pictureUser
     * @return
     */
    // 更新接口
    @Operation(summary = "更新用户图片下载信息")
    @PutMapping("/Pictureuserupdate")
    public ResponseData updatePictureUser(@RequestBody App1Pictureuser pictureUser) {
        boolean result = iApp1PictureuserService.updateById(pictureUser);
        return result ? ResponseData.success("更新成功") : ResponseData.failed("更新失败", "");
    }




}