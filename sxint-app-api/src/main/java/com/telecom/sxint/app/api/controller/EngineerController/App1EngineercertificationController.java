package com.telecom.sxint.app.api.controller.EngineerController;


import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Employeehours;
import com.telecom.sxint.app.core.domain.entity.Engineerpojo.App1Engineercertification;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.domain.entity.dto.CertificationQueryDTO;
import com.telecom.sxint.app.core.service.Engineerservice.IApp1EmployeehoursService;
import com.telecom.sxint.app.core.service.Engineerservice.IApp1EngineercertificationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author
 * @since 2024-08-19
 */
@RestController
@Tag(name = "工程师数据操作控制器", description = "工程师数据操作管理")
@RequestMapping("/app1-engineer")
public class App1EngineercertificationController {
    @Autowired
    private IApp1EngineercertificationService iApp1EngineercertificationService;
    @Autowired
    private IApp1EmployeehoursService iApp1EmployeehoursService;

    /**
     * 根据编码查询工时
     * @param personnelCode
     * @return
     */
    @Operation(summary = "根据编码查询工时", description = "根据编码查询工时")
    @GetMapping("/getHoursByCode")
    public ResponseData getHoursByCode(@RequestParam String personnelCode){
        List<App1Employeehours> list = iApp1EmployeehoursService.queryAllById(personnelCode);
        Double sum = 0.0;
        for (App1Employeehours app1Employeehours : list) {
            sum += Double.parseDouble(String.valueOf(app1Employeehours.getReviewedHours()));
        }
        return ResponseData.success(sum);
    }
    /**
     * 修改明细
     * @param app1Engineercertification
     * @return
     */
    @Operation(summary = "修改明细", description = "根据人员编码更新明细表")
    @PostMapping("/certificationupdate")
    public ResponseData<App1Engineercertification> certificationupdate(@RequestBody App1Engineercertification app1Engineercertification) {
        App1Engineercertification log1=iApp1EngineercertificationService.updateByIdrdate(app1Engineercertification);
        return ResponseData.success(log1);
    }

    /**
     * 查询所有工程师数据
     * @return
     */
    @Operation(summary = "查询所有工程师数据")
    @GetMapping("/certificationlist")
    public ResponseData listAll() {
        List<App1Engineercertification> app1EngineercertificationList = iApp1EngineercertificationService.queryAllCertification();
        return ResponseData.success(app1EngineercertificationList);
    }

    /**
     * 根据ID查询工程师数据
     * @param id
     * @return
     */
    @Operation(summary = "根据ID查询工程师数据")
    @GetMapping("/certificationget/{id}")
    public ResponseData getById(@PathVariable Integer id) {
        return ResponseData.success(iApp1EngineercertificationService.queryById(id));
    }

    /**
     * 根据条件查询工程师数据
     * @param filter
     * @return
     */
    @Operation(summary = "根据条件查询工程师数据")
    @PostMapping("/certificationsearch")
    public ResponseData searchByConditions(@RequestBody App1Engineercertification filter) {
        List<App1Engineercertification> app1EngineercertificationList = iApp1EngineercertificationService.queryByConditions(filter);
        return ResponseData.success(app1EngineercertificationList);
    }
    /**
     * 明细表分页查询
     * @param certificationQueryDTO
     * @return
     */
    @PostMapping("/certificationpage")
    @Operation(summary = "明细查询", description = "明细表分页查询")

    public ResponseData<PageData<App1Engineercertification>> queryCertificationPage(@RequestBody CertificationQueryDTO certificationQueryDTO){

        PageData pageData=iApp1EngineercertificationService.queryCertificationPage(certificationQueryDTO);
        return ResponseData.success(pageData);
    }
    /**
     * 批量删除研究人员信息
     * @param ids
     * @return
     */
    @Operation(summary = "批量删除研究人员信息", description = "批量删除研究人员信息")
    @DeleteMapping("/certificationdeleteids")
    public ResponseData deletByIds(@RequestBody List<Integer> ids) {
        if(ids==null||ids.isEmpty())return ResponseData.success("未选择删除记录");
        iApp1EngineercertificationService.deletByIds(ids);
        return ResponseData.success("删除成功");
    }

    /**
     * 获取研发人员各等级区县分布
     * @param level
     * @return
     */
    @Operation(summary = "获取研发人员各等级区县分布", description = "传入等级获取该等级的人员区县分布")
    @PostMapping("/levelarea")
    public ResponseData<List<Map<String,Object>>> level1(@RequestParam Integer level){
        //获取分布区域和对应数量
        List<Map<String,Object>> log1=iApp1EngineercertificationService.getCompanyCountByLevel(level);

        return new ResponseData<>(log1);
    }

    /**
     * 获取研发人员认证级别缺失情况
     * @return
     */
    @Operation(summary = "获取研发人员认证级别缺失情况", description = "获取研发人员认证级别缺失情况")
    @GetMapping("/levellack")
    public ResponseData<List<Map<String,Object>>> level1(){
        List<Map<String, Object>> log2=iApp1EngineercertificationService.getLostSituation();
        return new ResponseData<>(log2);
    }
    /**
     * 根据组织分页查询工程师
     * @param certificationQueryDTO
     * @return
     */
    @PostMapping("/byCompanyPage")
    @Operation(summary = "根据组织分页查询明细", description = "根据组织分页查询明细")

    public ResponseData<PageData<App1Engineercertification>> queryByCompanyPage(@RequestBody CertificationQueryDTO certificationQueryDTO,String company){
        PageData pageData=iApp1EngineercertificationService.queryByCompanyPage(certificationQueryDTO,company);
        return ResponseData.success(pageData);
    }

    /**
     * 新增明细
     * @param app1Engineercertification
     * @return
     */
    @Operation(summary = "新增明细", description = "新增明细")
    @PostMapping("/certificationadd")
    public ResponseData<App1Engineercertification> certificationadd(@RequestBody App1Engineercertification app1Engineercertification) {
        return iApp1EngineercertificationService.addCertification(app1Engineercertification);
    }
}
