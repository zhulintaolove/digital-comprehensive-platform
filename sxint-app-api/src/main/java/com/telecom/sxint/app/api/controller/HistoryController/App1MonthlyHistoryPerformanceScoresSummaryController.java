package com.telecom.sxint.app.api.controller.HistoryController;


import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.telecom.sxint.app.core.domain.entity.Historypojo.App1HistoryAssessment;
import com.telecom.sxint.app.core.domain.entity.Historypojo.App1MonthlyHistoryPerformanceScoresSummary;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo.App1Assessment;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo.App1MonthlyPerformanceScoresSummary;
import com.telecom.sxint.app.core.service.Historyservice.IApp1HistoryAssessmentService;
import com.telecom.sxint.app.core.service.Historyservice.IApp1MonthlyHistoryPerformanceScoresSummaryService;
import com.telecom.sxint.app.core.service.Systemservice.App1Monthlyservice.IApp1AssessmentService;
import com.telecom.sxint.app.core.service.Systemservice.App1Monthlyservice.IApp1MonthlyPerformanceScoresSummaryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author
 * @since 2024-09-24
 */
@RestController
@Tag(name = "月度历史记录管理控制器", description = "月度历史记录管理")
@RequestMapping("/app1-systemhistory")
public class App1MonthlyHistoryPerformanceScoresSummaryController {

    @Autowired
    private IApp1MonthlyHistoryPerformanceScoresSummaryService app1MonthlyHistoryPerformanceScoresSummaryService;

    @Autowired
    private IApp1MonthlyPerformanceScoresSummaryService app1MonthlyPerformanceScoresSummaryService;
    @Autowired
    private IApp1AssessmentService app1AssessmentService;
    @Autowired
    private IApp1HistoryAssessmentService app1HistoryAssessmentService;
    /**
     * 新增历史记录
     */
    @Operation(summary = "定稿月度历史记录")
    @PostMapping("/addHistory")
    public ResponseData addHistory(String yearMonth){
        List<String> departNames=app1MonthlyPerformanceScoresSummaryService.getAllDepartName();
        List<App1MonthlyPerformanceScoresSummary> list = app1MonthlyPerformanceScoresSummaryService.getExist();
        for (App1MonthlyPerformanceScoresSummary sourceTable : list){
            App1MonthlyHistoryPerformanceScoresSummary targetTable = new App1MonthlyHistoryPerformanceScoresSummary();
            targetTable.setYearMonth(yearMonth);
            targetTable.setSequenceNumber(sourceTable.getSequenceNumber());
            targetTable.setGsScore(sourceTable.getGsScore());
            targetTable.setAbbreviation(sourceTable.getAbbreviation());
            targetTable.setIsDelete(0);
            targetTable.setDepartmentName(sourceTable.getDepartmentName());
            targetTable.setFunctionalWorkScore(sourceTable.getFunctionalWorkScore());
            targetTable.setPublicKpiScore(sourceTable.getPublicKpiScore());
            targetTable.setPartyBranchIndex(sourceTable.getPartyBranchIndex());
            targetTable.setReverseSupportScore(sourceTable.getReverseSupportScore());
            targetTable.setTotalScore(sourceTable.getTotalScore());
            targetTable.setReverseSupportScore(sourceTable.getReverseSupportScore());
            targetTable.setRanking(sourceTable.getRanking());
            app1MonthlyHistoryPerformanceScoresSummaryService.save(targetTable);
            //存入历史记录后删除
            app1MonthlyPerformanceScoresSummaryService.deleteById(sourceTable);
        }
        for(String departName:departNames) {
            List<App1Assessment> assessments = app1AssessmentService.selectWithDepartment(departName);
            for (App1Assessment assessment : assessments) {
                App1HistoryAssessment targetTable=new App1HistoryAssessment();
                targetTable.setYearMonth(yearMonth);
                targetTable.setAssessedDepartment(assessment.getAssessedDepartment());
                targetTable.setCategory(assessment.getCategory());
                targetTable.setAssessmentItem(assessment.getAssessmentItem());
                targetTable.setWeight(assessment.getWeight());
                targetTable.setTarget(assessment.getTarget());
                targetTable.setSelfScoringMethod(assessment.getSelfScoringMethod());
                targetTable.setNotes(assessment.getNotes());
                targetTable.setTargetValue(assessment.getTargetValue());
                targetTable.setCompletionValue(assessment.getCompletionValue());
                targetTable.setCompletionRateRank(assessment.getCompletionRateRank());
                targetTable.setScore(assessment.getScore());
                targetTable.setIsDelete(0);
                app1HistoryAssessmentService.save(targetTable);
                //存入历史记录后删除
                app1AssessmentService.deleteById(assessment);
            }
        }
        return ResponseData.success("新增成功");
    }

    @Operation(summary = "更新月度历史记录")
    @PostMapping("/updateHistory")
    public ResponseData updateHistory(@RequestBody App1MonthlyHistoryPerformanceScoresSummary app1MonthlyHistoryPerformanceScoresSummary){
        boolean success = app1MonthlyHistoryPerformanceScoresSummaryService.updateByIdAndDate(app1MonthlyHistoryPerformanceScoresSummary);
        return success? ResponseData.success("更新成功") : ResponseData.success("更新失败");
    }


    /**
     * 删除历史记录
     */
    @Operation(summary = "删除月度历史记录")
    @DeleteMapping("/deleteHistory")
    public ResponseData deleteHistory(@RequestParam Integer sequence_number) {
        UpdateWrapper<App1MonthlyHistoryPerformanceScoresSummary> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("sequence_number", sequence_number) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1
        app1MonthlyHistoryPerformanceScoresSummaryService.update(null, updateWrapper);
        return ResponseData.success("删除成功");
    }

    /**
     * 查询全部历史记录,分页显示
     */
    @Operation(summary = "查询全部月度历史记录")
    @PostMapping("/queryAllHistory")
    public ResponseData<IPage<App1MonthlyHistoryPerformanceScoresSummary>> queryAllHistory(
            @RequestParam(defaultValue = "1") Integer currentPage,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        Page<App1MonthlyHistoryPerformanceScoresSummary> page = new Page<>(currentPage, pageSize);
        IPage<App1MonthlyHistoryPerformanceScoresSummary> pageResult = app1MonthlyHistoryPerformanceScoresSummaryService.page(page);
        return ResponseData.success(pageResult);
    }

    /**
     * 根据年月查询历史记录，并分页显示
     */
    /**
     * 根据年月查询历史记录，并分页显示
     */
    @Operation(summary = "根据年月查询月度历史记录")
    @PostMapping("/queryHistoryByYearAndMonth")
    public ResponseData<List<App1MonthlyHistoryPerformanceScoresSummary>> queryHistoryByYearAndMonth(
            String yearMonth){
        QueryWrapper<App1MonthlyHistoryPerformanceScoresSummary> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("year_month", yearMonth) // 设置查询条件，这里假设id是查询条件
                .eq("is_delete", 0);
        List<App1MonthlyHistoryPerformanceScoresSummary> list = app1MonthlyHistoryPerformanceScoresSummaryService.list(queryWrapper);
        return ResponseData.success(list);
    }



}
