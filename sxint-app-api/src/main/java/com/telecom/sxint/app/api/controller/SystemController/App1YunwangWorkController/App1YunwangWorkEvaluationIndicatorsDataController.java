package com.telecom.sxint.app.api.controller.SystemController.App1YunwangWorkController;


import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkEvaluationIndicatorsData;
import com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice.IApp1YunwangWorkEvaluationIndicatorsDataService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
@RestController
@Tag(name = "云网工作评估数据", description = "云网工作评估数据")
@RequestMapping("/app1-system")
public class App1YunwangWorkEvaluationIndicatorsDataController {
    @Autowired
    private IApp1YunwangWorkEvaluationIndicatorsDataService app1YunwangWorkEvaluationIndicatorsDataService;

    /**
     * 单条修改
     * @param app1YunwangWorkEvaluationIndicatorsData
     * @return
     */
    @Operation(summary = "单条修改", description = "根据考核项目和部门指标修改数据")
    @PostMapping("update-dates")
    public ResponseData<App1YunwangWorkEvaluationIndicatorsData> updateDates(@RequestBody App1YunwangWorkEvaluationIndicatorsData app1YunwangWorkEvaluationIndicatorsData) {
        App1YunwangWorkEvaluationIndicatorsData data = app1YunwangWorkEvaluationIndicatorsDataService.updateByItemAndIndicator(app1YunwangWorkEvaluationIndicatorsData);
        return ResponseData.success(data);
    }

    /**
     * 查询
     * @param filter
     */
    @Operation(summary = "根据部门查询考核数据",description = "根据部门查询数据")
    @PostMapping("/searchByDataProvisionDepartment")
    public ResponseData<List<App1YunwangWorkEvaluationIndicatorsData>> searchByDataProvisionDepartment(
            @RequestBody App1YunwangWorkEvaluationIndicatorsData filter){
        QueryWrapper<App1YunwangWorkEvaluationIndicatorsData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("data_provision_department", filter.getDataProvisionDepartment());
        List<App1YunwangWorkEvaluationIndicatorsData> list = app1YunwangWorkEvaluationIndicatorsDataService.list(queryWrapper);
        return ResponseData.success(list);
    }

    /**
     * 根据条件查询
     * @param filter
     * @return
     */
    @Operation(summary = "根据条件查询考核数据",description = "根据条件查询考核数据")
    @PostMapping("/queryByConditions")
    public ResponseData<List<App1YunwangWorkEvaluationIndicatorsData>> queryByConditions(
            @RequestBody App1YunwangWorkEvaluationIndicatorsData filter){
        List<App1YunwangWorkEvaluationIndicatorsData> list = app1YunwangWorkEvaluationIndicatorsDataService.queryByConditions(filter);
        return ResponseData.success(list);
    }

    String getCellValue(Cell cell) {
        if (cell != null) {
            switch (cell.getCellType()) {
                case STRING:
                    return cell.getStringCellValue();
                case NUMERIC:
                    return String.valueOf(cell.getNumericCellValue());
                case BOOLEAN:
                    return String.valueOf(cell.getBooleanCellValue());
                default:
                    return "";
            }
        }
        return "";
    }

    public static void downloadUnfilledToXlsx(String excelName, HttpServletResponse response, Class cla, List list) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode(excelName, "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*="+fileName+ ".xlsx");
        WriteCellStyle headWriteCellStyle = new WriteCellStyle();
        WriteFont headWriteFont = new WriteFont();
        headWriteFont.setFontHeightInPoints((short) 18);
        headWriteCellStyle.setWriteFont(headWriteFont);
        headWriteFont.setBold(true);
        //内容样式策略
        WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
        contentWriteCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
        contentWriteCellStyle.setBorderLeft(BorderStyle.THIN);
        contentWriteCellStyle.setBorderTop(BorderStyle.THIN);
        contentWriteCellStyle.setBorderRight(BorderStyle.THIN);
        contentWriteCellStyle.setBorderBottom(BorderStyle.THIN);
        //设置 自动换行
        contentWriteCellStyle.setWrapped(true);
        // 字体策略
        WriteFont contentWriteFont = new WriteFont();
        // 字体大小
        contentWriteFont.setFontHeightInPoints((short) 10);
        contentWriteCellStyle.setWriteFont(contentWriteFont);
        EasyExcel.write(response.getOutputStream(), cla)
                .registerWriteHandler(new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle))
                .autoCloseStream(Boolean.TRUE)
                .sheet("网运")
                .doWrite(list);
    }


}
