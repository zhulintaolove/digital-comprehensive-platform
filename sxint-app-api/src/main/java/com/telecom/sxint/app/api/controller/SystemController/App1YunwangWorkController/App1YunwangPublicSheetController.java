package com.telecom.sxint.app.api.controller.SystemController.App1YunwangWorkController;


import cn.zjtele.pubinfo.boot.redis.config.RedisHelper;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1Monthlypojo.App1MonthlyPerformanceScoresSummary;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangPublicSheet;
import com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice.IApp1YunwangPublicSheetService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p >
 *
 * @author
 * @since 2024-09-03
 */
@RestController
@Tag(name = "公共云网工作评估指标", description = "公共云网工作评估")
@RequestMapping("/app1-system")
public class  App1YunwangPublicSheetController {
    @Autowired
    private IApp1YunwangPublicSheetService yunwangPublicSheetService;
    @Autowired
    private RedisHelper redisHelper;
    String cacheKey = "YunwangPublicSheet:list";
    @Operation(summary = "根据SheetName获取云网公共表记录")
    @GetMapping("/yunwangPublicSheetgetBySheetName/{SheetName}")
    public ResponseData getBySheetName(@PathVariable String SheetName) {
        List<App1YunwangPublicSheet> result = yunwangPublicSheetService.selectBySheetName(SheetName);
        return ResponseData.success(result);
    }
    /**
     * 新增云网公共表记录
     * @param publicSheet
     * @return
     */
    @Operation(summary = "新增云网公共表记录")
    @PostMapping("/yunwangPublicSheetcreate")
    public ResponseData create(@RequestBody App1YunwangPublicSheet publicSheet) {
        boolean result = yunwangPublicSheetService.save(publicSheet);
        redisHelper.delete(cacheKey);
        return result ? ResponseData.success("新增成功") : ResponseData.failed("新增失败", "");
    }

    /**
     * 根据ID获取云网公共表记录
     * @param id
     * @return
     */
    @Operation(summary = "根据ID获取云网公共表记录")
    @GetMapping("/yunwangPublicSheetget/{id}")
    public ResponseData getById(@PathVariable Integer id) {
        App1YunwangPublicSheet result = yunwangPublicSheetService.getById(id);
        // 检查is_delete字段是否为0
        if (result != null && result.getIsDelete() == 0) {
            // 如果is_delete为0，返回成功响应
            return ResponseData.success(result);
        } else {
            // 如果is_delete为1或对象为空，返回空结果或错误信息
            return ResponseData.failed("未找到有效的数据","40000" );
        }
    }

    /**
     * 获取所有云网公共表记录
     * @return
     */
    @Operation(summary = "获取所有云网公共表记录")
    @GetMapping("/yunwangPublicSheetlist")
    public ResponseData list() {
        List<App1YunwangPublicSheet> cacheData = (List<App1YunwangPublicSheet>) redisHelper.valueGet(cacheKey);
        if (cacheData != null && !cacheData.isEmpty()) {
            return ResponseData.success(cacheData);
        }
        List<App1YunwangPublicSheet> list = yunwangPublicSheetService.list();
        redisHelper.valueSet(cacheKey,list);
        return ResponseData.success(list);
    }

    /**
     * 更新云网公共表记录
     * @param publicSheet
     * @return
     */
    @Operation(summary = "更新云网公共表记录")
    @PutMapping("/yunwangPublicSheetupdate")
    public ResponseData update(@RequestBody App1YunwangPublicSheet publicSheet) {
        boolean result = yunwangPublicSheetService.updateById(publicSheet);
        redisHelper.delete(cacheKey);
        return result ? ResponseData.success("更新成功") : ResponseData.failed("更新失败", "");
    }

    /**
     * 删除云网公共表记录
     * @param id
     * @return
     */
    @Operation(summary = "删除云网公共表记录")
    @DeleteMapping("/yunwangPublicSheetdelete/{id}")
    public ResponseData deleteById(@PathVariable Integer id) {
        UpdateWrapper<App1YunwangPublicSheet> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1
        yunwangPublicSheetService.update(null, updateWrapper);
        redisHelper.delete(cacheKey);
        return ResponseData.success("删除成功");
    }
}