package com.telecom.sxint.app.api.controller;


import cn.zjtele.pubinfo.common.http.request.RequestContextHolder;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.PubUser;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p >
 *
 * @author
 * @since 2024-08-14
 */
@Tag(name = "获取用户信息", description = "获取用户信息")
@RestController
@RequestMapping("/app1-vatinvoinceprintlog")
public class PubUserController {


    @GetMapping("/getPubUser")
    @Operation(summary = "获取用户信息", description = "获取用户信息")
    public ResponseData getPubUser(HttpServletRequest request){
        PubUser pubUser = new PubUser();
        pubUser.setId(RequestContextHolder.getContext().getUserId());
        pubUser.setRealName(RequestContextHolder.getContext().getUserName());
        pubUser.setOrgId(RequestContextHolder.getContext().getOrgId());
        pubUser.setLoginName(RequestContextHolder.getContext().getLoginName());
        pubUser.setOrgName(RequestContextHolder.getContext().getOrgName());
        List<String> list = RequestContextHolder.getContext().getOrgList();
        Boolean flag=RequestContextHolder.getContext().getSuperAdmin();

        return ResponseData.success(pubUser);
    }


}