package com.telecom.sxint.app.api.controller.SystemController.App1YunwangWorkController;


import cn.zjtele.pubinfo.boot.redis.config.RedisHelper;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkEvaluationIndicators;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1YunwangWorkpojo.App1YunwangWorkOperationSummary;
import com.telecom.sxint.app.core.service.Systemservice.App1YunwangWorkservice.IApp1YunwangWorkOperationSummaryService;
import io.swagger.v3.oas.annotations.tags.Tag;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2024-09-03
 */

@Tag(name = "云网工作评估数据", description = "云网工作评估数据")
@RestController

@RequestMapping("/app1-system")
public class App1YunwangWorkOperationSummaryController {

    @Autowired
    private IApp1YunwangWorkOperationSummaryService app1YunwangWorkOperationSummaryService;
    @Autowired
    private RedisHelper redisHelper;
    String cacheKey = "YunwangWorkOperationsSummary:list";
    /**
     * 查询所有数据
     * @return
     */
    @Operation(summary = "查询所有数据")
    @PostMapping("/Yunwanglist")
    public ResponseData<List<App1YunwangWorkOperationSummary>> list(){
        List<App1YunwangWorkOperationSummary> cacheData = (List<App1YunwangWorkOperationSummary>) redisHelper.valueGet(cacheKey);
        if (cacheData != null && !cacheData.isEmpty()) {
            return ResponseData.success(cacheData);
        }
        List <App1YunwangWorkOperationSummary> list = app1YunwangWorkOperationSummaryService.list();
        redisHelper.valueSet(cacheKey,list);
        return ResponseData.success(list);
    }

    /**
     * 根据考核项目修改
     * @param app1YunwangWorkOperationSummary
     * @return
     */
    @Operation(summary = "根据考核项目修改")
    @PostMapping("/updateByAssessmentItem")
    public ResponseData<?> updateByAssessmentItem(@RequestBody App1YunwangWorkOperationSummary app1YunwangWorkOperationSummary){
        app1YunwangWorkOperationSummaryService.updateByAssessmentItem(app1YunwangWorkOperationSummary);
        this.calculateTotal();
        redisHelper.delete(cacheKey);
        return ResponseData.success("更新成功");
    }


    /**
     * 汇总
     * @return
     */
    @Transactional
    @Operation(summary = "汇总")
    @PostMapping("/calculate")
    public ResponseData calculateTotal(){
        //获取数据
        List<App1YunwangWorkOperationSummary> allRecords = app1YunwangWorkOperationSummaryService.list();
        //创建变量存储计算结果
        BigDecimal yuecheng = BigDecimal.ZERO;
        BigDecimal keqiao = BigDecimal.ZERO;
        BigDecimal shangyu = BigDecimal.ZERO;
        BigDecimal shenghzou = BigDecimal.ZERO;
        BigDecimal xinchang = BigDecimal.ZERO;
        BigDecimal zhuji = BigDecimal.ZERO;

        //遍历数据逐行累加
        for (App1YunwangWorkOperationSummary record : allRecords) {
            if ("汇总".equals(record.getAssessmentItem())) {
                continue; // 如果是汇总记录，则跳过
            }

            // 获取列数据，如果为null则使用BigDecimal.ZERO
            BigDecimal recordYueCheng = (record.getYuecheng() != null) ? record.getYuecheng() : BigDecimal.ZERO;
            BigDecimal recordKeQiao = (record.getKeqiao() != null) ? record.getKeqiao() : BigDecimal.ZERO;
            BigDecimal recordShangYu = (record.getShangyu() != null) ? record.getShangyu() : BigDecimal.ZERO;
            BigDecimal recordShengZhou = (record.getShengzhou() != null) ? record.getShengzhou() : BigDecimal.ZERO;
            BigDecimal recordXinChang = (record.getXinchang() != null) ? record.getXinchang() : BigDecimal.ZERO;
            BigDecimal recordZhuJi = (record.getZhuji() != null) ? record.getZhuji() : BigDecimal.ZERO;

            // 累加
            yuecheng = yuecheng.add(recordYueCheng).setScale(2, BigDecimal.ROUND_HALF_UP);
            keqiao = keqiao.add(recordKeQiao).setScale(2, BigDecimal.ROUND_HALF_UP);
            shangyu = shangyu.add(recordShangYu).setScale(2, BigDecimal.ROUND_HALF_UP);
            shenghzou = shenghzou.add(recordShengZhou).setScale(2, BigDecimal.ROUND_HALF_UP);
            xinchang = xinchang.add(recordXinChang).setScale(2, BigDecimal.ROUND_HALF_UP);
            zhuji = zhuji.add(recordZhuJi).setScale(2, BigDecimal.ROUND_HALF_UP);
        }

        //再次遍历填入数据
        for (App1YunwangWorkOperationSummary record : allRecords){
            if (record.getAssessmentItem().equals("汇总")){
                record.setYuecheng(yuecheng);
                record.setKeqiao(keqiao);
                record.setShangyu(shangyu);
                record.setShengzhou(shenghzou);
                record.setXinchang(xinchang);
                record.setZhuji(zhuji);
            }
        }
        allRecords.forEach(record -> {
            app1YunwangWorkOperationSummaryService.updateByAssessmentItem(record);
        });
        //更新
        return ResponseData.success(app1YunwangWorkOperationSummaryService.list());
    }
}