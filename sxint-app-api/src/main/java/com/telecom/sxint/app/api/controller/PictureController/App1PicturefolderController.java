package com.telecom.sxint.app.api.controller.PictureController;


import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.telecom.sxint.app.core.domain.entity.Picturepojo.App1Picturefolder;
import com.telecom.sxint.app.core.service.Pictureservice.IApp1PicturefolderService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p >
 *
 * @author
 * @since 2024-12-03
 */
@RestController
@RequestMapping("/app1-picturemanagement")
public class App1PicturefolderController {

    @Autowired
    private IApp1PicturefolderService pictureFolderService;

    @Operation(summary = "查询所有文件夹", description = "获取所有图片文件夹")
    @GetMapping("/listAll")
    public ResponseData<List<App1Picturefolder>> listAll() {
        return pictureFolderService.listAll();
    }


    @Operation(summary = "创建图片文件夹", description = "创建一个新的图片文件夹")
    @PostMapping("/create")
    public ResponseData<App1Picturefolder> create(@RequestBody App1Picturefolder folder) {
        return pictureFolderService.create(folder);
    }

    @Operation(summary = "修改图片文件夹", description = "修改现有的图片文件夹")
    @PutMapping("/update")
    public ResponseData<App1Picturefolder> update(@RequestBody App1Picturefolder folder) {
        return pictureFolderService.update(folder);
    }

    @Operation(summary = "删除图片文件夹", description = "根据ID删除图片文件夹")
    @DeleteMapping("/delete/{id}")
    public ResponseData delete(@PathVariable Integer id) {
        return pictureFolderService.delete(id);
    }
}