package com.telecom.sxint.app.api.controller.HistoryController;

import cn.zjtele.pubinfo.common.http.response.ResponseData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.telecom.sxint.app.core.domain.entity.Historypojo.App1HistoryCountyIndicator;
import com.telecom.sxint.app.core.domain.entity.Historypojo.App1MonthlyHistoryPerformanceScoresSummary;
import com.telecom.sxint.app.core.domain.entity.Historypojo.App1QuarterHistoryPublicSheet;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1CountyIndicators;
import com.telecom.sxint.app.core.domain.entity.Systempojo.App1countyIndicatorspojo.App1QuarterPublicSheet;
import com.telecom.sxint.app.core.service.Historyservice.IApp1HistoryCountyIndicatorService;
import com.telecom.sxint.app.core.service.Historyservice.IApp1QuarterHistoryPublicSheetService;

import com.telecom.sxint.app.core.service.Systemservice.App1countyIndicatorsservice.IApp1CountyIndicatorsService;
import com.telecom.sxint.app.core.service.Systemservice.App1countyIndicatorsservice.IApp1QuarterPublicSheetService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * </p >
 * 前端控制器
 *
 * @author
 * @since 2024-09-24
 */
@RestController
@Tag(name = "季度历史记录管理控制器", description = "季度历史记录管理")
@RequestMapping("/app1-systemhistory")
public class App1QuarterHistoryPublicSheetController {

    @Autowired
    private IApp1QuarterHistoryPublicSheetService historyService;
    @Autowired
    private IApp1QuarterPublicSheetService quarterPublicSheetService;
    @Autowired
    private IApp1HistoryCountyIndicatorService historyCountyIndicatorService;
    @Autowired
    private IApp1CountyIndicatorsService countyIndicatorsService;
    @Operation(summary = "查询季度历史记录总表")
    @PostMapping("/QuarterHistoryCountyIndicatorlist")
    public ResponseData QuarterHistoryCountyIndicatorList(){

        QueryWrapper<App1HistoryCountyIndicator> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("id");
        List<App1HistoryCountyIndicator> list = historyCountyIndicatorService.list(queryWrapper);
        if (list.isEmpty()) {
            return ResponseData.failed("查询数据为空","");
        }
        return ResponseData.success(list);
    }
    // 创建记录
    @Operation(summary = "增加季度历史记录")
    @PostMapping("/Quartercreate")
    @Transactional // 使用事务管理，保证操作的一致性
    public ResponseData QuartercreateHistoryRecord( String yearMonth) {

        try
        {
            List<App1QuarterPublicSheet>   app1QuarterPublicSheets = quarterPublicSheetService.list();
            List<App1CountyIndicators> countyIndicators = countyIndicatorsService.list();

            for (App1QuarterPublicSheet a : app1QuarterPublicSheets) {
                App1QuarterHistoryPublicSheet quarterHistoryPublicSheet = new App1QuarterHistoryPublicSheet();
                quarterHistoryPublicSheet.setIsField(a.getIsField());
                quarterHistoryPublicSheet.setSheetName(a.getSheetName());
                quarterHistoryPublicSheet.setGeneralFieldA(a.getGeneralFieldA());
                quarterHistoryPublicSheet.setGeneralFieldB(a.getGeneralFieldB());
                quarterHistoryPublicSheet.setGeneralFieldC(a.getGeneralFieldC());
                quarterHistoryPublicSheet.setGeneralFieldD(a.getGeneralFieldD());
                quarterHistoryPublicSheet.setGeneralFieldE(a.getGeneralFieldE());
                quarterHistoryPublicSheet.setGeneralFieldF(a.getGeneralFieldF());
                quarterHistoryPublicSheet.setGeneralFieldG(a.getGeneralFieldG());
                quarterHistoryPublicSheet.setGeneralFieldH(a.getGeneralFieldH());
                quarterHistoryPublicSheet.setGeneralFieldI(a.getGeneralFieldI());
                quarterHistoryPublicSheet.setGeneralFieldJ(a.getGeneralFieldJ());
                quarterHistoryPublicSheet.setGeneralFieldK(a.getGeneralFieldK());
                quarterHistoryPublicSheet.setGeneralFieldL(a.getGeneralFieldL());
                quarterHistoryPublicSheet.setGeneralFieldM(a.getGeneralFieldM());
                quarterHistoryPublicSheet.setGeneralFieldN(a.getGeneralFieldN());
                quarterHistoryPublicSheet.setGeneralFieldO(a.getGeneralFieldO());
                quarterHistoryPublicSheet.setYearMonth(yearMonth);
                historyService.save(quarterHistoryPublicSheet);
                quarterPublicSheetService.removeById(a);


            }
            for (App1CountyIndicators b : countyIndicators) {
                App1HistoryCountyIndicator app1HistoryCountyIndicator = new App1HistoryCountyIndicator();
                app1HistoryCountyIndicator.setYearMonth(yearMonth);
                app1HistoryCountyIndicator.setIndicatorName(b.getIndicatorName());
                app1HistoryCountyIndicator.setAssessmentMet(b.getAssessmentMethod());
                app1HistoryCountyIndicator.setProvider(b.getProvider());
                app1HistoryCountyIndicator.setIsDelete(0);
                app1HistoryCountyIndicator.setScore(b.getScore());
                historyCountyIndicatorService.save(app1HistoryCountyIndicator);
                countyIndicatorsService.removeById(b);

            }
            return ResponseData.success("记录创建成功");
        }

        catch (Exception e) {
            // 捕获异常并返回错误信息
            return ResponseData.failed("记录创建失败：" + e.getMessage(),"");
        }

    }



    @Operation(summary = "根据年月查询季度历史记录总表")
    @PostMapping("/QuarterHistoryCountyIndicatorByYearMonth")
    public ResponseData QuarterHistoryCountyIndicatorByyearMonthList(String yearMonth){
        // 检查 yearMonth 是否为空
        if (yearMonth == null || yearMonth.trim().isEmpty()) {
            return ResponseData.failed("YearMonth 参数不能为空","");
        }
        QueryWrapper<App1HistoryCountyIndicator> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("year_month", yearMonth);
        queryWrapper.orderByAsc("id");
        List<App1HistoryCountyIndicator> list = historyCountyIndicatorService.list(queryWrapper);
        return ResponseData.success(list);
    }

    /**
     * 根据年月查询历史记录，并分页显示
     */
    /**
     * 根据年月查询历史记录，并分页显示
     */
    @Operation(summary = "根据年月查询季度历史记录")
    @PostMapping("/QuarterHistoryPublicSheetByYearMonth")
    public ResponseData QuartergetyearMonthList(String yearMonth){
        // 检查 yearMonth 是否为空
        if (yearMonth == null || yearMonth.trim().isEmpty()) {
            return ResponseData.failed("YearMonth 参数不能为空","");
        }
        QueryWrapper<App1QuarterHistoryPublicSheet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("year_month", yearMonth);
        queryWrapper.orderByAsc("id");
        List<App1QuarterHistoryPublicSheet> list = historyService.list(queryWrapper);
        return ResponseData.success(list);
    }


    // 根据ID读取记录
    @Operation(summary = "根据id查询季度历史记录")
    @GetMapping("/Quarterget/{id}")
    public ResponseData QuartergetHistoryRecord(@PathVariable Integer id) {
        return ResponseData.success(historyService.getById(id));
    }

    // 更新记录
    @Operation(summary = "更新季度历史记录")
    @PutMapping("/Quarterupdate")
    public ResponseData QuarterupdateHistoryRecord(@RequestBody App1QuarterHistoryPublicSheet record) {
        boolean isUpdated = historyService.updateById(record);
        return ResponseData.success(isUpdated ? "记录更新成功" : "记录更新失败");
    }
    @Operation(summary = "根据ID删除记录季度历史记录")
    @DeleteMapping("/Quarterdelete/{id}")
    public ResponseData QuarterdeleteHistoryRecord(@PathVariable Integer id) {
        boolean isDeleted = historyService.removeById(id);
        return ResponseData.success(isDeleted ? "记录删除成功" : "记录删除失败") ;
    }

    @Operation(summary = "查询所有季度历史记录")
    @GetMapping("/Quarterlist")
    public ResponseData QuarterlistAllHistoryRecords() {
        return ResponseData.success(historyService.list()) ;
    }
}