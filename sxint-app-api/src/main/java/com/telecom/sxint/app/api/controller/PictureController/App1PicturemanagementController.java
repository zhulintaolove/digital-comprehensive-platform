package com.telecom.sxint.app.api.controller.PictureController;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.telecom.sxint.app.core.domain.entity.Vatinvoicepojo.App1Vatinvoinceprintlog;
import com.telecom.sxint.app.core.mapper.Picturemapper.App1PicturemanagementMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.telecom.sxint.app.core.domain.entity.Picturepojo.App1Picturemanagement;
import com.telecom.sxint.app.core.domain.entity.Query.PageData;
import com.telecom.sxint.app.core.domain.entity.dto.PictureQueryDTO;
import com.telecom.sxint.app.core.service.Pictureservice.IApp1PicturemanagementService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cn.zjtele.pubinfo.common.http.response.ResponseData;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
/**
 * <p>
 * 事件管理信息表 前端控制器
 * </p>
 *
 * @author 
 * @since 2024-08-23
 */
@RestController
@Tag(name = "办公室图片管理控制器", description = "办公室图片管理")
@RequestMapping("/app1-picturemanagement")
public class App1PicturemanagementController {
    @Autowired
    private IApp1PicturemanagementService app1PicturemanagementService;
    @Autowired
    private App1PicturemanagementMapper app1PicturemanagementMapper;
    /**
     * 查询全部图片或视频
     */
    @Operation(summary = "查询全部图片或视频", description = "查询全部图片或视频")
    @GetMapping("/Picturelist")
    public ResponseData getList() {
        // 查询数据库，获取符合条件的记录
        // 使用QueryWrapper构建查询条件
        QueryWrapper<App1Picturemanagement> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0) // 确保只查询is_delete字段为0的记录
        .orderByDesc("event_time"); // 确保只查询is_delete字段为0的记录

        // 根据构建的查询条件查询数据库，获取符合条件的记录
        // 查询数据库，获取符合条件的记录
        List<App1Picturemanagement> results = app1PicturemanagementService.list(queryWrapper);
        if (results==null||results.isEmpty()) {
            return ResponseData.failed("未查到相关记录","4001");
        }
        return ResponseData.success(results);
    }
    /**
     * 根据关键字搜索图片
     */
    @Operation(summary = "根据关键字搜索图片", description = "根据关键字搜索图片")
    @GetMapping ("/searchpicture/{keyWord}")
    public ResponseData  searchPictureByKeyWord(@PathVariable String keyWord) {
        QueryWrapper<App1Picturemanagement> queryWrapper = new QueryWrapper<>();
        queryWrapper
                .eq("is_delete",0)
                .like("department", keyWord)
                .or()
                .like("main_personnel", keyWord)
                .or()
                .like("event_name", keyWord)
                .or()
                .like("event_location", keyWord)
                .or()
                .like("tags", keyWord)
                .or()
                .like("event_type", keyWord)
                .orderByDesc("event_time");

        // 直接返回结果列表，不进行分页
        List<App1Picturemanagement> results= app1PicturemanagementMapper.selectList(queryWrapper);
        return ResponseData.success(results);
    }
    /**
     *  根据id获得图片
     * @param id
     * @return
     */
    @Operation(summary = "根据id获得图片", description = "根据id获得图片")
    @GetMapping("/getPicByID")
    public ResponseData getPicByID(@RequestParam Integer id) {
        if (id == null) {
            return ResponseData.failed("未选择图片记录", "4002");
        }
            // 构建查询条件
            QueryWrapper<App1Picturemanagement> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("id", id).eq("is_delete", 0); // 确保只查询is_delete字段为0的记录
            // 根据ID和is_delete条件查询数据库，获取符合条件的记录
            App1Picturemanagement app1Picturemanagement = app1PicturemanagementService.getOne(queryWrapper);
            if (app1Picturemanagement == null) {
                return ResponseData.failed("未查到相关记录", "4001");
            }
            return ResponseData.success(app1Picturemanagement);
    }
    /**
     * 搜索图片、关键词搜索
     * @param key
     * @return
     */
    @Operation(summary = "关键词搜索图片", description = "关键词搜索图片")
    @GetMapping("/getPicByKey/{key}")
    public ResponseData getPicByKey(@PathVariable String key) {
        if(StrUtil.isBlank(key)){
            return ResponseData.failed("请选择关键词","4001");
        }
        List<App1Picturemanagement> results = app1PicturemanagementService.getPictureByKey(key).getData();
        if (results==null||results.isEmpty()) {
            return ResponseData.failed("未查到相关记录","4001");
        }
        // 按照事件时间排序
        results.sort(Comparator.comparing(App1Picturemanagement::getEventTime));
        return ResponseData.success(results);
    }
    /**
     * 查询所有图片（分页）
     */
    @Operation(summary = "查询所有图片（分页）", description = "查询所有图片（分页）")
    @PostMapping("/getAllPicturesPage")
    public ResponseData<PageData<App1Picturemanagement>> getAllPicturesPage(@RequestBody PictureQueryDTO pictureQueryDTO) {
        // 查询所有图片（分页查询）
        PageData<App1Picturemanagement> pageData = app1PicturemanagementService.queryAllPicturesPage(pictureQueryDTO);

        // 确保分页结果按事件时间排序
        pageData.getRecords().sort(Comparator.comparing(App1Picturemanagement::getEventTime));

        return ResponseData.success(pageData);
    }
    /**
     * 上传图片或视频
     * @param file
     * @return
     */
    @Operation(summary = "上传图片或视频", description = "上传图片或视频")
    @PostMapping("/uploadfile")
    public ResponseData uploadFile(@RequestParam("file") MultipartFile file){

        return app1PicturemanagementService.uploadFile(file);
    }
    /**
     * 新增图片数据
     * @param app1Picturemanagement
     * @return
     */
    @Operation(summary = "新增图片数据", description = "新增图片数据")
    @PostMapping("/insertpicture")
    public ResponseData<App1Picturemanagement> insertPicture(@RequestBody App1Picturemanagement app1Picturemanagement){
        return app1PicturemanagementService.insertPicture(app1Picturemanagement);
    }

    //新增多个图片数据
    @Operation(summary = "新增多个图片数据", description = "新增多个图片数据")
    @PostMapping("/insertpictures")
    public ResponseData<List<App1Picturemanagement>> insertPictures(@RequestBody List<App1Picturemanagement> app1Picturemanagements){
        return app1PicturemanagementService.insertPictures(app1Picturemanagements);
    }
    /**
     * 根据id修改图片数据
     * @param app1Picturemanagement
     * @return
     */
    @Operation(summary = "修改图片数据", description = "根据id修改图片数据")
    @PutMapping("/updatepicture")
    public  ResponseData<App1Picturemanagement> updatePicture(@RequestBody App1Picturemanagement app1Picturemanagement){
        return app1PicturemanagementService.updatePicture(app1Picturemanagement);
    }


    /**
     * 分页条件查询
     * @param pictureQueryDTO
     * @return
     */
    @Operation(summary = "分页条件查询", description = "分页条件查询")
    @PostMapping("/picturepage")
    public ResponseData<PageData<App1Picturemanagement>> queryPicturePage(@RequestBody PictureQueryDTO pictureQueryDTO){
        // 在服务层中添加排序逻辑
        PageData<App1Picturemanagement> pageData = app1PicturemanagementService.queryPicturePage(pictureQueryDTO);

        // 确保在分页查询时按照事件时间排序
        pageData.getRecords().sort(Comparator.comparing(App1Picturemanagement::getEventTime));

        return ResponseData.success(pageData);
    }

    /**
     * 批量删除图片信息
     * @param ids
     * @return
     */
    @Operation(summary = "批量删除图片信息", description = "根据ids删除图片信息")
    @DeleteMapping("/picturedeleteids")
    public ResponseData deletByIds(@RequestBody List<Integer> ids) {
        if(ids==null||ids.isEmpty())return ResponseData.success("未选择删除记录");
        UpdateWrapper<App1Picturemanagement> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in("id", ids) // 设置更新条件，这里假设id是更新条件
                .set("is_delete", 1); // 设置 is_delete 字段的值为 1

        // 调用服务类的 update 方法进行批量更新
        app1PicturemanagementService.update(null, updateWrapper);
        return ResponseData.success("删除成功");
    }
    @Operation(summary = "根据事件类型分页查询图片", description = "分页查询根据事件类型分页查询图片图片")
    @PostMapping("/searchByEventPage")
    public ResponseData searchByEventPage(@RequestBody PictureQueryDTO pictureQueryDTO,String event) {
        PageData<App1Picturemanagement> pageData = app1PicturemanagementService.queryPicturePageByEvent(pictureQueryDTO,event);
        pageData.getRecords().sort(Comparator.comparing(App1Picturemanagement::getEventTime));
        return ResponseData.success(pageData);
    }

}
